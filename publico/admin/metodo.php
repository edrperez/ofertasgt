<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
//esto crea el arreglo que se usa en varias partes del script.
$adminMetodo = new Metodo();
$idCat = $sanyval->sanyval(filter_input(INPUT_GET, 'id'), 'entero', 'entero');
if ($idCat > 0) {
    $datosMetodo = $adminMetodo->recuperar($idCat);
    if (!is_array($datosMetodo)) {
        header("location: " . $_SERVER['PHP_SELF']);
        exit();
    }
}
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'agregar':
            $id = filter_input(INPUT_POST, 'metodo_id');
            $nombre = filter_input(INPUT_POST, 'nombre');
            $archivo = '';
            $imagen = '';
            //Comprobamos la existencia de la imagen.
            if ($id > 0) {
                $datos = $adminMetodo->recuperar($id);
                $archivo = '../' . RUTA_IMAGENES_DISENO . "pagos/" . $datos['imagen'];
            }
            //Configuramos el nombre de la imagen a la del nuevo archivo si
            //es que ya existe otra.
            if (file_exists($archivo) == true) {
                if (empty($_FILES["file"]["name"])) {
                    $imagen = $datos['imagen'];
                }
            }
            if (file_exists($archivo) == false && empty($_FILES["file"]["name"])) {
                echo 'Debe agregar una imagen.';
                break;
            }
            //Realizamos todas las verificaciones de la imagen.
            if (!empty($_FILES["file"]["type"])) {
                $validas = array("jpeg", "jpg", "png");
                $temporal = explode(".", $_FILES["file"]["name"]);
                $extension = end($temporal);
                if (((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg"))) && in_array($extension, $validas)) {
                    if ($_FILES["file"]["error"] > 0) {
                        echo "Error: " . $_FILES["file"]["error"] . "<br/><br/>";
                        $registro = new Registro($vSesion, 'Manejo de Métodos de Pago', 'Error cargando imagen. Error: ' . $_FILES["file"]["error"]);
                    } else {
                        $nueva = uniqid(rand(), true) . '.' . $extension;
                        $origen = $_FILES['file']['tmp_name'];
                        $destino = '../' . RUTA_IMAGENES_DISENO . "pagos/" . $nueva;
                        $arreglo = $adminMetodo->recuperar($id);
                        if ($id > 0) {
                            @unlink('../' . RUTA_IMAGENES_DISENO . "pagos/" . $arreglo['imagen']);
                        }
                        move_uploaded_file($origen, $destino);
                        $imagen = $nueva;
                    }
                } else {
                    echo "Imagen inválida.";
                    break;
                }
            }
            echo $adminMetodo->agregarMetodo($id, $nombre, $imagen);
            break;
        case 'cambiarEstado':
            $id = filter_input(INPUT_POST, 'metodo_id');
            $seguro = filter_input(INPUT_POST, 'seguro');
            if (isset($seguro)) {
                if (isset($_POST['estado'])) {
                    $estado = 'Y';
                } else {
                    $estado = 'N';
                }
                echo $adminMetodo->cambiarEstado($id, $estado);
            } else {
                echo 'El método no se ha modificado.';
            }
            echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            break;
        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('modificar_metodos_de_pago') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();

    if (isset($_GET['editar'])) {
        /**
         * Muestra el menú en el que se pueden editar los métodos de manera
         * individual.
         * 
         */
        $metodoEditar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.metodo.editar.php');
        $metodoEditar->metodo_id = $datosMetodo['metodo_id'];
        $metodoEditar->nombre = $datosMetodo['nombre'];
        $metodoEditar->imagen = $datosMetodo['imagen'];
        $metodoEditar->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
        $metodoEditar->publish();
    } elseif (isset($_GET['cambiarEstado'])) {
        /**
         * Muestra el menú para cambiar de estado al método.
         */
        $metodoEstado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.metodo.estado.php');
        $metodoEstado->metodo_id = $datosMetodo['metodo_id'];
        $metodoEstado->nombre = $datosMetodo['nombre'];
        $metodoEstado->estado = $datosMetodo['estado'];
        $metodoEstado->publish();
    } else {
        /**
         * Muestra el menú en el que se pueden agregar métodos. También
         * despliega un listado de métodos a editar.
         * 
         */
        if (!isset($_GET['pag'])) {
            $pagina = 1;
        } else {
            $pagina = $sanyval->sanyval(filter_input(INPUT_GET, 'pag'), 'entero', 'entero');
        }

        if (empty($pagina)) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $enlace = "" . $_SERVER['PHP_SELF'] . "?pag=";
        $paginar = new Paginado($obtopc->retornar('paginaresultados'), $enlace);
        $cadena = 'SELECT metodo_id, nombre, estado FROM metodo ORDER BY nombre';
        $datos = $paginar->retornar($cadena, $pagina);

        $metodoAgregar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.metodo.agregar.php');
        $metodoAgregar->publish();
        if (!empty($datos)) {
            $metodoListar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.metodo.listar.php');
            $metodoListar->datos = $datos;
            $metodoListar->paginar = $paginar->paginar($pagina);
            $metodoListar->publish();
        }
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}