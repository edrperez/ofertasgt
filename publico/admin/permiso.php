<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
$adminPermiso = new Permiso();
if (isset($_GET['op'])) {
/**
 * Todas las opciones de trabajo.
 */
    switch ($_GET['op']) {
        case 'editar':
            $id = filter_input(INPUT_POST, 'permiso_id');
            $nombre = filter_input(INPUT_POST, 'nombre');
            $llave = filter_input(INPUT_POST, 'llave');
            echo $adminPermiso->modificarPermiso($id, $nombre, $llave);
            break;
        case 'eliminar':
            $id = filter_input(INPUT_POST, 'permiso_id');
            echo $adminPermiso->eliminarPermiso($id);
            break;

        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('modificar_permisos') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();

    if (isset($_GET['editar'])) {
        /**
         * Muestra el menú en el que se pueden editar los permisos de manera
         * individual.
         * 
         */
        $permisoEditar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.permiso.editar.php');
        $idpermiso = @$sanyval->sanyval(filter_input(INPUT_GET, 'idpermiso'), 'entero', 'entero');
        if (($idpermiso === false)) {
            $permisoEditar->titulo = 'Nuevo Permiso:';
        } else {
            $cadena = sprintf('SELECT permiso_id, nombre, llave FROM permiso WHERE permiso_id = %u', $idpermiso);
            $sql = $BD->GetRow($cadena);
            if (count($sql) == 0) {
                header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
                exit();
            }
            $permisoEditar->titulo = 'Editar Permiso: ' . $sql['nombre'];
            $permisoEditar->permiso_id = $sql['permiso_id'];
            $permisoEditar->nombre = $sql['nombre'];
            $permisoEditar->llave = $sql['llave'];
        }
        $permisoEditar->publish();
    } else {
        /**
         * Muestra el listado de permisos a editar.
         * 
         */
        $permisoMenu = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.permiso.php');
        $permisoMenu->permisos = $ACL->obtTodosPermisos('completo');
        $permisoMenu->publish();
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}