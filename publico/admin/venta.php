<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
//esto crea el arreglo que se usa en varias partes del script.
$adminVenta = new Venta();
$idVen = $sanyval->sanyval(filter_input(INPUT_GET, 'id'), 'entero', 'entero');
if ($idVen > 0) {
    $datosVenta = $adminVenta->recuperarVenta($idVen);
    if (!is_array($datosVenta)) {
        header("location: " . $_SERVER['PHP_SELF']);
        exit();
    }
}
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'exportar':
            $fechai = filter_input(INPUT_POST, 'fechai');
            $fechaf = filter_input(INPUT_POST, 'fechaf');
            $empresa_id = filter_input(INPUT_POST, 'empresa_id');
            $di = DateTime::createFromFormat('Y-m-d H:i', $fechai);
            $df = DateTime::createFromFormat('Y-m-d H:i', $fechaf);
            /* if (!$di->format('Y-m-d H:i') == $fechai || !$df->format('Y-m-d H:i') == $fechaf) {
              echo 'Formato incorrecto de fecha.';
              exit();
              } */

            $BD->SetFetchMode(ADODB_FETCH_ASSOC);
            $cadenaH = sprintf('SELECT CONCAT(venta_detalle.oferta_id, venta_detalle.venta_id, venta_detalle.venta_detalle_id) as Cupón,
                empresa.nombre as Empresa, oferta.nombre as Oferta, venta.fechai as Fecha,
                venta_detalle.venta_id as Venta, venta.nombre as Comprador,
                venta.correo as \'Correo Comprador\', venta.nit as NIT, venta.telefono as Teléfono,
                venta.direccion as Dirección, venta_detalle.usuario as Beneficiario,
                venta_detalle.correo as \'Correo Beneficiario\', venta_detalle.precioo as \'Precio Original\',
                venta_detalle.preciod as \'Precio con Descuento\',
                venta_detalle.cobrado as Canjeado
                FROM venta_detalle INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
                JOIN venta ON venta.venta_id = venta_detalle.venta_id
                JOIN empresa ON empresa.empresa_id = oferta.empresa_id
                WHERE empresa.empresa_id = %u', $empresa_id);
            $regH = $BD->GetRow($cadenaH);
            $encabezados = array();
            foreach ($regH as $k => $v) {
                $encabezados[] = $k;
            }
            $cadena = sprintf('SELECT CONCAT(venta_detalle.oferta_id, venta_detalle.venta_id, venta_detalle.venta_detalle_id) as Cupón,
                empresa.nombre as Empresa,oferta.nombre as Oferta, venta.fechai as Fecha,
                venta_detalle.venta_id as Venta, venta.nombre as Comprador,
                venta.correo as \'Correo Comprador\', venta.nit as NIT, venta.telefono as Teléfono,
                venta.direccion as Dirección, venta_detalle.usuario as Beneficiario,
                venta_detalle.correo as \'Correo Beneficiario\', venta_detalle.precioo as \'Precio Original\',
                venta_detalle.preciod as \'Precio con Descuento\',
                venta_detalle.cobrado as Canjeado
                FROM venta_detalle INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
                JOIN venta ON venta.venta_id = venta_detalle.venta_id
                JOIN empresa ON empresa.empresa_id = oferta.empresa_id
                WHERE (empresa.empresa_id = %u AND venta.fechai BETWEEN "%s" AND "%s") ORDER BY Fecha ASC', $empresa_id, $fechai, $fechaf);
            $reg = $BD->Execute($cadena);

            if ($reg->RecordCount() == 0) {
                echo 'No hay datos para exportar';
                exit();
            }

            $fp = fopen('php://output', 'w');
            if ($fp && $reg) {
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . date("Y-m-d H-i-s", strtotime($fechai)) . '_' . date("Y-m-d H-i-s", strtotime($fechaf)) . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                echo "\xEF\xBB\xBF";
                fputcsv($fp, $encabezados);
                while (!$reg->EOF) {
                    fputcsv($fp, array_values($reg->fields));
                    $reg->MoveNext();
                }
                die;
            }
            //echo $adminVenta->agregarEmpresa($id, $nombre, $valores);
            break;
        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('ver_ventas') != true && $ACL->tienePermiso('ventas_completo') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->datetimepicker = true;
    $encabezado->publish();

    if (isset($_GET['detalle'])) {
        /**
         * Muestra el detalle de cada venta
         */
        $ventaDetalle = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.venta.detalle.php');
        $ventaDetalle->datos = $datosVenta;
        $ventaDetalle->detalle = $adminVenta->recuperarDetalle($idVen);
        $ventaDetalle->publish();
    } else {
        /**
         * Muestra el menú en el que se pueden agregar empresas. También
         * despliega un listado de empresas a editar.
         * 
         */
        if (!isset($_GET['pag'])) {
            $pagina = 1;
        } else {
            $pagina = $sanyval->sanyval(filter_input(INPUT_GET, 'pag'), 'entero', 'entero');
        }

        if (empty($pagina)) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $enlace = "" . $_SERVER['PHP_SELF'] . "?pag=";
        $paginar = new Paginado($obtopc->retornar('paginaresultados'), $enlace);
        //Recuperamos los registros sólo del usuario actual
        $cadena = 'SELECT venta_detalle.venta_id, venta.fechai as Fecha,
            empresa.nombre as Empresa, SUM(venta_detalle.preciod) as Monto
            FROM venta_detalle INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
            JOIN venta ON venta.venta_id = venta_detalle.venta_id
            JOIN empresa ON empresa.empresa_id = oferta.empresa_id
            WHERE empresa.usuario_id = ' . $vSesion . ' GROUP BY venta_id ORDER BY fecha DESC';
        $datos = $paginar->retornar($cadena, $pagina);
        $adminEmpresa = new Empresa();
        if ($ACL->tienePermiso('ventas_completo') == false) {
            $empresas = $adminEmpresa->recuperarTodos(true);
        } else {
            $empresas = $adminEmpresa->recuperarTodos(false);
        }

        if (!empty($datos)) {
            $ventaListar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.venta.listar.php');
            $ventaListar->datos = $datos;
            $ventaListar->paginar = $paginar->paginar($pagina);
            $ventaListar->empresas = $empresas;
            $ventaListar->ver_ventas = $ACL->tienePermiso('ver_ventas');
            $ventaListar->publish();
        }
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->datetimepicker = true;
    $pie->publish();
}