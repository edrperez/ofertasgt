<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Sólo se configura la variable que tendrá el valor del id del usuario
//pasado por GET.
if (isset($_GET["idus"])) {
    $idUs = $sanyval->sanyval(filter_input(INPUT_GET, 'idus'), 'entero', 'entero');
    if (empty($idUs)) {
        header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
        exit();
    }
}
//Configuramos el estado de la variable POST
if (isset($_POST['estado'])) {
    $estado = 'Y';
} else {
    $estado = 'N';
}
$adminUsuario = new Usuario();
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'agregar':
            $usuario = filter_input(INPUT_POST, 'usuario');
            $clave = filter_input(INPUT_POST, 'clave');
            $correo = filter_input(INPUT_POST, 'correo');
            echo $adminUsuario->agregarUsuario($usuario, $clave, $correo);
            break;
        case 'editar':
            $usuario = filter_input(INPUT_POST, 'usuario');
            $clave = filter_input(INPUT_POST, 'clave');
            $correo = filter_input(INPUT_POST, 'correo');
            $idus = filter_input(INPUT_POST, 'usuario_id');
            echo $adminUsuario->modificar($idus, $usuario, $clave, $correo);
            break;
        case 'estadoUsuario':
            $id = filter_input(INPUT_POST, 'usuario_id');
            $seguro = filter_input(INPUT_POST, 'seguro');
            if (isset($seguro)) {
                echo $adminUsuario->cambiarEstado($id, $estado);
            } else {
                echo 'El usuario no se ha modificado.';
            }
            echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            break;
        case 'usuarioRoles':
            $idus = filter_input(INPUT_POST, 'usuario_id');
            echo $adminUsuario->asignarRoles($idus, $_POST);
            break;
        case 'usuarioPermisos':
            $idus = filter_input(INPUT_POST, 'usuario_id');
            echo $adminUsuario->asignarPermisos($idus, $_POST);
            break;

        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('modificar_usuarios') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();

    /*if ($_SERVER['QUERY_STRING'] != '') {
        $direccion = $_SERVER['PHP_SELF'];
    } else {
        $direccion = 'index.php';
    }

    echo '<a href="' . $direccion . '">&#60;&#60;</a>';*/

    if (isset($_GET['editar'])) {
        /**
         * Muestra el menú para modificar los datos de un usuario.
         */
        $cadena = sprintf('SELECT usuario_id, usuario, correo FROM usuario WHERE usuario_id = %u', $idUs);
        $sql = $BD->GetRow($cadena);
        if (count($sql) == 0) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $usuarioACL = new ACL($idUs);
        $roles = $usuarioACL->obtTodosRoles('completo');
        $nombres = array();
        foreach ($roles as $k => $v) {
            $nombres[$v['id']] = $usuarioACL->obtRolNombreDeId($v['id']);
        }
        $usuarioModificar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.usuario.editar.php');
        $usuarioModificar->idUs = $idUs;
        $usuarioModificar->usuario = $sql['usuario'];
        $usuarioModificar->usuario_id = $sql['usuario_id'];
        $usuarioModificar->correo = $sql['correo'];
        $usuarioModificar->roles = $usuarioACL->obtUsuarioRoles();
        $usuarioModificar->permisos = $usuarioACL->obtPermisos();
        $usuarioModificar->nombres = $nombres;
        $usuarioModificar->publish();
    } elseif (isset($_GET['estadoUsuario'])) {
        /**
         * Muestra el menú para cambiar de estado al usuario.
         */
        $cadena = sprintf('SELECT usuario_id, usuario, estado FROM usuario WHERE usuario_id = %u', $idUs);
        $sql = $BD->GetRow($cadena);
        if (count($sql) == 0) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $usuarioEstado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.usuario.estado.php');
        $usuarioEstado->usuario = $sql['usuario'];
        $usuarioEstado->usuario_id = $sql['usuario_id'];
        $usuarioEstado->estado = $sql['estado'];
        $usuarioEstado->publish();
    } elseif (isset($_GET['roles'])) {
        /**
         * Muestra el menú para asignar roles a un usuario.
         */
        $usuarioACL = new ACL($idUs);
        $roles = $usuarioACL->obtTodosRoles('completo');
        $asignados = array();
        foreach ($roles as $k => $v) {
            $asignados[$v['id']] = $usuarioACL->usuarioTieneRol($v['id']);
        }
        $usuarioRoles = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.usuario.roles.php');
        $usuarioRoles->usuario_id = $idUs;
        $usuarioRoles->usuario = $usuarioACL->obtUsuario($idUs);
        $usuarioRoles->roles = $usuarioACL->obtTodosRoles('completo');
        $usuarioRoles->asignados = $asignados;
        $usuarioRoles->publish();
    } elseif (isset($_GET['permisos'])) {
        /**
         * Muestra el menú para asignar permisos a un usuario.
         */
        $usuarioACL = new ACL($idUs);
        $usuarioPermisos = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.usuario.permisos.php');
        $usuarioPermisos->usuario = $usuarioACL->obtUsuario($idUs);
        $usuarioPermisos->usuario_id = $idUs;
        $usuarioPermisos->permisos = $usuarioACL->obtPermisos();
        $usuarioPermisos->permisosTotales = $usuarioACL->obtTodosPermisos('completo');
        $usuarioPermisos->publish();
    } else {
        /**
         * Muestra el menú en el que se pueden agregar datos. También despliega
         * un listado de usuarios y sus opciones.
         * 
         */
        if (!isset($_GET['pag'])) {
            $pagina = 1;
        } else {
            $pagina = $sanyval->sanyval(filter_input(INPUT_GET, 'pag'), 'entero', 'entero');
        }
        if (empty($pagina)) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $enlace = "" . $_SERVER['PHP_SELF'] . "?pag=";
        $paginar = new Paginado($obtopc->retornar('paginaresultados'), $enlace);
        $cadena = 'SELECT * FROM usuario WHERE usuario_id > 0 ORDER BY usuario';
        $datos = $paginar->retornar($cadena, $pagina);
        $usuarioAgregar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.usuario.agregar.php');
        $usuarioAgregar->datos = $datos;
        $usuarioAgregar->paginar = $paginar->paginar($pagina);
        $usuarioAgregar->intentos = $obtopc->retornar('intentos');
        $usuarioAgregar->publish();
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}