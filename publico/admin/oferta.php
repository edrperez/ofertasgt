<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
//esto crea el arreglo que se usa en varias partes del script.
$adminOferta = new Oferta();
$idCat = $sanyval->sanyval(filter_input(INPUT_GET, 'id'), 'entero', 'entero');
if ($idCat > 0) {
    $datosOferta = $adminOferta->recuperar($idCat);
    if (!is_array($datosOferta)) {
        header("location: " . $_SERVER['PHP_SELF']);
        exit();
    }
}
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'agregar':

            $id = filter_input(INPUT_POST, 'oferta_id');
            $nombre = filter_input(INPUT_POST, 'nombre');
            $valores = array();
            //Asignamos a un arreglo todos los valores extras del formulario.
            $valores['resumen'] = filter_input(INPUT_POST, 'resumen');
            $valores['descripcion'] = filter_input(INPUT_POST, 'descripcion');
            $valores['condiciones'] = filter_input(INPUT_POST, 'condiciones');
            $valores['fechaf'] = filter_input(INPUT_POST, 'fechaf');
            $valores['fechav'] = filter_input(INPUT_POST, 'fechav');
            $valores['empresa_id'] = filter_input(INPUT_POST, 'empresa_id');
            $valores['precioo'] = filter_input(INPUT_POST, 'precioo');
            $valores['preciod'] = filter_input(INPUT_POST, 'preciod');
            $valores['categoria_id'] = filter_input(INPUT_POST, 'categoria_id');
            //Verificamos que el formato de la fecha sea correcto.
            $df = DateTime::createFromFormat('Y-m-d H:i', $valores['fechaf']);
            $dv = DateTime::createFromFormat('Y-m-d H:i', $valores['fechav']);
            if (!$df->format('Y-m-d H:i') == $valores['fechaf'] || !$dv->format('Y-m-d H:i') == $valores['fechav']) {
                echo 'Formato incorrecto de fecha.';
                exit();
            }
            //Asignamos nombre a la imagen de si el archivo ya existe.
            $archivo = '';
            $imagen = '';
            if ($id > 0) {
                $datos = $adminOferta->recuperar($id);
                $archivo = '../' . RUTA_IMAGENES_DISENO . "ofertas/" . $datos['imagen'];
            }
            //Asignamos el nombre a la imagen con el nuevo archivo.
            if (file_exists($archivo) == true) {
                if (empty($_FILES["portada"]["name"])) {
                    $imagen = $datos['imagen'];
                }
            }
            if (file_exists($archivo) == false && empty($_FILES["portada"]["name"])) {
                echo 'Debe agregar una imagen.';
                break;
            }
            //Hacemos todas las verificaciones a la imagen
            if (!empty($_FILES["portada"]["type"])) {
                $validas = array("jpeg", "jpg", "png");
                $temporal = explode(".", $_FILES["portada"]["name"]);
                $extension = end($temporal);
                if (((($_FILES["portada"]["type"] == "image/png") || ($_FILES["portada"]["type"] == "image/jpg") || ($_FILES["portada"]["type"] == "image/jpeg"))) && in_array($extension, $validas)) {
                    if ($_FILES["portada"]["error"] > 0) {
                        echo "Error: " . $_FILES["portada"]["error"] . "<br/><br/>";
                        $registro = new Registro($vSesion, 'Manejo de Ofertas', 'Error cargando imagen. Error: ' . $_FILES["portada"]["error"]);
                    } else {
                        $nueva = uniqid(rand(), true) . '.' . $extension;
                        $origen = $_FILES['portada']['tmp_name'];
                        $destino = '../' . RUTA_IMAGENES_DISENO . "ofertas/" . $nueva;
                        $arreglo = $adminOferta->recuperar($id);
                        if ($id > 0) {
                            @unlink('../' . RUTA_IMAGENES_DISENO . "ofertas/" . $arreglo['imagen']);
                        }
                        move_uploaded_file($origen, $destino);
                        $imagen = $nueva;
                    }
                } else {
                    echo "Imagen inválida.";
                    break;
                }
            }
            echo $adminOferta->agregarOferta($id, $nombre, $imagen, $valores);
            break;
        case 'cambiarEstado':
            $id = filter_input(INPUT_POST, 'oferta_id');
            $seguro = filter_input(INPUT_POST, 'seguro');
            if (isset($seguro)) {
                if (isset($_POST['estado'])) {
                    $estado = 'Y';
                } else {
                    $estado = 'N';
                }
                echo $adminOferta->cambiarEstado($id, $estado);
            } else {
                echo 'La oferta no se ha modificado.';
            }
            echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            break;
        case 'imagenes':
            //Hacemos todas las verificaciones a las imagenes, ahora incluímos
            //también el tamaño.
            if (isset($_FILES['archivos'])) {
                $id = filter_input(INPUT_POST, 'oferta_id');
                $imagenes = array();
                $validas = array("jpeg", "jpg", "png");
                $tamanio = 1024 * 2048;
                $archivos = $_FILES['archivos'];
                $contador = count($archivos["name"]);

                for ($i = 0; $i < $contador; $i++) {
                    $temporal = explode(".", $archivos["name"][$i]);
                    $extension = end($temporal);
                    if (((($archivos["type"][$i] == "image/png") || ($archivos["type"][$i] == "image/jpg") || ($archivos["type"][$i] == "image/jpeg"))) && in_array($extension, $validas)) {
                        if ($archivos["error"][$i] > 0) {
                            echo "Error: " . $archivos["error"][$i] . "<br/><br/>";
                            $registro = new Registro($vSesion, 'Manejo de Ofertas', 'Error cargando imagen: ' . $archivos["name"] . '. Error: ' . $archivos["error"]);
                        } else {
                            if ($archivos['size'][$i] >= $tamanio) {
                                echo $archivos['name'][$i] . ' excede el tamaño permitido: ' . (($tamanio / 1024) / 1024) . ' MB.';
                            } else {
                                $nueva = uniqid(rand(), true) . '.' . $extension;
                                $origen = $archivos['tmp_name'][$i];
                                $destino = '../' . RUTA_IMAGENES_DISENO . "ofertas/" . $nueva;
                                move_uploaded_file($origen, $destino);
                                $imagenes[$archivos['name'][$i]] = $nueva;
                            }
                        }
                    } else {
                        echo "Imagen inválida: " . $archivos['name'][$i];
                    }
                }
                echo $adminOferta->agregarImagenes($id, $imagenes);
            }
            break;
        case 'eliminarImagen':
            $id = filter_input(INPUT_POST, 'id');
            $nombre = filter_input(INPUT_POST, 'nombre');
            $adminOferta->eliminarImagen($id, $nombre);
            break;
        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('modificar_ofertas') != true && $ACL->tienePermiso('activar_ofertas') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->datetimepicker = true;
    $encabezado->publish();

    if (isset($_GET['editar'])) {
        if ($ACL->tienePermiso('modificar_ofertas') != true)
            header("location: index.php");
        /**
         * Muestra el menú en el que se pueden editar las ofertas de manera
         * individual.
         * 
         */
        $adminEmpresa = new Empresa();
        $empresas = $adminEmpresa->recuperarTodos(true);
        $adminCategoria = new Categoria();
        $categorias = $adminCategoria->recuperarTodos();

        $ofertaEditar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.oferta.editar.php');
        $ofertaEditar->datos = $datosOferta;
        $ofertaEditar->empresas = $empresas;
        $ofertaEditar->categorias = $categorias;
        $ofertaEditar->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
        $ofertaEditar->RUTA_JS = RUTA_JS;
        $ofertaEditar->publish();
    } elseif (isset($_GET['cambiarEstado'])) {
        /**
         * Muestra el menú para cambiar de estado a la oferta.
         */
        $ofertaEstado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.oferta.estado.php');
        $ofertaEstado->oferta_id = $datosOferta['oferta_id'];
        $ofertaEstado->nombre = $datosOferta['nombre'];
        $ofertaEstado->estado = $datosOferta['estado'];
        $ofertaEstado->publish();
    } elseif (isset($_GET['imagenes'])) {
        /**
         * Muestra el menú para agregar y editar imágenes a la oferta.
         */
        $ofertaImagenes = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.oferta.imagenes.php');
        $ofertaImagenes->oferta_id = $datosOferta['oferta_id'];
        $ofertaImagenes->nombre = $datosOferta['nombre'];
        $ofertaImagenes->imagenes = $adminOferta->recuperarImagenesOferta($datosOferta['oferta_id']);
        $ofertaImagenes->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
        $ofertaImagenes->publish();
    } else {
        /**
         * Muestra el menú en el que se pueden agregar ofertas. También
         * despliega un listado de ofertas a editar.
         * 
         */
        if (!isset($_GET['pag'])) {
            $pagina = 1;
        } else {
            $pagina = $sanyval->sanyval(filter_input(INPUT_GET, 'pag'), 'entero', 'entero');
        }

        if (empty($pagina)) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $enlace = "" . $_SERVER['PHP_SELF'] . "?pag=";
        $paginar = new Paginado($obtopc->retornar('paginaresultados'), $enlace);
        if ($ACL->tienePermiso('activar_ofertas') == true) {
            //Si hay vistas disponibles
            $cadena = 'SELECT * FROM listar_ofertas';
            //Si no hay vistas disponibles
            /* $cadena = 'SELECT oferta.oferta_id as oferta_id, oferta.nombre as nombre, ';
              $cadena.= 'oferta.estado as estado, empresa.nombre as empresa FROM oferta ';
              $cadena.= 'INNER JOIN empresa ON oferta.empresa_id = empresa.empresa_id ';
              $cadena.= 'ORDER BY empresa.nombre'; */
        } else {
            //Si hay vistas disponibles
            $cadena = 'SELECT * FROM listar_ofertas WHERE usuario_id = ' . $vSesion;
            //Si no hay vistas disponibles
            /* $cadena = 'SELECT oferta.oferta_id as oferta_id, oferta.nombre as nombre, ';
              $cadena.= 'oferta.estado as estado, empresa.nombre as empresa FROM oferta ';
              $cadena.= 'INNER JOIN empresa ON oferta.empresa_id = empresa.empresa_id ';
              $cadena.= 'WHERE empresa.usuario_id = ' . $vSesion . ' ORDER BY empresa.nombre'; */
        }
        $datos = $paginar->retornar($cadena, $pagina);
        if ($ACL->tienePermiso('modificar_ofertas') == true) {
            $adminEmpresa = new Empresa();
            $empresas = $adminEmpresa->recuperarTodos(true);
            $adminCategoria = new Categoria();
            $categorias = $adminCategoria->recuperarTodos();

            $ofertaAgregar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.oferta.agregar.php');
            $ofertaAgregar->empresas = $empresas;
            $ofertaAgregar->categorias = $categorias;
            $ofertaAgregar->RUTA_JS = RUTA_JS;
            $ofertaAgregar->publish();
        }
        if (!empty($datos)) {
            $ofertaListar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.oferta.listar.php');
            $ofertaListar->datos = $datos;
            $ofertaListar->paginar = $paginar->paginar($pagina);
            $ofertaListar->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
            $ofertaListar->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
            $ofertaListar->publish();
        }
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->datetimepicker = true;
    $pie->tinymce = true;
    $pie->publish();
}