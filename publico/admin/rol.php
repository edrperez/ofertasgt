<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
$adminRol = new Rol();
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'editar':
            $id = filter_input(INPUT_POST, 'rol_id');
            $nombre = filter_input(INPUT_POST, 'nombre');
            echo $adminRol->modificarRol($id, $nombre, $_POST);
            break;
        case 'eliminar':
            $id = filter_input(INPUT_POST, 'rol_id');
            echo $adminRol->eliminarRol($id);
            break;
        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('modificar_roles') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();

    if (isset($_GET['editar'])) {
        /**
         * Muestra el menú en el que se pueden editar los roles de manera
         * individual.
         * 
         */
        $rolEditar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.rol.editar.php');
        $idrol = @$sanyval->sanyval(filter_input(INPUT_GET, 'idrol'), 'entero', 'entero');
        if (($idrol === false)) {
            $rolEditar->titulo = 'Nuevo Rol:';
        } else {
            $cadena = sprintf('SELECT * FROM rol WHERE rol_id = %u', $idrol);
            $sql = $BD->GetRow($cadena);
            if (count($sql) == 0) {
                header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
                exit();
            }
            $rolEditar->titulo = 'Editar Rol: ' . $sql['nombre'];
            $rolEditar->idrol = $sql['rol_id'];
            $rolEditar->nombre = $sql['nombre'];
        }
        $rolEditar->rPerms = $ACL->obtRolPermisos($idrol);
        $rolEditar->aPerms = $ACL->obtTodosPermisos('completo');
        $rolEditar->publish();
    } else {
        /**
         * Muestra el listado de roles a editar.
         * 
         */
        $rolMenu = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.rol.php');
        $rolMenu->roles = $ACL->obtTodosRoles('completo');
        $rolMenu->publish();
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}