<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'acceso':
            $usuario = $sanyval->sanyval($_POST["usuario"], 'plano', false);
            $clave = $sanyval->sanyval($_POST["clave"], 'plano', false);
            $sesion = $Acceso->iniciarSesion($usuario, $clave, $ageip);
            if ($sesion == 0) {
                echo 'Datos inválidos.';
                echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }

            break;
        case 'salir':
            if ($Acceso->cerrarSesiones($vSesion)) {
                
            }
            break;
    }
} else {
    /**
     * Llamar al encabezado.
     */
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();
    /**
     * Comprobar si existe sesión y permiso de administración.
     */
    if ($vSesion > 0) {
        if ($ACL->tienePermiso('acceso_administrativo') != true) {
            header("location: ../index.php");
        }
        if ($ACL->tienePermiso('ver_registro') == true) {
            /**
             * Muestra el log de registro si tiene el permiso de ver_registro
             */
            $reg = $BD->Execute('SELECT registro_id, usuario.usuario, area, contenido, '
                    . 'fecha, ip, navegador, error FROM registro '
                    . 'INNER JOIN usuario ON registro.usuario_id = usuario.usuario_id '
                    . 'ORDER BY registro_id DESC');
            $datos = [];

            while (!$reg->EOF) {
                $datos[$reg->fields['registro_id']] = ['usuario' => $reg->fields['usuario'],
                    'area' => $reg->fields['area'],
                    'contenido' => $reg->fields['contenido'],
                    'fecha' => $reg->fields['fecha'],
                    'ip' => $reg->fields['ip'],
                    'navegador' => $reg->fields['navegador'],
                    'error' => $reg->fields['error']];
                $reg->MoveNext();
            }

            $registro = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.registro.php');
            $registro->datos = $datos;
            $registro->publish();
        }
    } else {
        $acceso = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.acceso.php');
        $acceso->publish();
    }
    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}