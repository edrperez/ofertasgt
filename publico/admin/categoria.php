<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
//esto crea el arreglo que se usa en varias partes del script.
$adminCategoria = new Categoria();
$idCat = $sanyval->sanyval(filter_input(INPUT_GET, 'id'), 'entero', 'entero');
if ($idCat > 0) {
    $datosCategoria = $adminCategoria->recuperar($idCat);
    if (!is_array($datosCategoria)) {
        header("location: " . $_SERVER['PHP_SELF']);
        exit();
    }
}
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'agregar':
            $id = filter_input(INPUT_POST, 'categoria_id');
            $nombre = $sanyval->sanyval(filter_input(INPUT_POST, 'nombre'), 'plano', false);
            if (!ctype_alnum($nombre)) {
                echo 'El nombre sólo puede contener letras y números.';
                exit();
            };
            echo $adminCategoria->agregarCategoria($id, $nombre);
            break;
        case 'cambiarEstado':
            $id = filter_input(INPUT_POST, 'categoria_id');
            $seguro = filter_input(INPUT_POST, 'seguro');
            if (isset($seguro)) {
                if (isset($_POST['estado'])) {
                    $estado = 'Y';
                } else {
                    $estado = 'N';
                }
                echo $adminCategoria->cambiarEstado($id, $estado);
            } else {
                echo 'La categoría no se ha modificado.';
            }
            echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            break;
        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || $ACL->tienePermiso('modificar_categorias') != true) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();

    if (isset($_GET['editar'])) {
        /**
         * Muestra el menú en el que se pueden editar las categorías de manera
         * individual.
         * 
         */
        $categoriaEditar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.categoria.editar.php');
        $categoriaEditar->categoria_id = $datosCategoria['categoria_id'];
        $categoriaEditar->nombre = $datosCategoria['nombre'];
        $categoriaEditar->publish();
    } elseif (isset($_GET['cambiarEstado'])) {
        /**
         * Muestra el menú para cambiar de estado a la categoría.
         */
        $categoriaEstado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.categoria.estado.php');
        $categoriaEstado->categoria_id = $datosCategoria['categoria_id'];
        $categoriaEstado->nombre = $datosCategoria['nombre'];
        $categoriaEstado->estado = $datosCategoria['estado'];
        $categoriaEstado->publish();
    } else {
        /**
         * Muestra el menú en el que se pueden agregar categorías. También
         * despliega un listado de categorías a editar.
         * 
         */
        if (!isset($_GET['pag'])) {
            $pagina = 1;
        } else {
            $pagina = $sanyval->sanyval(filter_input(INPUT_GET, 'pag'), 'entero', 'entero');
        }

        if (empty($pagina)) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $enlace = "" . $_SERVER['PHP_SELF'] . "?pag=";
        $paginar = new Paginado($obtopc->retornar('paginaresultados'), $enlace);
        $cadena = 'SELECT categoria_id, nombre, estado FROM categoria ORDER BY nombre';
        $datos = $paginar->retornar($cadena, $pagina);

        $categoriaAgregar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.categoria.agregar.php');
        $categoriaAgregar->publish();
        if (!empty($datos)) {
            $categoriaListar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.categoria.listar.php');
            $categoriaListar->datos = $datos;
            $categoriaListar->paginar = $paginar->paginar($pagina);
            $categoriaListar->publish();
        }
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}