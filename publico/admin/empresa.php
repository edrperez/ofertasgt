<?php

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require ('../../recursos/conf.php');
//Instanciamos un nuevo objeto para generar los datos del id pasado por GET
//esto crea el arreglo que se usa en varias partes del script.
$adminEmpresa = new Empresa();
$idEmp = $sanyval->sanyval(filter_input(INPUT_GET, 'id'), 'entero', 'entero');
if ($idEmp > 0) {
    $datosEmpresa = $adminEmpresa->recuperar($idEmp);
    if (!is_array($datosEmpresa)) {
        header("location: " . $_SERVER['PHP_SELF']);
        exit();
    }
}
if (isset($_GET['op'])) {
    /**
     * Todas las opciones de trabajo.
     */
    switch ($_GET['op']) {
        case 'agregar':
            $id = filter_input(INPUT_POST, 'empresa_id');
            $nombre = filter_input(INPUT_POST, 'nombre');
            //Colectamos todos los otros campos en un sólo arreglo para pasarlo
            //al método.
            $valores = array();
            $valores['correo'] = filter_input(INPUT_POST, 'correo');
            $valores['web'] = filter_input(INPUT_POST, 'web');
            $valores['direccion'] = filter_input(INPUT_POST, 'direccion');
            //Se recorren los campos de teléfonos para agregar al arreglo.
            foreach ($_POST as $k => $v) {
                if (strpos($k, 'telefono') === false) {
                    
                } else {
                    $valores['telefonos'][$k] = $v;
                }
            }
            //Se recorren los campos de métodos de pago para agregar al arreglo.
            foreach ($_POST as $k => $v) {
                if (strpos($k, 'metodo') === false) {
                    
                } else {
                    $valores['metodos'][] = $v;
                }
            }
            echo $adminEmpresa->agregarEmpresa($id, $nombre, $valores);
            break;
        case 'cambiarEstado':
            $id = filter_input(INPUT_POST, 'empresa_id');
            $seguro = filter_input(INPUT_POST, 'seguro');
            if (isset($seguro)) {
                if (isset($_POST['estado'])) {
                    $estado = 'Y';
                } else {
                    $estado = 'N';
                }
                echo $adminEmpresa->cambiarEstado($id, $estado);
            } else {
                echo 'La empresa no se ha modificado.';
            }
            echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            break;
        default:
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
            break;
    }
} else {
    //Si no hay sesión iniciada se redirige a la portada.
    if ($vSesion == 0 || ($ACL->tienePermiso('modificar_empresas') != true && $ACL->tienePermiso('activar_empresas') != true)) {
        header("location: index.php");
    }
    $encabezado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->modificar_usuarios = $ACL->tienePermiso('modificar_usuarios');
    $encabezado->modificar_roles = $ACL->tienePermiso('modificar_roles');
    $encabezado->modificar_permisos = $ACL->tienePermiso('modificar_permisos');
    $encabezado->modificar_categorias = $ACL->tienePermiso('modificar_categorias');
    $encabezado->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
    $encabezado->activar_empresas = $ACL->tienePermiso('activar_empresas');
    $encabezado->modificar_metodos_de_pago = $ACL->tienePermiso('modificar_metodos_de_pago');
    $encabezado->modificar_ofertas = $ACL->tienePermiso('modificar_ofertas');
    $encabezado->activar_ofertas = $ACL->tienePermiso('activar_ofertas');
    $encabezado->ver_ventas = $ACL->tienePermiso('ver_ventas');
    $encabezado->ventas_completo = $ACL->tienePermiso('ventas_completo');
    $encabezado->sesion = $vSesion;
    $encabezado->publish();

    if (isset($_GET['editar'])) {
        if($ACL->tienePermiso('modificar_empresas') != true)
            header("location: index.php");
        /**
         * Muestra el menú en el que se pueden editar las empresas de manera
         * individual.
         * 
         */
        /* * ******************************************************************
         * LA SIGUIENTE CONSULTA Y CREACIÓN DE ARREGLOS DE MÉTODOS DE PAGO
         * ES TEMPORAL Y SERÁ SUSTITUIDO POR SU RESPECTIVA CLASE Y MÉTODO
         * ******************************************************************* */

        $arreglo = [];
        $cadena = sprintf('SELECT * FROM metodo WHERE estado = "Y" ORDER BY nombre ASC');
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            //$registro = new Registro($vSesion, $this->areaLog, 'Error recuperando todos los métodos de pago.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg());
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['metodo_id']] = ['nombre' => $reg->fields['nombre'], 'imagen' => $reg->fields['imagen'], 'codigo' => $reg->fields['codigo']];
                $reg->MoveNext();
            }
            $arreglo;
        }
        /*         * *******************************************************************
         * TERMINA CONSULTA TEMPORAL
         * ******************************************************************* */

        $empresaEditar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.empresa.editar.php');
        $empresaEditar->empresa_id = $datosEmpresa['empresa_id'];
        $empresaEditar->nombre = $datosEmpresa['nombre'];
        $empresaEditar->web = $datosEmpresa['web'];
        $empresaEditar->correo = $datosEmpresa['correo'];
        $empresaEditar->direccion = $datosEmpresa['direccion'];
        $empresaEditar->telefonos = json_decode($datosEmpresa['telefonos']);
        $empresaEditar->metodos = $arreglo;
        $empresaEditar->metodosAsignados = @$datosEmpresa['metodos'];
        $empresaEditar->ruta = '../' . RUTA_IMAGENES_DISENO . '/pagos/';
        $empresaEditar->publish();
    } elseif (isset($_GET['cambiarEstado'])) {
        /**
         * Muestra el menú para cambiar de estado a la empresa.
         */
        $empresaEstado = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.empresa.estado.php');
        $empresaEstado->empresa_id = $datosEmpresa['empresa_id'];
        $empresaEstado->nombre = $datosEmpresa['nombre'];
        $empresaEstado->estado = $datosEmpresa['estado'];
        $empresaEstado->publish();
    } else {
        /**
         * Muestra el menú en el que se pueden agregar empresas. También
         * despliega un listado de empresas a editar.
         * 
         */
        if (!isset($_GET['pag'])) {
            $pagina = 1;
        } else {
            $pagina = $sanyval->sanyval(filter_input(INPUT_GET, 'pag'), 'entero', 'entero');
        }

        if (empty($pagina)) {
            header("location: " . $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF']);
            exit();
        }
        $enlace = "" . $_SERVER['PHP_SELF'] . "?pag=";
        $paginar = new Paginado($obtopc->retornar('paginaresultados'), $enlace);
        //Recuperamos los registros sólo del usuario actual si no tiene permisos
        //para activar empresas.
        if ($ACL->tienePermiso('activar_empresas') == true) {
            $cadena = 'SELECT empresa_id, nombre, estado FROM empresa ORDER BY nombre';
        } else {
            $cadena = 'SELECT empresa_id, nombre, estado FROM empresa WHERE usuario_id = ' . $vSesion . ' ORDER BY nombre';
        }
        $datos = $paginar->retornar($cadena, $pagina);

        if ($ACL->tienePermiso('modificar_empresas') == true) {
            /********************************************************************
             * LA SIGUIENTE CONSULTA Y CREACIÓN DE ARREGLOS DE MÉTODOS DE PAGO
             * ES TEMPORAL Y SERÁ SUSTITUIDO POR SU RESPECTIVA CLASE Y MÉTODO
             ******************************************************************** */

            $arreglo = [];
            $cadena = sprintf('SELECT * FROM metodo WHERE estado = "Y" ORDER BY nombre ASC');
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                //$registro = new Registro($vSesion, $this->areaLog, 'Error recuperando todos los métodos de pago.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg());
            } else {
                while (!$reg->EOF) {
                    $arreglo[$reg->fields['metodo_id']] = ['nombre' => $reg->fields['nombre'], 'imagen' => $reg->fields['imagen'], 'codigo' => $reg->fields['codigo']];
                    $reg->MoveNext();
                }
                $arreglo;
            }
            /*********************************************************************
             * TERMINA CONSULTA TEMPORAL
             ******************************************************************** */
            $empresaAgregar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.empresa.agregar.php');
            $empresaAgregar->metodos = $arreglo;
            $empresaAgregar->publish();
        }

        if (!empty($datos)) {
            $empresaListar = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.empresa.listar.php');
            $empresaListar->datos = $datos;
            $empresaListar->paginar = $paginar->paginar($pagina);
            $empresaListar->activar_empresas = $ACL->tienePermiso('activar_empresas');
            $empresaListar->modificar_empresas = $ACL->tienePermiso('modificar_empresas');
            $empresaListar->publish();
        }
    }

    $pie = new Templater('../' . RUTA_RECURSOS . RUTA_VISTAS . 'manejo.pie.php');
    $pie->RUTA_JS = RUTA_JS;
    $pie->publish();
}