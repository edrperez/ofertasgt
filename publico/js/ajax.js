function nuevoAjax()
{
    var xmlhttp = false;
    try
    {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP", "Msxml2.XMLHTTP.4.0", "Msxml2.XMLH TTP.5.0", "Msxml2.XMLHTTP.3.0");
    }
    catch (e)
    {
        try
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (E)
        {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}


function enviarFormulario(url, formid, divrespuesta, redireccion) {
    document.getElementById(divrespuesta).innerHTML = "<h2 align='center'>Procesando...</h2>";
    var Formulario = document.getElementById(formid);
    var longitudFormulario = Formulario.elements.length;
    var cadenaFormulario = "";
    var sepCampos;
    sepCampos = "";

    for (var i = 0; i < longitudFormulario; i++)
    {
        if (Formulario.elements[i].tagName == "SELECT")
        {
            for (y = 0; y < Formulario.elements[i].options.length; y++)
            {
                if (Formulario.elements[i].options[y].selected)
                {
                    cadenaFormulario += Formulario.elements[i].name + "=" + escape(Formulario.elements[i].options[y].value) + "&";
                }
            }
        }
        else
        {
            if (Formulario.elements[i].type == 'checkbox' || Formulario.elements[i].type == 'radio')
            {
                if (Formulario.elements[i].checked == true)
                {
                    cadenaFormulario += Formulario.elements[i].name + "=" + escape(Formulario.elements[i].value) + "&";
                }
            }
            else
            {
                cadenaFormulario += Formulario.elements[i].name + "=" + escape(Formulario.elements[i].value) + "&";
            }
        }
    }
    peticion = nuevoAjax();
    peticion.open("POST", url, true);
    peticion.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    peticion.send(cadenaFormulario);
    peticion.onreadystatechange = function () {
        if (peticion.readyState == 4 && (peticion.status == 200 || window.location.href.indexOf("http") == -1)) {

            document.getElementById(divrespuesta).innerHTML = peticion.responseText;
            if (redireccion == 0) {
            } else {
                setTimeout("window.location='" + redireccion + "'", 3000);
            }

        }
    }
}

function enviarFormularioGet(url, divrespuesta) {
    var cadenaFormulario = "";
    peticion = nuevoAjax();
    peticion.open("GET", url, true);
    peticion.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    peticion.send(cadenaFormulario);
    peticion.onreadystatechange = function () {
        if (peticion.readyState == 4 && (peticion.status == 200 || window.location.href.indexOf("http") == -1)) {

            document.getElementById(divrespuesta).innerHTML = peticion.responseText;
        }
    }
}

function vete(milisegundos, redireccion) {
    setTimeout("window.location='" + redireccion + "'", milisegundos);
}

function resizeIframe(obj){
   {obj.style.height = 0;};
   {obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';}
}

function eliminarImagen(url, id, nombre) {
    var cadenaFormulario = "";
    cadenaFormulario += "id=" + id + "&";
    cadenaFormulario += "nombre=" + nombre + "&";
    peticion = nuevoAjax();
    peticion.open("POST", url, true);
    peticion.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    peticion.send(cadenaFormulario);
    peticion.onreadystatechange = function () {
        if (peticion.readyState == 4 && (peticion.status == 200 || window.location.href.indexOf("http") == -1)) {
            var fila = document.getElementById(id);
            fila.style.display = 'none';
        }
    }
}