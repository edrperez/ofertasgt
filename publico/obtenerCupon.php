<?php
require_once '../recursos/conf.php';

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//Obtenemos el número del cupón y consultamos si existe para generar su PDF
if (isset($_GET['cupon'])) {
    $cupon = $sanyval->sanyval(filter_input(INPUT_GET, 'cupon'), 'entero', 'entero');
    $recuperarCupon = new Venta();
    $datos = $recuperarCupon->recuperarCupon($cupon);
    require_once('html2pdf/html2pdf.class.php');
    $numero = $datos->fields['oferta_id'] . $datos->fields['venta_id'] . $cupon;
    if (empty($numero)) {
        exit;
    }
    ob_start();
    ?>
    <style type="text/css">
        <!--
        div.zone { border: none; border-radius: 6mm; background: #FFFFFF; border-collapse: collapse; padding:3mm; font-size: 2.7mm;}
        -->
    </style>
    <page format="100x200" orientation="L" backcolor="#C94D3A" style="font: arial;">
        <div style="rotate: 90; position: absolute; width: 100mm; height: 4mm; left: 195mm; top: 0; font-style: italic; font-weight: normal; text-align: center; font-size: 3mm;">
            Este es un cupón generado en <a href="http://ofertasgt.cu.cc/" style="color: #222222; text-decoration: none;">OfertasGT.cu.cc</a>
        </div>
        <table style="width: 99%;border: none;" cellspacing="4mm" cellpadding="0">
            <tr>
                <td colspan="2" style="width: 100%">
                    <div class="zone" style="height: 34mm;position: relative;font-size: 5mm;">
                        <div style="position: absolute; right: 3mm; top: 3mm; text-align: right; font-size: 4mm; ">
                            <img src="../publico/img/logo-70.png" alt="logo" style="margin-top: 3mm; margin-left: 20mm">
                        </div>
                        <div style="position: absolute; left: 3mm; top: 3mm; text-align: left; font-size: 4mm; ">
                            <u><?php echo $datos->fields['Oferta']; ?></u><br>
                            <strong><?php echo $datos->fields['Empresa']; ?></strong><br>
                            Teléfonos: <?php echo implode(', ', ((array) (object) json_decode($datos->fields['telefonos']))); ?><br><br>
                            Beneficiario: <strong><?php echo $datos->fields['usuario']; ?></strong><br>
                            Precio original: Q.<?php echo $datos->fields['precioo']; ?><br>
                            Precio con descuento: <strong>Q.<?php echo $datos->fields['preciod']; ?></strong><br>
                            Fecha de compra: <?php echo $datos->fields['Fecha']; ?><br>

                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">
                    <div class="zone" style="height: 40mm;vertical-align: middle;text-align: center;">
                        <barcode type="C39" value="<?php echo $numero; ?>" style="width: 37mm; border: none;" ></barcode>
                    </div>
                </td>
                <td style="width: 75%">
                    <div class="zone" style="vertical-align: middle; text-align: justify">
                        <strong>Condiciones:</strong><?php echo strip_tags($datos->fields['condiciones']); ?><br>
                    </div>
                </td>
            </tr>
        </table>
    </page>
    <?php
    $content = ob_get_clean();
    //Devolvemos al usuario el PDF conteniendo el cupón
    try {
        $html2pdf = new HTML2PDF('P', 'Letter', 'es', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('cupon-' . $numero . '.pdf', 'D');
    } catch (HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
} else {
    header("location: /");
    exit();
}