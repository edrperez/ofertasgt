<?php

require_once '../recursos/conf.php';
/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Llamar al encabezado.
 */
$listaCategoria = new Categoria();
$categorias = $listaCategoria->recuperarTodos();
$encabezado = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'encabezado.php');
$encabezado->RUTA_JS = RUTA_JS;
$encabezado->RUTA_CSS = RUTA_CSS;
$encabezado->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$encabezado->categorias = $categorias;
$encabezado->publish();

/**
 * Llamar los datos de la empresa y métodos de pago
 */
$id = $sanyval->sanyval(filter_input(INPUT_GET, 'ver'), 'entero', 'entero');
$datos = new Empresa();
$empresa = $datos->recuperar($id);

if (!is_array($empresa) || $empresa['estado'] == 'N') {
    header("location: /");
}

/**
 * Convertir el objeto stdClass a un arreglo normal
 */
$telefonos = implode(', ', ((array) (object) json_decode($empresa['telefonos'])));

//Recuperamos los datos de las ofertas de la empresa
$listaOferta = new Oferta();
$ofertas = $listaOferta->recuperarTodos(false, $id);

//Mostramos los datos de la empresa
$verEmpresa = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'verEmpresa.php');
$verEmpresa->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$verEmpresa->datos = $empresa;
$verEmpresa->telefonos = $telefonos;
$verEmpresa->publish();

//Mostramos los datos de las ofertas que tiene esa empresa
$listadoOfertas = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'verEmpresaOfertas.php');
$listadoOfertas->ofertas = $ofertas;
$listadoOfertas->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$listadoOfertas->publish();


/**
 * Llamar al pie
 */
$pie = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'pie.php');
$pie->publish();
