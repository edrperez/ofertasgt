<?php

require_once '../recursos/conf.php';

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//Recibimos el comando para pagar vía AJAX y formateamos el arreglo de datos
if (isset($_GET['pagar'])) {
    $valores = array();
    $valores['compradorN'] = filter_input(INPUT_POST, 'compradorN');
    $valores['nit'] = filter_input(INPUT_POST, 'nit');
    $valores['compradorC'] = filter_input(INPUT_POST, 'compradorC');
    $valores['telefono'] = filter_input(INPUT_POST, 'telefono');
    $valores['direccion'] = filter_input(INPUT_POST, 'direccion');
    $valores['totalDetalle'] = filter_input(INPUT_POST, 'totalDetalle');
    $datosOferta = new Oferta();

    for ($i = 0; $i < $valores['totalDetalle']; $i++) {
        $dato = $datosOferta->recuperar(filter_input(INPUT_POST, 'oferta' . $i), true);

        $valores['detalle'][$i] = ['oferta' => filter_input(INPUT_POST, 'oferta' . $i),
            'nombre' => filter_input(INPUT_POST, 'nombre' . $i),
            'correo' => filter_input(INPUT_POST, 'correo' . $i),
            'precioo' => $dato['precioo'],
            'preciod' => $dato['preciod'],
            'noferta' => $dato['nombre']];
        unset($dato);
    }

    //Pasamos los datos al método que realiza la venta
    $venta = new Venta();
    $error = $venta->agregarVenta($valores);
    if ($error == false) {
        unset($_SESSION['carrito']);
        ob_start();
        $contenidoFactura = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'facturaCorreo.php');
        $contenidoFactura->valores = $valores;
        echo $contenidoFactura->parse();
        $content = ob_get_clean();
        require_once('html2pdf/html2pdf.class.php');
        require 'phpmailer/PHPMailerAutoload.php';
        try {
            $html2pdf = new HTML2PDF('P', 'Letter', 'es');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            //$html2pdf->Output('factura.pdf', 'F');
            /* Habilitar la siguiente línea y comentar la anterior para generar una
             * cadena que se pueda adjuntar como documento PDF al correo.
             */
            $archivo = $html2pdf->Output('', true);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
        /* Configurar los datos del correo e instanciar PHPMailer. */
        $cuerpo = utf8_decode('Factura adjunta. <br><br><br>http://ofertasgt.cu.cc');
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = "localhost";
        $mail->Port = 25;
        $mail->SMTPAuth = true;
        $mail->Username = "envio@correo.com";
        $mail->Password = "clave";
        $mail->setFrom('envio@correo.com', 'OfertasGT');
        $mail->addReplyTo('envio@correo.com', 'OfertasGT');
        $mail->addAddress($valores['compradorC'], $valores['compradorN']);
        $mail->Subject = utf8_decode('Factura de compra en OfertasGT');
        $mail->msgHTML($cuerpo);
        $mail->AltBody = $cuerpo;
        $mail->AddStringAttachment($archivo, 'factura.pdf');

        //Realizar el envío del correo en si.
        if (!$mail->send()) {
            echo 'Error de envío de cupón: ' . $mail->ErrorInfo;
            $registro = new Registro($vSesion, 'Envío de factura', 'Error enviando cupón id del detalle de la venta: ' . $k . ' ' . $mail->ErrorInfo, 'Y');
        } else {
            echo 'Factura enviada.<br>';
            $registro = new Registro($vSesion, 'Envío de factura', 'Factura enviada.');
        }
        echo 'Venta realizada.';
        echo '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
    } else {
        echo $error;
    }
} else {
    /**
     * Llamar al encabezado.
     */
    $listaCategoria = new Categoria();
    $categorias = $listaCategoria->recuperarTodos();
    $encabezado = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'encabezado.php');
    $encabezado->RUTA_JS = RUTA_JS;
    $encabezado->RUTA_CSS = RUTA_CSS;
    $encabezado->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
    $encabezado->categorias = $categorias;
    $encabezado->publish();

    /**
     * Si no hay productos redirigir a la portada.
     */
    if (empty($_GET)) {
        header("location: /");
        exit();
    }

    /**
     * Llamar a la vista que genera el formulario para la factura y generamos
     * todos los datos para pasarlos a la vista.
     */
    $ofertas_id = [];
    $cantidades = [];
    $nombres = [];
    $tabla = [];
    $total = 0;
    foreach ($_GET as $k => $v) {
        $ofertas_id[$k] = $k;
        $cantidad[$k] = $v;
    }

    $datosOfertas = new Oferta();
    $datos = $datosOfertas->recuperarTodos(false, false, $ofertas_id);

    foreach ($datos as $k => $v) {
        $total += $v['preciod'] * $cantidad[$k];
        $nombres[$k] = $v['nombre'];
    }

    foreach ($nombres as $k => $v) {
        for ($i = 1; $i <= $cantidad[$k]; $i++) {
            $tabla[] = ['oferta' => $k, 'nombre' => $nombres[$k]];
        }
    }

    $factura = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'factura.php');
    $factura->total = $total;
    $factura->tabla = $tabla;
    $factura->publish();


    /**
     * Llamar al pie
     */
    $pie = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'pie.php');
    $pie->publish();
}