<?php

require_once '../recursos/conf.php';

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Llamar al encabezado.
 */
$listaCategoria = new Categoria();
$categorias = $listaCategoria->recuperarTodos();
$encabezado = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'encabezado.php');
$encabezado->RUTA_JS = RUTA_JS;
$encabezado->RUTA_CSS = RUTA_CSS;
$encabezado->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$encabezado->categorias = $categorias;
$encabezado->publish();
//Recogemos los resultados del formulario para ver las compras anteriores
if (isset($_GET['anterior'])) {
    $listarVentas = new Venta();
    //Estos 3 datos; NIT, correo y teléfono son necesarios para consultar los datos de un comprador
    $nit = filter_input(INPUT_POST, 'nit');
    $correo = @mysql_escape_string($sanyval->sanyval(filter_input(INPUT_POST, 'correo'), 'email', 'email'));
    $telefono = filter_input(INPUT_POST, 'telefono');
    $ventas = $listarVentas->recuperarAnteriores($nit, $correo, $telefono);
    if (empty($ventas)) {
        echo 'No existen ventas con esos datos.';
    } else {
        //Mostramos una tabla donde se listan todas las compras del cliente
        $formulario = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'verAnteriores.php');
        $formulario->ventas = $ventas;
        $formulario->publish();
    }
} else {
    //Formulario para consultar compras anteriores
    $formulario = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'formularioAnteriores.php');
    $formulario->publish();
}
/**
 * Llamar al pie
 */
$pie = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'pie.php');
$pie->publish();
