<?php

require_once '../recursos/conf.php';
/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Llamar al encabezado.
 */
$listaCategoria = new Categoria();
$categorias = $listaCategoria->recuperarTodos();
$encabezado = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'encabezado.php');
$encabezado->RUTA_JS = RUTA_JS;
$encabezado->RUTA_CSS = RUTA_CSS;
$encabezado->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$encabezado->categorias = $categorias;
$encabezado->publish();

/**
 * Llamar los datos de la oferta seleccionada, empresa y métodos de pago
 */
$oferta = $sanyval->sanyval(filter_input(INPUT_GET, 'ver'), 'entero', 'entero');
$datosOferta = new Oferta();
$datos = $datosOferta->recuperar($oferta, true);
$datosEmpresa = new Empresa();
$empresa = $datosEmpresa->recuperar($datos['empresa_id']);
$datosMetodo = new Metodo();
$metodos = $datosMetodo->recuperarTodos();

if (!is_array($datos) || $empresa['estado'] == 'N') {
    header("location: /");
}
//Cargamos las imágenes de la oferta para mostrar en el slide show
$imagenes = $datosOferta->recuperarImagenesOferta($oferta);

$verOferta = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'verOferta.php');
$verOferta->oferta = $oferta;
$verOferta->datos = $datos;
$verOferta->imagenes = $imagenes;
$verOferta->empresa = $empresa;
$verOferta->metodos = $metodos;
$verOferta->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$verOferta->publish();


/**
 * Llamar al pie
 */
$pie = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'pie.php');
$pie->publish();
