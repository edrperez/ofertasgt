<?php

require_once '../recursos/conf.php';
//Eliminamos el producto del carrito en sesión y reordenamos el arreglo
$limpiar = $sanyval->sanyval(filter_input(INPUT_GET, 'limpiar'), 'entero', 'entero');
if ($limpiar > 0) {
    $k = array_search($limpiar, $_SESSION['carrito']);
    if ($k !== false) {
        unset($_SESSION['carrito'][$k]);
    }
    $_SESSION["carrito"] = array_values($_SESSION["carrito"]);
    exit();
}

/*
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Llamar al encabezado.
 */
$listaCategoria = new Categoria();
$categorias = $listaCategoria->recuperarTodos();
$encabezado = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'encabezado.php');
$encabezado->RUTA_JS = RUTA_JS;
$encabezado->RUTA_CSS = RUTA_CSS;
$encabezado->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$encabezado->categorias = $categorias;
$encabezado->publish();

/**
 * Llamar los datos de la oferta seleccionada, empresa y métodos de pago
 */
$oferta = $sanyval->sanyval(filter_input(INPUT_GET, 'oferta'), 'entero', 'entero');

if ($oferta > 0) {
    $datosOferta = new Oferta();
    $datos = $datosOferta->recuperar($oferta, true);
    $datosEmpresa = new Empresa();
    $empresa = $datosEmpresa->recuperar($datos['empresa_id']);
    $datosMetodo = new Metodo();
    $metodos = $datosMetodo->recuperarTodos();

    if (!is_array($datos) || $empresa['estado'] == 'N') {
        header("location: /");
        exit();
    }
}
//Agregamos valores al carrito de la sesión
$_SESSION['carrito'][] = $oferta;
$_SESSION['carrito'] = array_filter(array_values(array_unique($_SESSION['carrito'])));

//Recuperamos los datos de las ofertas de los productos válidos en el carrito
if (!empty($_SESSION['carrito'])) {
    $datosOfertas = new Oferta();
    $datos = $datosOfertas->recuperarTodos(false, false, $_SESSION['carrito']);
} else {
    $datos = array();
}
//Llamamos a la vista que muestra el carrito
$verOferta = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'verCarrito.php');
$verOferta->ofertas = $datos;
$verOferta->RUTA_IMAGENES_DISENO = RUTA_IMAGENES_DISENO;
$verOferta->publish();


/**
 * Llamar al pie
 */
$pie = new Templater(RUTA_RECURSOS . RUTA_VISTAS . 'pie.php');
$pie->publish();
