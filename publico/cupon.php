<?php
/** Este script se debe ejecutar como un cron job cada minuto para no saturar
 * el servidor.
 * 
 * Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Declaramos los valores iniciales recuperando los datos de X cantidad de cupones,
 * luego llamamos a las librerías de HTML2PDF para generar el cupón en PDF y
 * PHPMailer para enviar el correo con el cupón adjunto. */
require_once '../recursos/conf.php';
$cupones = new Venta();
$pendientes = $cupones->recuperarCupones(1);
require_once('html2pdf/html2pdf.class.php');
require 'phpmailer/PHPMailerAutoload.php';
/* Generamos el número del cupón compuesto por la concatenación de:
 * - Id de la oferta.
 * - Id de la venta.
 * - Id del detalle de la venta.
 */
$numero = 0;
foreach ($pendientes as $k => $v) {
    $numero = $v['oferta_id'] . $v['venta_id'] . $k;
    ob_start();
    ?>
    <style type="text/css">
        <!--
        div.zone { border: none; border-radius: 6mm; background: #FFFFFF; border-collapse: collapse; padding:3mm; font-size: 2.7mm;}
        -->
    </style>
    <page format="100x200" orientation="L" backcolor="#C94D3A" style="font: arial;">
        <div style="rotate: 90; position: absolute; width: 100mm; height: 4mm; left: 195mm; top: 0; font-style: italic; font-weight: normal; text-align: center; font-size: 3mm;">
            Este es un cupón generado en <a href="http://ofertasgt.cu.cc/" style="color: #222222; text-decoration: none;">OfertasGT.cu.cc</a>
        </div>
        <table style="width: 99%;border: none;" cellspacing="4mm" cellpadding="0">
            <tr>
                <td colspan="2" style="width: 100%">
                    <div class="zone" style="height: 34mm;position: relative;font-size: 5mm;">
                        <div style="position: absolute; right: 3mm; top: 3mm; text-align: right; font-size: 4mm; ">
                            <img src="../publico/img/logo-70.png" alt="logo" style="margin-top: 3mm; margin-left: 20mm">
                        </div>
                        <div style="position: absolute; left: 3mm; top: 3mm; text-align: left; font-size: 3.5mm; ">
                            <u><?php echo $v['oferta']; ?></u><br>
                            <strong><?php echo $v['empresa']; ?></strong><br>
                            Teléfonos: <?php echo $v['telefonos']; ?><br><br>
                            Beneficiario: <strong><?php echo $v['usuario']; ?></strong><br>
                            Precio original: Q.<?php echo $v['precioo']; ?><br>
                            Precio con descuento: <strong>Q.<?php echo $v['preciod']; ?></strong><br>
                            Fecha de compra: <?php echo strip_tags(date('d-m-Y H:i',strtotime($v['fecha']))); ?><br>
                            Canjeable hasta: <?php echo strip_tags(date('d-m-Y H:i',strtotime($v['fechav']))); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">
                    <div class="zone" style="height: 40mm;vertical-align: middle;text-align: center;">
                        <barcode type="C39" value="<?php echo $numero; ?>" style="width: 37mm; border: none;" ></barcode>
                    </div>
                </td>
                <td style="width: 75%">
                    <div class="zone" style="vertical-align: middle; text-align: justify">
                        <strong>Condiciones:</strong><?php echo strip_tags($v['condiciones']); ?><br>
                    </div>
                </td>
            </tr>
        </table>
    </page>
    <?php
    $content = ob_get_clean();

    try {
        $html2pdf = new HTML2PDF('P', 'Letter', 'es', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        //$html2pdf->Output('cupon.pdf');
        /* Habilitar la siguiente línea y comentar la anterior para generar una
         * cadena que se pueda adjuntar como documento PDF al correo.
         */
        $archivo = $html2pdf->Output('', true);
    } catch (HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
    /* Comentar la siguiente línea para realizar el envío del correo. En el XAMPP
     * sobre Windows no deja enviar el correo.
     */
    //exit();
    /* Configurar los datos del correo e instanciar PHPMailer. */
    $cuerpo = utf8_decode('Hola ' . $v['usuario'] . ', adjunto encontrarás tu cupón número ' . $numero . '. <br><br><br>http://ofertasgt.cu.cc');
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = "localhost";
    $mail->Port = 25;
    $mail->SMTPAuth = true;
    $mail->Username = "envio@correo.com";
    $mail->Password = "clave";
    $mail->setFrom('envio@correo.com', 'OfertasGT');
    $mail->addReplyTo('envio@correo.com', 'OfertasGT');
    $mail->addAddress($v['correo'], $v['usuario']);
    $mail->Subject = utf8_decode('Cupón de OfertasGT');
    $mail->msgHTML($cuerpo);
    $mail->AltBody = $cuerpo;
    $mail->AddStringAttachment($archivo, 'cupon-' . $numero . '.pdf');

    //Realizar el envío del correo en si.
    if (!$mail->send()) {
        echo 'Error de envío de cupón: ' . $mail->ErrorInfo;
        $registro = new Registro($vSesion, 'Envío de cupones', 'Error enviando cupón id del detalle de la venta: ' . $k . ' ' . $mail->ErrorInfo, 'Y');
    } else {
        echo 'Cupón ' . $numero . ' enviado.';
		$sql = sprintf('UPDATE venta_detalle SET enviado = "Y" WHERE venta_detalle_id = %u', $k);
		$reg = $BD->Execute($sql);
		if (!$reg) {
            $registro = new Registro($vSesion, 'Envío de cupones', 'No se ha podido modificar el estado del envío del cupón: ' . $k . '. ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        $registro = new Registro($vSesion, 'Envío de cupones', 'Cupón ' . $numero . ' enviado.');
    }
}