# README #

Código del sitio elaborado para el proyecto final de Ingeniería del Software, Primer Semestre 2015, Universidad Mariano Gálvez de Guatemala. Demo en http://ofertasgt.cu.cc

El proyecto es un sitio Web de compra de cupones de productos y servicios.

### Materiales y herramientas ###
* Diseño de clases con ArgoUML: http://argouml.tigris.org
* Diseño del esquema de la base de datos con MySQL Workbench: http://dev.mysql.com/downloads/workbench/
* Diseño de la interfaz: Tema Simplex de Bootstrap http://bootswatch.com/simplex/
* Framework para varias llamadas de JavaScript con jQuery https://jquery.org/
* Plugin para elegir la fecha y hora con DateTimePicker http://xdsoft.net/jqplugins/datetimepicker/
* Editor WYSIWYG para los formularios con TinyMCE http://www.tinymce.com/
* El slide show de imágenes con http://bxslider.com/
* Cuenta regresiva con The Final Countdown http://hilios.github.io/jQuery.countdown/
* Ordenamiento de la tabla de registro de eventos con DataTables https://www.datatables.net/
* Generación de archivos PDF con HTML2PDF http://html2pdf.fr/es/default
* Envío de correos con PHPMailer https://github.com/PHPMailer/PHPMailer
* Procesamiento de plantillas con Templater
* Presentación con HTML5, CSS3 y JavaScript
* Lenguaje de programación PHP http://php.net/
* Servidor Web Apache http://httpd.apache.org/
* Servidor de Bases de Datos https://www.mysql.com/
* Desarrollo de la aplicación con NetBeans IDE https://netbeans.org/

### Contacto ###

* edrperez@gmail.com