<?php

/**
 * Administra el registro de eventos en el sistema, sean errores o no.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Registro {
    // --- ATTRIBUTES ---

    /**
     * El Id numérico del usuario, si hay, que ha generado la actividad.
     *
     * @access private
     * @var Integer
     */
    private $usuario = null;

    /**
     * El lugar donde se llevó a cabo el evento.
     *
     * @access private
     * @var String
     */
    private $area = null;

    /**
     * La descripción del evento.
     *
     * @access private
     * @var String
     */
    private $contenido = null;

    /**
     * Si es un error Y o sino es un error N.
     *
     * @access private
     * @var Boolean
     */
    private $error = null;

    // --- OPERATIONS ---

    /**
     * Constructor de la clase, se le pasan los valores iniciales y se asignan
     * a los de la clase. Luego se llama al método registrarEvento() para que
     * ejecute el proceso.
     *
     * @access public
     * @param  usuario
     * @param  area
     * @param  contenido
     * @param  error
     * @return mixed
     */
    public function __construct($usuario = 0, $area = null, $contenido = null, $error = 'N') {
        $this->usuario = $usuario;
        $this->area = $area;
        $this->contenido = $contenido;
        $this->error = $error;
        $this->registrarEvento();
    }

    /**
     * Realiza el registro del evento, usa los atributos asignados de la clase.
     * Si ocurre algún error lo imprime en pantalla, sino, no devuelve nada.
     *
     * @access private
     * @return mixed
     */
    private function registrarEvento() {
        global $BD;
        $ip = $this->obtenerAgentIp();
        if ($this->usuario == 0) {
            $sql = sprintf('INSERT INTO registro (area,contenido,fecha,ip,navegador,error) VALUES ("%s", "%s", "%s", "%s", "%s", "%s")', $this->area, @mysql_escape_string($this->contenido), date("Y-m-d H:i:s"), $ip['ip'], $ip['navegador'], $this->error);
        } else {
            $sql = sprintf('INSERT INTO registro (usuario_id,area,contenido,fecha,ip,navegador,error) VALUES (%u, "%s", "%s", "%s", "%s", "%s", "%s")', $this->usuario, $this->area, @mysql_escape_string($this->contenido), date("Y-m-d H:i:s"), $ip['ip'], $ip['navegador'], $this->error);
        }
        $reg = $BD->Execute($sql);

        if (!$reg) {
            printf('%s: %s <br />', $BD->ErrorNo(), $BD->ErrorMsg());
        }
    }

    /**
     * Obtiene la IP y el navegador del cliente que está generando el evento.
     * Devuelve un arreglo con ambos valores.
     *
     * @access private
     * @return mixed
     */
    public static function obtenerAgentIp() {
        if ($_SERVER) {
            if (@$_SERVER["HTTP_X_FORWARDED_FOR"]) {
                $ipReal = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } elseif (@$_SERVER["HTTP_CLIENT_IP"]) {
                $ipReal = $_SERVER["HTTP_CLIENT_IP"];
            } else {
                $ipReal = $_SERVER["REMOTE_ADDR"];
            }
        } else {
            if (getenv("HTTP_X_FORWARDED_FOR")) {
                $ipReal = getenv("HTTP_X_FORWARDED_FOR");
            } elseif (getenv("HTTP_CLIENT_IP")) {
                $ipReal = getenv("HTTP_CLIENT_IP");
            } else {
                $ipReal = getenv("REMOTE_ADDR");
            }
        }
        return array('navegador' => $_SERVER["HTTP_USER_AGENT"], 'ip' => $_SESSION['ipr'] = $ipReal);
    }

}

/* end of class Registro */