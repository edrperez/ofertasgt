<?php

/**
 * Administra los roles.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Rol {
    // --- ATTRIBUTES ---

    /**
     * Nombre del área para el log.
     *
     * @access public
     * @var String
     */
    private $areaLog = 'Manejo de roles';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Agrega o modifica un rol y agrega, modifica o elimina
     * los permisos de ese rol.
     *
     * @access public
     * @param  id
     * @param  nombre
     * @param  permisos
     * @return mixed
     */
    public function modificarRol($id, $nombre, $permisos = array()) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $idrol = $sanyval->sanyval($id, 'entero', 'entero');
        $nombre = utf8_encode(@mysql_escape_string(($sanyval->sanyval($nombre, 'plano', false))));
        $cadena = sprintf('SELECT nombre FROM rol WHERE rol_id = %u', $idrol);
        $sql = $BD->GetRow($cadena);
        if (count($sql) == 0) {
            $sql = sprintf('INSERT INTO rol(nombre) VALUES("%s")', $nombre);
        } else {
            $sql = sprintf('UPDATE rol SET nombre = "%s" WHERE rol_id = %u', $nombre, $idrol);
        }

        //$sql = sprintf('REPLACE INTO rol SET rol_id = %u, nombre = "%s"', $idrol, $nombre);
        $reg = $BD->Execute($sql);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD;
        } else {
            if ($BD->Insert_ID() == 0) {
                $idrol = $idrol;
            } else {
                $idrol = $BD->Insert_ID();
            }

            $reg = $BD->Execute(sprintf('DELETE FROM rol_permiso WHERE rol_id = %u', $idrol));
            foreach ($permisos as $k => $v) {
                if (substr($k, 0, 5) == "perm_") {
                    $idpermisos = str_replace("perm_", "", $k);
                    if ($v == 'X') {
                        $sql = sprintf("DELETE FROM rol_permiso WHERE rol_id = %u AND permiso_id = %u", $idrol, $idpermisos);
                        $reg = $BD->Execute($sql);
                        if (!$reg) {
                            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                            return ERROR_BD;
                        }
                        continue;
                    }
                    $sql = sprintf("INSERT INTO rol_permiso(rol_id, permiso_id, valor, fecha) VALUES(%u, %u, %u, '%s')", $idrol, $idpermisos, $v, date("Y-m-d H:i:s"));
                    //$sql = sprintf("REPLACE INTO rol_permiso SET rol_id = %u, permiso_id = %u, valor = %u, fecha = '%s'", $idrol, $idpermisos, $v, date("Y-m-d H:i:s"));
                    $reg = $BD->Execute($sql);
                    if (!$reg) {
                        $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                        return ERROR_BD;
                    }
                }
            }
            if (empty($id)) {
                $registro = new Registro($vSesion, $this->areaLog, 'El rol ha sido agregado: ' . $nombre);
                return 'El rol ha sido agregado. <img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'El rol ha sido editado : ' . $id . '.');
                return 'El rol ha sido editado. <img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }
        }
    }

    /**
     * Recibe el id de un rol y elimina dicho rol, también
     * elimina la asignación de ese rol al usuario y los
     * permisos asignados a ese rol.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function eliminarRol($id) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $idrol = $sanyval->sanyval($id, 'entero', 'entero');
        $sql = sprintf("DELETE FROM rol WHERE rol_id = %u LIMIT 1", $idrol);
        $reg = $BD->Execute($sql);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            $respuesta.= ERROR_BD;
        } else {
            $registro = new Registro($vSesion, $this->areaLog, 'Se ha eliminado el rol: ' . $idrol . '.');
            $respuesta.= 'Se ha eliminado el rol.<br />';
        }
        $sql = sprintf("DELETE FROM rol_usuario WHERE rol_id = %u", $idrol);
        $reg = $BD->Execute($sql);
        if (!$reg) {
            $respuesta.= ERROR_BD;
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            $respuesta.= 'Se han eliminado las asignaciones de este rol a los usuarios.<br />';
            $registro = new Registro($vSesion, $this->areaLog, 'Se han eliminado las asignaciones de este rol a los usuarios: ' . $idrol . '.');
        }
        $sql = sprintf("DELETE FROM rol_permiso WHERE rol_id = %u", $idrol);
        $reg = $BD->Execute($sql);
        if (!$reg) {
            $respuesta.= ERROR_BD;
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            $respuesta.= 'Se han eliminado las asignaciones de este rol a los permisos.<br />';
            $registro = new Registro($vSesion, $this->areaLog, 'Se han eliminado las asignaciones de este rol a los permisos: ' . $idrol . '.');
        }
        $respuesta.= '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
        return $respuesta;
    }

}

/* end of class Rol */