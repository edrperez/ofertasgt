<?php

/**
 * Consulta los valores de permisos, roles por usuario o en general.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class ACL {
    // --- ATTRIBUTES ---

    /**
     * Arreglo de permisos de la forma llave y valor.
     *
     * @access private
     * @var Array
     */
    private $perms = array();

    /**
     * Id del usuario sobre el cual se está trabajando, su valor por defecto es
     *
     * @access private
     * @var Integer
     */
    private $id = 0;

    /**
     * Arreglo con los roles asignados a un usuario.
     *
     * @access private
     * @var Array
     */
    private $usuarioRoles = array();

    /**
     * Nombre del área para el log.
     *
     * @access private
     * @var String
     */
    private $areaLog = 'ACL';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase que recibe usuario si existe, llama a
     * para agregar roles a usuarioRoles, luego llama al método construirACL()
     *
     * @access public
     * @param  idus
     * @return mixed
     */
    public function __construct($idus = '') {
        if ($idus != '') {
            $this->id = floatval($idus);
        } else {
            $this->id = 0;
        }
        $this->usuarioRoles = $this->obtUsuarioRoles();
        $this->construirACL();
    }

    /**
     * Contruye el arreglo con los permisos asignados a un usuario y a sus
     *
     * @access private
     * @return mixed
     */
    private function construirACL() {
        if (count($this->usuarioRoles) > 0) {
            $this->perms = array_merge($this->perms, $this->obtRolPermisos($this->usuarioRoles));
        }
        $this->perms = array_merge($this->perms, $this->obtUsuarioPermisos($this->id));
    }

    /**
     * Recibe el id de un permiso y devuelve el nombre llave (slug) del permiso.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function obtPermisosLlaveDeId($id) {
        global $BD;
        $cadena = sprintf('SELECT llave FROM permiso WHERE permiso_id = %u', floatval($id));
        $sql = $BD->GetRow($cadena);
        if ($BD->ErrorNo() > 0) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        return $sql[0];
    }

    /**
     * Recibe el id de un permiso y devuelve el nombre del permiso.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function obtPermisosNombreDeId($id) {
        global $BD;
        $cadena = sprintf('SELECT nombre FROM permiso WHERE permiso_id = %u', floatval($id));
        $sql = $BD->GetRow($cadena);
        if ($BD->ErrorNo() > 0) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        return $sql[0];
    }

    /**
     * Recibe el id de un rol y devuelve el nombre del rol.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function obtRolNombreDeId($id) {
        global $BD;
        $cadena = sprintf('SELECT nombre FROM rol WHERE rol_id = %u', floatval($id));
        $sql = $BD->GetRow($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        return $sql[0];
    }

    /**
     * Devuelve un arreglo con con los id de los roles asignados a un usuario.
     *
     * @access public
     * @return mixed
     */
    public function obtUsuarioRoles() {
        global $BD;
        $cadena = sprintf('SELECT * FROM rol_usuario WHERE usuario_id = %u ORDER BY fecha ASC', floatval($this->id));
        $sql = $BD->Execute($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        $resp = array();
        while (!$sql->EOF) {
            $resp[] = $sql->fields['rol_id'];
            $sql->MoveNext();
        }
        return $resp;
    }

    /**
     * Obtiene todos los roles en forma de llave y valor o en formato 'completo'
     * genera un id y el nombre del rol por cada item del arreglo.
     *
     * @access public
     * @param  formato
     * @return mixed
     */
    public function obtTodosRoles($formato = '') {
        $formato = strtolower($formato);
        global $BD;
        $cadena = sprintf('SELECT * FROM rol ORDER BY nombre ASC');
        $sql = $BD->Execute($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        $resp = array();
        while (!$sql->EOF) {
            if ($formato == 'completo') {
                $resp[] = array("id" => $sql->fields['rol_id'], "nombre" => $sql->fields['nombre']);
            } else {
                $resp[] = $sql->fields['rol_id'];
            }
            $sql->MoveNext();
        }
        return $resp;
    }

    /**
     * Obtiene todos los permisos en forma de llave y valor o en formato
     * que genera un id y el nombre del permiso por cada item del arreglo.
     *
     * @access public
     * @param  formato
     * @return mixed
     */
    public function obtTodosPermisos($formato = '') {
        $formato = strtolower($formato);
        global $BD;
        $cadena = sprintf('SELECT * FROM permiso ORDER BY nombre ASC');
        $sql = $BD->Execute($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        $resp = array();
        while (!$sql->EOF) {
            if ($formato == 'completo') {
                $resp[$sql->fields['llave']] = array('id' => $sql->fields['permiso_id'], 'nombre' => $sql->fields['nombre'], 'llave' => $sql->fields['llave']);
            } else {
                $resp[] = $sql->fields['permiso_id'];
            }
            $sql->MoveNext();
        }
        return $resp;
    }

    /**
     * Recibe un arreglo que contiene todos los roles a evaluar, devuelve un
     * con los valores de los permisos para esos roles con su valor, nombre,
     * herencia e id de cada permiso.
     *
     * @access public
     * @param  rol
     * @return mixed
     */
    public function obtRolPermisos($rol) {
        if (is_array($rol)) {
            $rolSQL = sprintf('SELECT * FROM rol_permiso WHERE rol_id IN (%s) ORDER BY rol_permiso_id ASC', implode(",", $rol));
        } else {
            $rolSQL = sprintf('SELECT * FROM rol_permiso WHERE rol_id = %u ORDER BY rol_permiso_id ASC', floatval($rol));
        }
        global $BD;
        $sql = $BD->Execute($rolSQL);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        $perms = array();
        while (!$sql->EOF) {
            $pLl = strtolower($this->obtPermisosLlaveDeId($sql->fields['permiso_id']));
            if ($pLl == '') {
                continue;
            }
            if ($sql->fields['valor'] === '1') {
                $pVa = true;
            } else {
                $pVa = false;
            }
            $perms[$pLl] = array('perm' => $pLl, 'heredado' => true, 'valor' => $pVa, 'nombre' => $this->obtPermisosNombreDeId($sql->fields['permiso_id']), 'id' => $sql->fields['permiso_id']);
            $sql->MoveNext();
        }
        return $perms;
    }

    /**
     * Recibe el id de un usuario y devuelve los permisos especiales asignados
     * usuario en forma de arreglo con su valor, nombre, slug, herencia e id de
     * permiso.
     *
     * @access private
     * @param  id
     * @return mixed
     */
    private function obtUsuarioPermisos($id) {
        global $BD;
        $cadena = sprintf('SELECT * FROM permiso_usuario WHERE usuario_id = %u ORDER BY fecha ASC', floatval($id));
        $sql = $BD->Execute($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        $perms = array();
        while (!$sql->EOF) {
            $pLl = strtolower($this->obtPermisosLlaveDeId($sql->fields['permiso_id']));
            if ($pLl == '') {
                continue;
            }
            if ($sql->fields['valor'] == '1') {
                $pVa = true;
            } else {
                $pVa = false;
            }
            $perms[$pLl] = array('perm' => $pLl, 'heredado' => false, 'valor' => $pVa, 'nombre' => $this->obtPermisosNombreDeId($sql->fields['permiso_id']), 'id' => $sql->fields['permiso_id']);
            $sql->MoveNext();
        }
        return $perms;
    }

    /**
     * Recibe un arreglo con roles, estos se comparan uno a uno con los roles
     * atritubto usuarioRoles y devuelve True si el rol a comparar existe.
     *
     * @access public
     * @param  rol
     * @return mixed
     */
    public function usuarioTieneRol($rol) {
        foreach ($this->usuarioRoles as $k => $v) {
            if (floatval($v) === floatval($rol)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Recibe el slug del permiso y verifica si el usuario lo tiene asignado,
     * True si es así.
     *
     * @access public
     * @param  llave
     * @return mixed
     */
    public function tienePermiso($llave) {
        $llave = strtolower($llave);
        if (array_key_exists($llave, $this->perms)) {
            if ($this->perms[$llave]['valor'] === '1' || $this->perms[$llave]['valor'] === true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Utiliza el atribudo id y devuelve el nombre del usuario.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function obtUsuario($id) {
        global $BD;
        $cadena = sprintf('SELECT usuario FROM usuario WHERE usuario_id = %u', floatval($id));
        $sql = $BD->GetRow($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        return $sql[0];
    }

    /**
     * Devuelve los valores del atributo perms.
     *
     * @access public
     * @return mixed
     */
    public function obtPermisos() {
        return $this->perms;
    }

}

/* end of class ACL */