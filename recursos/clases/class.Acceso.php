<?php

/**
 * Administra el acceso y sesion de usuarios.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Acceso {
    // --- ATTRIBUTES ---

    /**
     * Nombre del área para el registro de eventos, su valor por defecto es
     *
     * @access private
     * @var String
     */
    private $areaLog = 'Acceso';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Recibe dos cadenas, una contiene el usuario y la otra una clave. Realiza
     * entre el usuario y la clave, mínimo de longitud de clave y si son
     * Devuelve un arreglo con el usuario, clave y un valor si es válido, de lo
     * devuelve un arreglo con error.
     *
     * @access public

     * @param  usuario
     * @param  clave
     * @return mixed
     */
    public function validar($usuario, $clave = 'guatemala99') {
        global $obtopc;
        $claveMinima = $obtopc->retornar('claveminima');
        if ($usuario == $clave) {
            return array('valido' => 0, 'mensaje' => 'La clave y el usuario no pueden ser iguales.');
        }
        if (!ctype_alnum($usuario)) {
            return array('valido' => 0, 'mensaje' => 'El usuario debe contener sólo letras y números.');
        }
        if (ctype_alnum($clave)) {
            if (strlen($clave) >= $claveMinima && preg_match('/\d/', $clave)) {
                
            } else {
                return array('valido' => 0, 'mensaje' => sprintf('La clave debe ser de un mínimo de %s carácteres y contener letras y números.', $claveMinima));
            }
        } else {
            return array('valido' => 0, 'mensaje' => 'La clave debe contener sólo letras y números.');
        }
        return array('valido' => 1, 'usuario' => $usuario, 'clave' => $clave);
    }

    /**
     * Consulta si el usuario existe o no en la base de datos, devuelve un
     *
     * @access public
     * @param  usuario
     * @return mixed
     */
    public function usuarioExiste($usuario) {
        global $BD;
        $sql = sprintf('SELECT usuario FROM usuario WHERE usuario LIKE "%s"', $usuario);
        $reg = $BD->GetRow($sql);
        if (count($reg) > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * Recibe dos cadenas, una contiene el usuario y la otra una clave.
     * Verifica en la base de datos que el usuario y la clave concuerden,
     * comprueba el número de intento de inicio de sesión y devuelve el id
     * del usuario si todo sale bien, de lo contrario devuelve 0.
     *
     * @access private
     * @param  usuario
     * @param  clave
     * @return mixed
     */
    public function verificarAcceso($usuario, $clave) {
        global $BD;
        global $obtopc;
        $maxintento = $obtopc->retornar('intentos');
        $valorAcceso = 0;
        $clave = $this->generarClave($usuario, $clave);
        $sql = sprintf('SELECT usuario_id, usuario, clave, intento FROM usuario WHERE (usuario LIKE "%s" AND estado = "Y")', $usuario);
        $reg = $BD->GetRow($sql);
        if (count($reg) > 0) {
            $intento = $reg['intento'];
            if ($clave == $reg['clave']) {
                $valorAcceso = $reg['usuario_id'];
                $intento = 0;
                $sql1 = sprintf('UPDATE usuario SET intento = %u, uacceso = "%s" WHERE usuario_id = %u', $intento, date("Y-m-d H:i:s"), $reg['usuario_id']);
                $reg1 = $BD->GetRow($sql1);
                if (!$reg1) {
                    
                }
                $registro = new Registro($reg['usuario_id'], $this->areaLog, 'Sesión iniciada.');
            } else {
                $intento++;
                if ($intento >= $maxintento) {
                    $sql1 = sprintf('UPDATE usuario SET intento = %u, estado = "N" WHERE usuario_id = %u', $intento, $reg['usuario_id']);
                } else {
                    $sql1 = sprintf('UPDATE usuario SET intento = %u WHERE usuario_id = %u', $intento, $reg['usuario_id']);
                }
                $reg1 = $BD->GetRow($sql1);
                if (!$reg1) {
                    
                }
                $registro = new Registro($reg['usuario_id'], $this->areaLog, 'Intento de inicio de sesión con el usuario ' . $usuario . '.');
            }
        }
        return $valorAcceso;
    }

    /**
     * Recibe tres cadenas, una contiene el usuario, la otra una clave y la
     * última la información del navegador del cliente e IP del cliente. Inicia
     * una sesión o actualiza la sesión del cliente para evitar la desconexión
     * por límite de tiempo.
     *
     * @access public
     * @param  usuario
     * @param  clave
     * @param  ageip
     * @return mixed
     */
    public function iniciarSesion($usuario, $clave, $ageip) {
        global $BD;
        $vAcceso = $this->validar($usuario, $clave);
        if ($vAcceso['valido'] == 0) {
            exit($vAcceso['mensaje']);
        }

        $usuario_id = $this->verificarAcceso($usuario, $clave);

        if ($usuario_id > 0) {
            $sql = sprintf('SELECT sesion_id, usuario_id FROM sesion WHERE sesion_id = "%s" AND usuario_id = %u', session_id(), $usuario_id);
            $reg = $BD->GetRow($sql);
            if (count($reg) > 0) {
                $sql = sprintf('UPDATE sesion SET uacceso = "%s", datos = "%s" WHERE sesion_id = "%s" AND usuario_id = %u', date("Y-m-d H:i:s"), @mysql_escape_string($ageip), session_id(), $usuario_id);
            } else {
                $sql = sprintf('INSERT INTO sesion SET sesion_id = "%s", usuario_id = %u, uacceso = "%s", datos = "%s"', session_id(), $usuario_id, date("Y-m-d H:i:s"), @mysql_escape_string($ageip));
            }

            $reg = $BD->Execute($sql);

            if (!$reg) {
                echo ERROR_BD;
                $registro = new Registro($usuario_id, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            } else {
                if (file_exists('../' . RUTA_IMAGENES_DISENO . 'working.gif')) {
                    echo '<img src="../' . RUTA_IMAGENES_DISENO . 'working.gif" onLoad="vete(1000,\'index.php\')" />';
                } else {
                    echo '<img src="' . RUTA_IMAGENES_DISENO . 'working.gif" onLoad="vete(1000,\'index.php\')" />';
                }
            }
        }
        return $usuario_id;
    }

    /**
     * Obtiene el tiempo mínimo de una sesión para desconectar, verifica y
     * compara los valores de la sesión y actualiza o elimina dicha sesión
     * acorde. Devuelve el id del usuario o 0 si no hay o se venció la sesión
     * activa.
     *
     * @access public
     * @return mixed
     */
    public function verificarSesion() {
        global $BD;
        global $obtopc;
        $acceder = 0;
        $minssesion = (($obtopc->retornar('minssesion')) * 60);
        $sql = sprintf('SELECT sesion_id, usuario_id, uacceso FROM sesion WHERE sesion_id = "%s"', session_id());
        $reg = $BD->GetRow($sql);
        if (count($reg) > 0) {
            $diferencia = (time() - strtotime($reg['uacceso']));
            if ($diferencia >= $minssesion) {
                $sql1 = sprintf('DELETE FROM sesion WHERE sesion_id = "%s"', session_id());
                $reg1 = $BD->Execute($sql1);
                if (!$reg1) {
                    echo ERROR_BD;
                    $registro = new Registro($reg['usuario_id'], $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                }
                $registro = new Registro($reg['usuario_id'], $this->areaLog, 'Sesión terminada por inactividad.');
            } else {
                $sql1 = sprintf('SELECT usuario_id FROM usuario WHERE (usuario_id = %u AND estado = "Y")', $reg['usuario_id']);
                $reg1 = $BD->GetRow($sql1);
                if (count($reg1) > 0) {
                    $acceder = $reg['usuario_id'];
                    $sql2 = sprintf('UPDATE sesion SET uacceso = "%s" WHERE sesion_id = "%s"', date("Y-m-d H:i:s"), session_id());
                    $reg2 = $BD->Execute($sql2);
                    if (!$reg2) {
                        echo ERROR_BD;
                        $registro = new Registro($reg['usuario_id'], $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                    }
                    $this->ultimaSesion($reg['usuario_id']);
                }
            }
        }
        return $acceder;
    }

    /**
     * Recibe el id del usuario y elimina todas las sesion no cerradas del
     * mismo y reinicia los intento de conexión del usuario. Al terminar
     * redirige a la página inicial.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function cerrarSesiones($id) {
        global $BD;
        global $log;
        $sql = sprintf('DELETE FROM sesion WHERE usuario_id = %u', $id);
        $reg = $BD->Execute($sql);
        $this->ultimaSesion($id);
        if (!$reg) {
            echo ERROR_BD;
            $registro = new Registro($id, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            $infoSesion = session_get_cookie_params();
            if ((empty($infoSesion['domain'])) && (empty($infoSesion['secure']))) {
                setcookie(session_name(), '', time() - 3600, $infoSesion['path']);
            } elseif (empty($infoSesion['secure'])) {
                setcookie(session_name(), '', time() - 3600, $infoSesion['path'], $infoSesion['domain']);
            } else {
                setcookie(session_name(), '', time() - 3600, $infoSesion['path'], $infoSesion['domain'], $infoSesion['secure']);
            }
            session_destroy();
            $sql1 = sprintf('UPDATE usuario SET intento = 0 WHERE usuario_id = %u', $id);
            $reg1 = $BD->GetRow($sql1);
            if (!$reg1) {
                
            }
            $registro = new Registro($id, $this->areaLog, 'Sesión terminada.');
            header("location: index.php");
        }
    }

    /**
     * Recibe el id de un usuario actualiza la hora y fecha de su última
     * sesión.
     *
     * @access private
     * @param  id
     * @return mixed
     */
    public function ultimaSesion($id) {
        global $BD;
        $sql = sprintf('UPDATE usuario SET usesion = "%s" WHERE usuario_id = %u', date("Y-m-d H:i:s"), $id);
        $reg = $BD->Execute($sql);
        if (!$reg) {
            echo ERROR_BD;
            $registro = new Registro($id, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
    }

    /**
     * Recibe dos cadenas, una contiene el usuario y la otra una clave. Genera
     * una clave a partir de unificar los hash MD5 del usuario y un hash SHA1
     * de la clave, luego genera un hash SHA1 sobre el resultado. Devuelve este
     * último hash.
     *
     * @access public
     * @param  usuario
     * @param  clave
     * @return mixed
     */
    public function generarClave($usuario, $clave) {
        return sha1(md5($usuario) . sha1($clave));
    }

}

/* end of class Acceso */