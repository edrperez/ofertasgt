<?php

/**
 * Administra las ofertas.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Oferta {
    // --- ATTRIBUTES ---

    /**
     * Id de la oferta.
     *
     * @access private
     * @var Integer
     */
    private $id = null;

    /**
     * Nombre de la oferta.
     *
     * @access private
     * @var Integer
     */
    private $nombre = null;

    /**
     * Nombre del área para el log.
     *
     * @access private
     * @var String
     */
    private $areaLog = 'Manejo de Ofertas';

    /**
     * Conjunto de valores a actualizar de la oferta.
     *
     * @access public
     * @var Array
     */
    private $valores = array();

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Agregar una nueva oferta; devuelve un mensaje
     * si algo salió mal, de lo contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function agregar() {
        global $BD;
        global $sanyval;
        global $vSesion;

        $resumen = @mysql_escape_string($this->valores['resumen']);
        $descripcion = @mysql_escape_string($this->valores['descripcion']);
        $condiciones = @mysql_escape_string($this->valores['condiciones']);
        $empresa_id = @mysql_escape_string($this->valores['empresa_id']);
        $fechaf = @mysql_escape_string($this->valores['fechaf']);
        $fechav = @mysql_escape_string($this->valores['fechav']);
        $precioo = @mysql_escape_string($this->valores['precioo']);
        $preciod = @mysql_escape_string($this->valores['preciod']);
        $categoria_id = @mysql_escape_string($this->valores['categoria_id']);
        $cadena = sprintf('INSERT INTO oferta (nombre, estado, empresa_id, imagen, fechai, fecham, fechaf, fechav, descripcion, resumen, condiciones, precioo, preciod, categoria_id) VALUES ("%s", "N", %u, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", %u, %u, %u)', $this->nombre, $empresa_id, $this->imagen, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $fechaf . ':00', $fechav . ':00', $descripcion, $resumen, $condiciones, $precioo, $preciod, $categoria_id);

        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            $registro = new Registro($vSesion, $this->areaLog, 'La oferta ha sido agregada: ' . $this->nombre . ' ' . $BD->Insert_ID());
            return true;
        }
    }

    /**
     * Modifica una oferta; devuelve un mensaje si
     * algo salió mal, de lo contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function modificar() {
        global $BD;
        global $vSesion;

        $resumen = @mysql_escape_string($this->valores['resumen']);
        $descripcion = @mysql_escape_string($this->valores['descripcion']);
        $condiciones = @mysql_escape_string($this->valores['condiciones']);
        $empresa_id = @mysql_escape_string($this->valores['empresa_id']);
        $fechaf = @mysql_escape_string($this->valores['fechaf']);
        $fechav = @mysql_escape_string($this->valores['fechav']);
        $precioo = @mysql_escape_string($this->valores['precioo']);
        $preciod = @mysql_escape_string($this->valores['preciod']);
        $categoria_id = @mysql_escape_string($this->valores['categoria_id']);
        $cadena = sprintf('UPDATE oferta SET nombre = "%s", imagen = "%s", empresa_id = %u, fecham = "%s", fechaf = "%s", fechav = "%s", descripcion = "%s", resumen = "%s", condiciones = "%s", precioo = %u, preciod = %u, categoria_id = %u WHERE oferta_id = %u', $this->nombre, $this->imagen, $empresa_id, date("Y-m-d H:i:s"), $fechaf . ':00', $fechav . ':00', $descripcion, $resumen, $condiciones, $precioo, $preciod, $categoria_id, $this->id);
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            $registro = new Registro($vSesion, $this->areaLog, 'Oferta editada: ' . $this->id . '|' . $this->nombre . '.');
            return true;
        }
    }

    /**
     * Cambia el estado de una oferta entre
     * activado y desactivado. Devuelve un mensaje
     * si algo salió mal, de lo contrario devuelve true.
     *
     * @access public
     * @param  id
     * @param  estado
     * @return mixed
     */
    public function cambiarEstado($id, $estado) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $id = $sanyval->sanyval($id, 'entero', 'entero');

        if ($id != false) {
            if ($estado == 'Y' || $estado == 'N') {
                $sql = sprintf('UPDATE oferta SET estado = "%s", fecham = "%s" WHERE oferta_id = %u', $estado, date("Y-m-d H:i:s"), $id);
            } else {
                return 'El estado debe ser Y o N.';
            }
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {
                if ($estado == 'Y') {
                    $registro = new Registro($vSesion, $this->areaLog, 'Oferta activada: ' . $id . '.');
                    return 'Oferta activada.';
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'Oferta desactivada: ' . $id . '.');
                    return 'Oferta desactivada.';
                }
            }
        }
    }

    /**
     * Devuelve los datos de una sóla oferta; devuelve
     * un mensaje si algo salió mal, de lo contrario
     * devuelve un arreglo.
     *
     * @access public
     * @param  id
     * @param activo
     * @return mixed
     */
    public function recuperar($id, $activo = false) {
        global $BD;
        global $vSesion;
        if ($activo == true) {
            $cadena = sprintf('SELECT * FROM oferta WHERE (oferta_id = %u AND estado = "Y" AND fechaf > "%s") ORDER BY fecham ASC', $id, date("Y-m-d H:i:s"));
        } else {
            $cadena = sprintf('SELECT * FROM oferta WHERE oferta_id = %u', $id);
        }
        $reg = $BD->GetRow($cadena);
        if (!$reg) {
			$registro = new Registro($vSesion, $this->areaLog, 'Error recuperando: ' . $this->id . '. ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            return $reg;
        }
    }

    /**
     * Devuelve los datos de todas las ofertas; devuelve
     * un mensaje si algo salió mal, de lo contrario devuelve
     * un arreglo multidimensional con todos los valores.
     *
     * @access public
     * @param categoria
     * @param empresa
     * @param ofertas
     * @return mixed
     */
    public function recuperarTodos($categoria = false, $empresa = false, $ofertas = array()) {
        global $BD;
        global $vSesion;
        $arreglo = [];
        if ($categoria != false) {
            $cadena = sprintf('SELECT * FROM oferta WHERE (estado = "Y" AND fechaf > "' . date("Y-m-d H:i:s") . '" AND categoria_id = ' . $categoria . ') ORDER BY fecham ASC');
        } elseif ($empresa != false) {
            $cadena = sprintf('SELECT * FROM oferta WHERE (estado = "Y" AND fechaf > "' . date("Y-m-d H:i:s") . '" AND empresa_id = ' . $empresa . ') ORDER BY fecham ASC');
        } elseif (!empty($ofertas)) {
            $cadena = sprintf('SELECT * FROM oferta WHERE (estado = "Y" AND fechaf > "' . date("Y-m-d H:i:s") . '" AND oferta_id IN(\'' . implode('\',\'', $ofertas) . '\')) ORDER BY fecham ASC');
        } else {
            $cadena = sprintf('SELECT * FROM oferta WHERE (estado = "Y" AND fechaf > "' . date("Y-m-d H:i:s") . '") ORDER BY fecham ASC');
        }

        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando todas las ofertas.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['oferta_id']] = ['nombre' => $reg->fields['nombre'],
                    'imagen' => $reg->fields['imagen'],
                    'resumen' => $reg->fields['resumen'],
                    'precioo' => $reg->fields['precioo'],
                    'preciod' => $reg->fields['preciod'],
                    'empresa_id' => $reg->fields['empresa_id'],
                    'categoria_id' => $reg->fields['categoria_id']];
                $reg->MoveNext();
            }
            return $arreglo;
        }
    }

    /**
     * Devuelve el resultado de agregar o modificar una
     * oferta. Si el nombre es el mismo y concuerda
     * con el id entonces actualiza, sino concuerda entonces da error.
     *
     * @access public
     * @param  id
     * @param  nombre
     * @param  imagen
     * @param  valores
     * @return mixed
     */
    public function agregarOferta($id, $nombre, $imagen, $valores = array()) {
        global $sanyval;
        $respuesta = '';
        if ($nombre == "") {
            return 'Debe incluir un nombre.';
        } else {
            $this->id = @mysql_escape_string(($sanyval->sanyval($id, 'plano', false)));
            $this->nombre = @mysql_escape_string(($sanyval->sanyval($nombre, 'plano', false)));
            $this->imagen = $imagen;
            $this->valores = $valores;
            if ($id > 0) {
                $accion = $this->modificar();
                if ($accion == 1)
                    $respuesta.= 'Oferta modificada.';
            } else {
                $accion = $this->agregar();
                if ($accion == 1)
                    $respuesta.= 'Oferta agregada.';
            }
            if ($accion != 1) {
                $respuesta.= $accion;
            } else {
                $respuesta.='<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="parent.vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }
        }
        return $respuesta;
    }

    /**
     * Recibe el id de una oferta y un arreglo con los nombres de los archivos
     * de imagen que se van a almacenar.
     *
     * @access private
     * @param id
     * @param valores
     * @return mixed
     */
    public function agregarImagenes($id, $valores = array()) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';

        $id = @mysql_escape_string(($sanyval->sanyval($id, 'plano', false)));
        foreach ($valores as $k => $v) {
            $cadena = sprintf('INSERT INTO imagen (oferta_id, nombre, modificado) VALUES (%u, "%s", "%s")', $id, $v, date("Y-m-d H:i:s"));
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                @unlink('../' . RUTA_IMAGENES_DISENO . "ofertas/" . $v);
                $respuesta.= ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg() . '<br>';
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'La imagen ha sido agregada: ' . $v . ' ' . $BD->Insert_ID());
                $respuesta.= 'Agregada: ' . $k . '<br>';
            }
        }
        $respuesta.='<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="parent.vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
        return $respuesta;
    }

    /**
     * Recibe el id de una oferta y devuelve un arreglo con el id de cada
     * imagen y su nombre.
     *
     * @access public
     * @param id
     * @return mixed
     */
    public function recuperarImagenesOferta($id) {
        global $BD;
        global $vSesion;
        $arreglo = [];

        $cadena = sprintf('SELECT imagen_id, nombre FROM imagen WHERE oferta_id = %u ORDER BY modificado DESC', $id);
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando imágenes de la oferta: ' . $id . ' .' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['imagen_id']] = ['nombre' => $reg->fields['nombre']];
                $reg->MoveNext();
            }
            return $arreglo;
        }
    }

    /**
     * Recibe el id de una imagen y elimina su registro en la BDD y el archivo
     * en el disco.
     *
     * @access public
     * @param id
     * @param nombre
     * @return mixed
     */
    public function eliminarImagen($id, $nombre) {
        global $BD;
        global $vSesion;

        $cadena = sprintf('DELETE FROM imagen WHERE imagen_id = %u', $id);
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error eliminando imagen: ' . $id . ' .' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            $registro = new Registro($vSesion, $this->areaLog, 'Imagen : ' . $id . ' eliminada.');
            @unlink('../' . RUTA_IMAGENES_DISENO . "ofertas/" . $nombre);
        }
    }

}

/* end of class Oferta */