<?php

/**
 * Registra las ventas.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Venta {
    // --- ATTRIBUTES ---

    /**
     * Nombre del área para el log.
     *
     * @access public
     * @var String
     */
    private $areaLog = 'Realización de venta';

    // --- OPERATIONS ---

    /**
     * Recibe los datos de una vez y los agrega a la BDD. Llama a
     * agregarDetalle() y le pasa los datos del detalle. Si hay algún
     * problema devuelve error, sino elimina los datos insertados.
     *
     * @access public
     * @param  valores
     * @return mixed
     */
    public function agregarVenta($valores = array()) {
        global $BD;
        global $sanyval;
        global $vSesion;
        global $ageip;

        $ip = Registro::obtenerAgentIp();
        $verificar = false;
        $error = true;

        $compradorN = @mysql_escape_string(($sanyval->sanyval($valores['compradorN'], 'plano', false)));
        $compradorC = @mysql_escape_string($sanyval->sanyval($valores['compradorC'], 'email', 'email'));
        $nit = @mysql_escape_string($sanyval->sanyval($valores['nit'], 'plano', false));
        $telefono = @mysql_escape_string($sanyval->sanyval($valores['telefono'], 'entero', 'entero'));
        $direccion = @mysql_escape_string($sanyval->sanyval($valores['direccion'], 'plano', false));

        if ($compradorC == false) {
            return 'El correo del comprador no parece ser válido.';
        }

        for ($i = 0; $i < $valores['totalDetalle']; $i++) {
            $verificar = @mysql_escape_string($sanyval->sanyval($valores['detalle'][$i]['correo'], 'email', 'email'));
            if ($verificar == false) {
                return 'El correo del beneficiario <strong>' . ($i + 1) . '</strong> no parece ser válido.';
            }
        }

        $cadena = sprintf('INSERT INTO venta (nombre, nit, telefono, correo, direccion, fechai, fecham, ip, navegador) VALUES ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")', $compradorN, $nit, $telefono, $compradorC, $direccion, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $ip['ip'], $ip['navegador']);
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            $venta_id = $BD->Insert_ID();
            $registro = new Registro($vSesion, $this->areaLog, 'Venta agregada: ' . $BD->Insert_ID());
            $error = $this->agregarDetalle($venta_id, $valores['detalle']);
        }
        return $error;
    }

    /**
     * Recibe los datos del detalle y los agrega a la BDD. Si hay algún
     * problema devuelve error y elimina los datos ingresados.
     *
     * @access private
     * @param venta
     * @param valores
     * @return mixed
     */
    private function agregarDetalle($venta, $valores = array()) {
        global $BD;
        global $vSesion;
        $error = false;

        foreach ($valores as $k => $v) {
            $cadena = sprintf('INSERT INTO venta_detalle (venta_id, oferta_id, usuario, correo, precioo, preciod) VALUES (%u, %u, "%s", "%s", %u, %u)', $venta, $v['oferta'], $v['nombre'], $v['correo'], $v['precioo'], $v['preciod']);
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                $error = true;
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'Detalle de venta agregado: ' . $BD->Insert_ID());
            }
        }
        return $error;
    }

    /**
     * Devuelve los datos de X cupones procesadas; devuelve un arreglo multidimensional.
     *
     * @access public
     * @param limite
     * @return mixed
     */
    public function recuperarCupones($limite = 5) {
        global $BD;
        global $vSesion;
        $arreglo = [];
        $cadena = sprintf('SELECT venta_detalle.venta_detalle_id, venta_detalle.venta_id, venta_detalle.oferta_id,
            venta_detalle.usuario,venta_detalle.precioo,venta_detalle.correo, venta_detalle.preciod, oferta.nombre as Oferta,
            venta.fechai as Fecha, empresa.nombre as Empresa, oferta.condiciones, empresa.telefonos, oferta.fechav FROM venta_detalle
            INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
            JOIN venta ON venta.venta_id = venta_detalle.venta_id
            JOIN empresa ON empresa.empresa_id = oferta.empresa_id
            WHERE enviado = "N" ORDER BY venta_detalle_id ASC LIMIT %u', $limite);
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando cupones.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['venta_detalle_id']] = ['venta_id' => $reg->fields['venta_id'],
                    'oferta_id' => $reg->fields['oferta_id'],
                    'usuario' => $reg->fields['usuario'],
                    'correo' => $reg->fields['correo'],
                    'precioo' => $reg->fields['precioo'],
                    'preciod' => $reg->fields['preciod'],
                    'oferta' => $reg->fields['Oferta'],
                    'fecha' => $reg->fields['Fecha'],
                    'fechav' => $reg->fields['fechav'],
                    'empresa' => $reg->fields['Empresa'],
                    'condiciones' => $reg->fields['condiciones'],
                    'telefonos' => $telefonos = implode(', ', ((array) (object) json_decode($reg->fields['telefonos'])))];
                $reg->MoveNext();
            }
            return $arreglo;
        }
    }

    /**
     * Devuelve los datos de una sola venta; devuelve un mensaje si algo
     * salió mal, de lo contrario devuelve un arreglo.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function recuperarVenta($id) {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT * FROM venta WHERE venta_id = %u', $id);
        $reg = $BD->GetRow($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando: ' . $id);
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            return $reg;
        }
    }

    /**
     * Devuelve los datos del detalle de una sola venta; devuelve un mensaje
     * si algo salió mal, de lo contrario devuelve un arreglo.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function recuperarDetalle($id) {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT venta_detalle.oferta_id, venta_detalle.venta_detalle_id,
            venta_detalle.preciod, venta_detalle.usuario, venta_detalle.correo, oferta.nombre as oferta,
            venta_detalle.cobrado
            FROM venta_detalle INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
            JOIN venta ON venta.venta_id = venta_detalle.venta_id
            JOIN empresa ON empresa.empresa_id = oferta.empresa_id
            WHERE venta_detalle.venta_id = %u AND empresa.usuario_id = %u', $id, $vSesion);
        $reg1 = $BD->Execute($cadena);
        if (!$reg1) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando: ' . $id);
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            while (!$reg1->EOF) {
                $reg[$reg1->fields['venta_detalle_id']] = ['oferta_id' => $reg1->fields['oferta_id'],
                    'oferta_id' => $reg1->fields['oferta_id'],
                    'preciod' => $reg1->fields['preciod'],
                    'usuario' => $reg1->fields['usuario'],
                    'correo' => $reg1->fields['correo'],
                    'oferta' => $reg1->fields['oferta'],
                    'venta_id' => $id,
                    'canjeado' => $reg1->fields['cobrado']];
                $reg1->MoveNext();
            }
            return $reg;
        }
    }

    /**
     * Devuelve los datos del detalle de las ventas anteriores para que el usuario
     * pueda volver a imprimir sus cupones.
     *
     * @access public
     * @param nit
     * @param correo
     * @param telefono
     * @return mixed
     */
    public function recuperarAnteriores($nit, $correo, $telefono) {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT venta_detalle.oferta_id, venta_detalle.venta_id, venta_detalle.venta_detalle_id,
            venta_detalle.usuario, venta_detalle.preciod, empresa.nombre as empresa, oferta.nombre as oferta,
            venta.fechai as fecha, venta_detalle.cobrado FROM venta_detalle
            INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
            JOIN venta ON venta.venta_id = venta_detalle.venta_id
            JOIN empresa ON empresa.empresa_id = oferta.empresa_id
            WHERE venta.nit = "%s" AND venta.correo ="%s" AND venta.telefono ="%s"', $nit, $correo, $telefono);
        $reg1 = $BD->Execute($cadena);
        if (!$reg1) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando venta anterior.', 'Y');
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            if ($reg1->RecordCount() > 0) {
                while (!$reg1->EOF) {

                    $reg[$reg1->fields['venta_detalle_id']] = ['oferta_id' => $reg1->fields['oferta_id'],
                        'venta_id' => $reg1->fields['venta_id'],
                        'oferta_id' => $reg1->fields['oferta_id'],
                        'preciod' => $reg1->fields['preciod'],
                        'usuario' => $reg1->fields['usuario'],
                        'empresa' => $reg1->fields['empresa'],
                        'oferta' => $reg1->fields['oferta'],
                        'fecha' => $reg1->fields['fecha'],
                        'cobrado' => $reg1->fields['cobrado'],];
                    $reg1->MoveNext();
                }
                return $reg;
            }
        }
    }

    /**
     * Devuelve los datos de 1 cupón en específico; devuelve un arreglo multidimensional.
     *
     * @access public
     * @param id
     * @return mixed
     */
    public function recuperarCupon($id) {
        global $BD;
        global $vSesion;
        //Si hay vistas disponibles
        //$cadena = sprintf('SELECT * FROM consultar_cupones WHERE cupon = %u', $id);
        //Si no hay vistas disponibles
        $cadena = sprintf('SELECT venta_detalle.venta_detalle_id, venta_detalle.venta_id, venta_detalle.oferta_id,
            venta_detalle.usuario,venta_detalle.precioo,venta_detalle.correo, venta_detalle.preciod, oferta.nombre as Oferta,
            venta.fechai as Fecha, empresa.nombre as Empresa, oferta.condiciones, empresa.telefonos FROM venta_detalle
            INNER JOIN oferta ON oferta.oferta_id = venta_detalle.oferta_id
            JOIN venta ON venta.venta_id = venta_detalle.venta_id
            JOIN empresa ON empresa.empresa_id = oferta.empresa_id
            WHERE venta_detalle.venta_detalle_id = %u', $id);
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando cupón: ' . $id . $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        } else {
            return $reg;
        }
    }

}

/* end of class Venta */