<?php

/**
 * Pagina los resultados de una consulta SQL.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Paginado {
    // --- ATTRIBUTES ---

    /**
     * Número máximo de elementos por página, su valor por defecto es 30.
     *
     * @access private
     * @var Integer
     */
    private $por_pagina = 30;

    /**
     * En enlace original agregado al paginado para crear el enlace final.
     *
     * @access private
     * @var String
     */
    private $enlace = null;

    /**
     * Máximo de páginas en un momento dado, su valor por defecto es 10.
     *
     * @access private
     * @var Integer
     */
    private $maxpags = 10;

    /**
     * Total de registros a paginar según la consulta, su valor por defecto es 0.
     *
     * @access private
     * @var Integer
     */
    private $total = 0;

    // --- OPERATIONS ---

    /**
     * Constructor de la clase que asigna los atributos.
     *
     * @access public
     * @param  por_pagina
     * @param  enlace
     * @param  maxpags
     * @return mixed
     */
    public function __construct($por_pagina, $enlace, $maxpags = 10) {
        $this->por_pagina = $por_pagina;
        $this->enlace = $enlace;
        $this->maxpags = $maxpags;
    }

    /**
     * Recibe el valor de la página actual y genera todo el listado de páginas
     * con su enlaces. Si el número de página que le fue pasado es válido
     * entonces devuelve la cadena con los números de página y enlaces, de lo
     * contrario redirige a la primera página.
     *
     * @access public
     * @param  actual
     * @return mixed
     */
    public function paginar($actual) {
        $total_paginas = ceil($this->total / $this->por_pagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        $minimo = $this->maxpags ? max(1, $actual - ceil($this->maxpags / 2)) : 1;
        $maximo = $this->maxpags ? min($total_paginas, $actual + floor($this->maxpags / 2)) : $total_paginas;
        if ($actual <= $total_paginas) {
            $texto = '<ul class="pagination">';
            if ($actual > 1)
                $texto .= '<li><a href="' . $this->enlace . $anterior . '">&laquo;</a></li>';
            else
                $texto .= '<li class="disabled"><a href="#">&laquo;</a></li>';
            if ($minimo != 1)
                $texto .= '... ';
            for ($i = $minimo; $i < $actual; $i++)
                $texto .= '<li><a href="' . $this->enlace . $i . '">' . $i . '</a></li>';
            $texto .= '<li class="active"><a href="#">' . $actual . '</a></li>';
            for ($i = $actual + 1; $i <= $maximo; $i++)
                $texto .= '<li><a href="' . $this->enlace . $i . '">' . $i . '</a></li>';
            if ($maximo != $total_paginas)
                $texto .= '... ';
            if ($actual < $total_paginas)
                $texto .= '<li><a href="' . $this->enlace . $posterior . '">&raquo;</a></li>';
            else
                $texto .= '<li class="disabled"><a href="#">&raquo;</a></li>';
            $texto .= '</ul>';
            return $texto;
        } else {
            header("location: index.php");
            exit();
        }
    }

    /**
     * Ejecuta la consulta y crea un arreglo en forma de llave y valor con lo
     * que se hace la paginación.
     *
     * @access private
     * @param  cadena
     * @param  pagina
     * @return mixed
     */
    private function ejecutarConsulta($cadena, $pagina = 1) {
        global $BD;
        $respuesta = array();
        $arregloT = array();
        $reg1 = ($pagina - 1) * $this->por_pagina;
        $cadena = sprintf($cadena);
        $sql = $BD->Execute($cadena);
        $this->total = $sql->RecordCount();
        if ($this->total > 0) {

            for ($i = $reg1; $i < min($reg1 + $this->por_pagina, $this->total); $i++) {
                $sql->Move($i);
                foreach ($sql->fields as $key => $value) {
                    $arregloT[$key] = $value;
                }
                $respuesta[$i] = $arregloT;
            }
            return $respuesta;
        }
    }

    /**
     * Llama a ejecutarConsulta() y devuelve la respuesta de ese método.
     *
     * @access public
     * @param  cadena
     * @param  pagina
     * @return mixed
     */
    public function retornar($cadena, $pagina) {
        $respuesta = $this->ejecutarConsulta($cadena, $pagina);
        return $respuesta;
    }

}

/* end of class Paginado */