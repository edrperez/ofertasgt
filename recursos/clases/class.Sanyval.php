<?php

/**
 * Clase para sanear y validar los datos. 
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY;
  without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class SanyVal {
    // --- ATTRIBUTES ---

    /**
     * El valor a trabajar.
     *
     * @access private
     * @var String
     */
    private $valor = null;

    /**
     * El nombre del tipo de saneamiento.
     *
     * @access private
     * @var String
     */
    private $sanear = null;

    /**
     * El nombre del tipo de validación.
     *
     * @access private
     * @var String
     */
    private $validar = null;

    // --- OPERATIONS ---

    /**
     * Se aceptan los valores y se ejecuta el saneamiento y validación si se
     * solicita. Primero se sanea, luego se valida, devuelve el resultado de
     * ambos métodos y se pasa trim en cada uno de ellos sólo por la paranoia
     * reinante.
     *
     * @access public
     * @param  valor
     * @param  sanear
     * @param  validad
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Sanea los datos usando filter_var dependiendo el filtro que se le pase.
     *
     * @access private
     * @param  valor
     * @param  opcion
     * @return mixed
     */
    private function sanear() {
        if ($this->sanear == false) {
            
        } else {
            $opcion = strtolower(trim($this->sanear));
            $valor = urldecode(trim($this->valor));
            switch ($opcion) {
                case 'plano':
                    $valor = filter_var($valor, FILTER_SANITIZE_STRING);
                    break;
                case 'email':
                    $valor = filter_var($valor, FILTER_SANITIZE_EMAIL);
                    break;
                case 'entero':
                    $valor = filter_var($valor, FILTER_SANITIZE_NUMBER_INT);
                    break;
                case 'decimal':
                    $valor = filter_var($valor, FILTER_SANITIZE_NUMBER_FLOAT, array('flags' =>
                        FILTER_FLAG_ALLOW_FRACTION, 'options' => '.'));
                    break;
                case 'especiales':
                    $valor = filter_var($valor, FILTER_SANITIZE_SPECIAL_CHARS);
                    break;
                case 'url':
                    $valor = filter_var($valor, FILTER_SANITIZE_URL);
                    break;
            }
        }
        $this->valor = $valor;
    }

    /**
     * Valida los datos usando filter_var dependiendo el filtro que se le pase.
     *
     * @access private
     * @author firstname and lastname of author, <author@example.org>
     * @param  valor
     * @param  opcion
     * @return mixed
     */
    private function validar() {
        if ($this->validar == false) {
            
        } else {
            $opcion = strtolower(trim($this->validar));
            switch ($opcion) {
                case 'email':
                    $fValidar = FILTER_VALIDATE_EMAIL;
                    break;
                case 'entero':
                    $fValidar = FILTER_VALIDATE_INT;
                    break;
                case 'decimal':
                    $fValidar = FILTER_VALIDATE_FLOAT;
                    break;
                case 'url':
                    $fValidar = FILTER_VALIDATE_URL;
                    break;
            }

            $this->valor = filter_var($this->valor, $fValidar);
        }
    }

    /**
     * Se aceptan los valores y se el saneamiento y validación si se solicita.
     * Primero se sanea, luego se valida, devuelve el resultado de ambos métodos
     * y se pasa trim en cada uno de ellos sólo por la paranoia reinante.
     * 
     * @param mixed $valor El valor a sanear.
     * @param string $sanear El tipo de saneamiento.
     * @param string $validar El tipo de validación.
     * @return mixed
     */
    public function sanyval($valor, $sanear, $validar) {
        $this->valor = $valor;
        $this->sanear = $sanear;
        $this->validar = $validar;
        $this->sanear();
        $this->validar();

        return $this->valor;
    }

}
