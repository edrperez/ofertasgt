<?php

/**
 * Obtiene el valor de la opción consultada.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Opcion {
    // --- ATTRIBUTES ---

    /**
     * Opcion a consultar.
     *
     * @access private
     * @var String
     */
    private $opcion = null;

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @param  opcion
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Realiza la consulta SQL en base al valor del atributo opcion.
     *
     * @access private
     * @return mixed
     */
    private function ejecutarConsulta() {
        global $BD;
        global $log;
        $cadena = sprintf('SELECT valor FROM opcion WHERE nombre LIKE "%s"', $this->opcion);
        $sql = $BD->GetRow($cadena);
        if (!$sql) {
            echo ERROR_BD;
            $registro = new Registro(0, 'Obtener opción', $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
        }
        return $sql[0];
    }

    /**
     * Asigna la variable al atributo opcion y llama
     * a ejecutarConsulta() para devolver el valor
     * de la opción.
     *
     * @access public
     * @param  opcion
     * @return mixed
     */
    public function retornar($opcion) {
        $this->opcion = $opcion;
        $respuesta = $this->ejecutarConsulta();

        return $respuesta;
    }

}

/* end of class Opcion */