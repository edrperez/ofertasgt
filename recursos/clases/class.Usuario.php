<?php

/**
 * Administración de usuarios.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Usuario {
    // --- ATTRIBUTES ---

    /**
     * Nombre del área para el log.
     *
     * @access private
     * @var String
     */
    private $areaLog = 'Manejo de Usuarios';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Recibe los valores de usuario, clave y correo para insertar un nuevo
     * usuario en el sistema.
     *
     * @access public
     * @param  usuario
     * @param  clave
     * @param  correo
     * @return mixed
     */
    public function agregarUsuario($usuario, $clave, $correo) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $usuario = $sanyval->sanyval($usuario, 'plano', false);
        $clave = $sanyval->sanyval($clave, 'plano', false);
        $correo = $sanyval->sanyval($correo, 'email', 'email');

        if ($correo == false) {
            return 'El correo no parece ser válido.';
        }

        $Acceso = new Acceso();
        $vAcceso = $Acceso->validar($usuario, $clave);
        if ($vAcceso['valido'] == 0) {
            return $vAcceso['mensaje'];
        }

        $usuarioExiste = $Acceso->usuarioExiste($vAcceso['usuario']);
        if ($usuarioExiste == 1) {
            return 'Ese usuario ya existe.';
        }
        $clave = $Acceso->generarClave($vAcceso['usuario'], $vAcceso['clave']);

        $sql = sprintf('INSERT INTO usuario (usuario,clave,correo,uacceso) VALUES ("%s", "%s", "%s","%s")', $usuario, $clave, $correo, date("Y-m-d H:i:s"));

        $reg = $BD->Execute($sql);

        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD;
        } else {
            $registro = new Registro($vSesion, $this->areaLog, 'Nuevo usuario agregado: ' . $usuario . '.');
            return 'Nuevo usuario agregado. <img src="../' . RUTA_IMAGENES_DISENO . 'working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
        }
    }

    /**
     * Modifica el usuario identificado por el atributo id y actualiza los
     * valores de usuario, clave y correo con los atributos.
     *
     * @access public
     * @param  id
     * @param  usuario
     * @param  clave
     * @param  correo
     * @return mixed
     */
    public function modificar($id, $usuario, $clave, $correo) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $usuario = @mysql_escape_string(($sanyval->sanyval($usuario, 'plano', false)));
        $clave = @mysql_escape_string($sanyval->sanyval($clave, 'plano', false));
        $correo = @mysql_escape_string($sanyval->sanyval($correo, 'email', 'email'));
        $idus = $sanyval->sanyval($id, 'entero', 'entero');

        $Acceso = new Acceso();
        if (empty($clave)) {
            $vAcceso = $Acceso->validar($usuario);
        } else {
            $vAcceso = $Acceso->validar($usuario, $clave);
        }

        if ($vAcceso['valido'] == 0) {
            return $vAcceso['mensaje'];
        }
        if ($correo == false) {
            return 'El correo no parece ser válido.';
        }

        if ($idus != false) {
            if (empty($clave)) {
                $sql = sprintf('UPDATE usuario SET correo = "%s" WHERE usuario_id = %u', $correo, $idus);
            } else {
                $clave = $Acceso->generarClave($vAcceso['usuario'], $vAcceso['clave']);
                $sql = sprintf('UPDATE usuario SET clave = "%s", correo = "%s" WHERE usuario_id = %u', $clave, $correo, $idus);
            }

            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {

                $registro = new Registro($vSesion, $this->areaLog, 'Usuario editado: ' . $idus . '|' . $usuario . '.');
                return 'Usuario editado. <img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }
        }
    }

    /**
     * Cambia el estado (activo/inactivo) de un usuario al recibir los datos de
     * id y estado.
     *
     * @access public
     * @param  id
     * @param  estado
     * @return mixed
     */
    public function cambiarEstado($id, $estado) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $idus = $sanyval->sanyval($id, 'entero', 'entero');

        if ($idus != false) {
            if ($estado == 'Y') {
                $sql = sprintf('UPDATE usuario SET estado = "%s", intento = 0 WHERE usuario_id = %u', $estado, $idus);
            } else {
                $sql = sprintf('UPDATE usuario SET estado = "%s" WHERE usuario_id = %u', $estado, $idus);
            }
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {
                if ($estado == 'Y') {
                    $registro = new Registro($vSesion, $this->areaLog, 'Usuario activado: ' . $idus . '.');
                    return 'Usuario activado.';
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'Usuario desactivado: ' . $idus . '.');
                    return 'Usuario desactivado.';
                }
            }
        }
    }

    /**
     * Agrega, modifica o elimina un rol a un usuario.
     *
     * @access public
     * @param  id
     * @param  roles
     * @return mixed
     */
    public function asignarRoles($id, $roles = array()) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $idus = $sanyval->sanyval($id, 'entero', 'entero');
        $respuesta = '';
        $reg = $BD->Execute(sprintf('DELETE FROM rol_usuario WHERE usuario_id = %u', $idus));
        foreach ($roles as $k => $v) {
            if (substr($k, 0, 4) == "rol_") {
                $idroles = str_replace("rol_", "", $k);
                if ($v == '0' || $v == 'x') {
                    $sql = sprintf('DELETE FROM rol_usuario WHERE usuario_id = %u AND rol_id = %u', $idus, $idroles);
                } else {
                    $sql = sprintf('REPLACE INTO rol_usuario SET usuario_id = %u, rol_id = %u, fecha = "%s"', $idus, $idroles, date("Y-m-d H:i:s"));
                }

                $reg = $BD->Execute($sql);
                if (!$reg) {
                    $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                    return ERROR_BD;
                } else {
                    $respuesta.= 'Rol del usuario modificado.<br />';
                    $registro = new Registro($vSesion, $this->areaLog, 'Rol del usuario modificado. : ' . $idus . '.');
                }
            }
        }
        $respuesta.= '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
        return $respuesta;
    }

    /**
     * Agrega, modifica o elimina un permiso a un usuario.
     *
     * @access public
     * @param  id
     * @param  permisos
     * @return mixed
     */
    public function asignarPermisos($id, $permisos = array()) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $idus = $sanyval->sanyval($id, 'entero', 'entero');
        $reg = $BD->Execute(sprintf('DELETE FROM permiso_usuario WHERE usuario_id = %u', $idus));
        foreach ($permisos as $k => $v) {
            if (substr($k, 0, 5) == "perm_") {
                $idpermisos = str_replace("perm_", "", $k);
                if ($v == 'x') {
                    $sql = sprintf('DELETE FROM permiso_usuario WHERE usuario_id = %u AND permiso_id = %u', $idus, $idpermisos);
                } else {
                    $sql = sprintf('REPLACE INTO permiso_usuario SET usuario_id = %u, permiso_id = %u, valor = %u, fecha = "%s"', $idus, $idpermisos, $v, date("Y-m-d H:i:s"));
                }

                $reg = $BD->Execute($sql);
                if (!$reg) {
                    $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                    return ERROR_BD;
                } else {
                    $respuesta.= 'Permiso del usuario modificado.<br />';
                    $registro = new Registro($vSesion, $this->areaLog, 'Permiso del usuario modificado: ' . $idus . ', permiso: ' . $idpermisos);
                }
            }
        }
        $respuesta.= '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
        return $respuesta;
    }

}

/* end of class Usuario */