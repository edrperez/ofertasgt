<?php

/**
 * Administra los datos de las empresas.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Empresa {
    // --- ATTRIBUTES ---

    /**
     * Id de la empresa.
     *
     * @access public
     * @var Integer
     */
    public $id = null;

    /**
     * Nombre de la empresa.
     *
     * @access public
     * @var String
     */
    public $nombre = null;

    /**
     * Conjunto de valores a actualizar de la empresa
     * como teléfono, correo electrónico, etc.
     *
     * @access public
     * @var Array
     */
    public $valores = array();

    /**
     * Nombre del área para el log.
     *
     * @access public
     * @var String
     */
    private $areaLog = 'Manejo de empresas';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Agregar un nueva empresa; devuelve un mensaje si algo salió mal, de lo
     * contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function agregar() {
        global $BD;
        global $sanyval;
        global $vSesion;
        $cadena = sprintf('SELECT nombre FROM empresa WHERE nombre LIKE "%s"', $this->nombre);
        $reg = $BD->GetRow($cadena);
        if (!empty($reg)) {
            $resultado = 'La empresa ' . $reg['nombre'] . ' ya existe.';
            return $resultado;
        } else {
            $telefonos = utf8_encode(@mysql_escape_string(json_encode($this->valores['telefonos'])));
            $correo = $sanyval->sanyval(utf8_encode(@mysql_escape_string($this->valores['correo'])), 'email', 'email');
            if ($correo == false) {
                return 'El correo no parece ser válido.';
            }
            $web = $sanyval->sanyval(utf8_encode(@mysql_escape_string($this->valores['web'])), 'url', 'url');
            if ($web == false) {
                return 'El sitio web no parece ser válido.';
            }
            $direccion = utf8_encode(@mysql_escape_string($this->valores['direccion']));
            $cadena = sprintf('INSERT INTO empresa (nombre, estado, usuario_id, modificado, telefonos, correo, web, direccion) VALUES ("%s", "N", %u, "%s", "%s", "%s", "%s", "%s")', $this->nombre, $vSesion, date("Y-m-d H:i:s"), $telefonos, $correo, $web, $direccion);
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
            } else {
                $this->id = $BD->Insert_ID();
                $registro = new Registro($vSesion, $this->areaLog, 'La empresa ha sido agregada: ' . $this->nombre . ' ' . $BD->Insert_ID());
                $this->modificarMetodosPago();
                return true;
            }
        }
    }

    /**
     * Modifica una empresa; devuelve un mensaje si algo salió mal, de lo
     * contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function modificar() {
        global $BD;
        global $sanyval;
        global $vSesion;
        $cadena = sprintf('SELECT empresa_id, nombre FROM empresa WHERE nombre LIKE "%s"', $this->nombre);
        $reg = $BD->GetRow($cadena);
        if (!empty($reg) && $reg['empresa_id'] != $this->id) {
            $resultado = 'La empresa ' . $this->nombre . ' ya existe.';
            return $resultado;
        } else {
            $telefonos = utf8_encode(@mysql_escape_string(json_encode($this->valores['telefonos'])));
            $correo = $sanyval->sanyval(utf8_encode(@mysql_escape_string($this->valores['correo'])), 'email', 'email');
            if ($correo == false) {
                return 'El correo no parece ser válido.';
            }
            $web = $sanyval->sanyval(utf8_encode(@mysql_escape_string($this->valores['web'])), 'url', 'url');
            if ($web == false) {
                return 'El sitio web no parece ser válido.';
            }
            $direccion = utf8_encode(@mysql_escape_string($this->valores['direccion']));
            $this->modificarMetodosPago();
            $cadena = sprintf('UPDATE empresa SET nombre = "%s", usuario_id = %u, modificado ="%s", telefonos ="%s", correo ="%s", web ="%s", direccion ="%s" WHERE empresa_id = %u', $this->nombre, $vSesion, date("Y-m-d H:i:s"), $telefonos, $correo, $web, $direccion, $this->id);
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'Empresa editada: ' . $this->id . '|' . $this->nombre . '.');
                return true;
            }
        }
    }

    /**
     * Cambia el estado de una empresa entre activado y desactivado. Devuelve
     * un mensaje si algo salió mal, de lo contrario devuelve true.
     *
     * @access public
     * @param  id
     * @param  estado
     * @return mixed
     */
    public function cambiarEstado($id, $estado) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $id = $sanyval->sanyval($id, 'entero', 'entero');

        if ($id != false) {
            if ($estado == 'Y' || $estado == 'N') {
                $sql = sprintf('UPDATE empresa SET estado = "%s", modificado = "%s", usuario_id = %u WHERE empresa_id = %u', $estado, date("Y-m-d H:i:s"), $vSesion, $id);
            } else {
                return 'El estado debe ser Y o N.';
            }
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {
                if ($estado == 'Y') {
                    $registro = new Registro($vSesion, $this->areaLog, 'Empresa activada: ' . $id . '.');
                    return 'Empresa activada.';
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'Empresa desactivada: ' . $id . '.');
                    return 'Empresa desactivada.';
                }
            }
        }
    }

    /**
     * Devuelve los datos de una sola empresa; devuelve un mensaje si algo
     * salió mal, de lo contrario devuelve un arreglo.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function recuperar($id) {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT * FROM empresa WHERE empresa_id = %u', $id);
        $reg = $BD->GetRow($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando: ' . $this->id);
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {

            $cadena = sprintf('SELECT metodo_id FROM metodo_empresa WHERE empresa_id = %u', $id);
            $reg1 = $BD->Execute($cadena);
            while (!$reg1->EOF) {
                $reg['metodos'][] = $reg1->fields['metodo_id'];
                $reg1->MoveNext();
            }
            return $reg;
        }
    }

    /**
     * Devuelve los datos de todas las empresas; devuelve un mensaje si algo
     * salió mal, de lo contrario devuelve un arreglo multidimensional.
     * Si propias es true sólo devuelve las empresas del usuario actual.
     *
     * @access public
     * @param propias
     * @return mixed
     */
    public function recuperarTodos($propias = false) {
        global $BD;
        global $vSesion;
        $arreglo = [];
        if ($propias == true) {
            $cadena = sprintf('SELECT * FROM empresa WHERE usuario_id = ' . $vSesion . ' ORDER BY nombre ASC');
        } else {
            $cadena = sprintf('SELECT * FROM empresa ORDER BY nombre ASC');
        }
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando todas las empresas.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg());
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['empresa_id']] = ['nombre' => $reg->fields['nombre'], 'estado' => $reg->fields['estado']];
                $reg->MoveNext();
            }
            return $arreglo;
        }
    }

    /**
     * Devuelve el resultado de agregar o modificar una empresa. Si el nombre
     * es el mismo y concuerda con el id entonces actualiza, sino concuerda
     * entonces da error.
     *
     * @access public
     * @param  id
     * @param  nombre
     * @param  valores
     * @return mixed
     */
    public function agregarEmpresa($id, $nombre, $valores = array()) {
        global $sanyval;
        $respuesta = '';
        if ($nombre == "") {
            return 'Debe incluir un nombre.';
        } else {
            $this->id = utf8_encode(@mysql_escape_string(($sanyval->sanyval($id, 'plano', false))));
            $this->nombre = utf8_encode(@mysql_escape_string(($sanyval->sanyval($nombre, 'plano', false))));
            $this->valores = $valores;
            if ($id > 0) {
                if ($this->esPropietario($id) == false) {
                    return '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(1,\'' . $_SERVER['PHP_SELF'] . '\')" />';
                }
                $accion = $this->modificar();
                if ($accion == 1)
                    $respuesta.= 'Empresa modificada.';
            } else {
                $accion = $this->agregar();
                if ($accion == 1)
                    $respuesta.= 'Empresa agregada.';
            }
            if ($accion != 1) {
                $respuesta.= $accion;
            } else {
                $respuesta.='<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }
        }
        return $respuesta;
    }

    /**
     * Modifica los métodos de pago que utiliza la empresa, se recibe el id de
     * la empresa, el usuario y el arreglo de métodos. Se verifica si el usuario
     * es dueño de la empresa.
     *
     * @access public
     * @return mixed
     */
    private function modificarMetodosPago() {
        global $BD;
        global $sanyval;
        global $vSesion;
        if (!empty($this->valores['metodos'])) {
            $metodos = $this->valores['metodos'];
            $reg = $BD->Execute(sprintf('DELETE FROM metodo_empresa WHERE empresa_id = %u', $this->id));
            foreach ($metodos as $k => $v) {
                $sql = sprintf('INSERT INTO metodo_empresa(metodo_id, empresa_id, fecha) VALUE(%u, %u,"%s")', $v, $this->id, date("Y-m-d H:i:s"));
                $reg = $BD->Execute($sql);
                if (!$reg) {
                    $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                    return ERROR_BD;
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'Método de pago modificado para la empresa: ' . $this->id . ' Método: ' . $v);
                }
            }
        }
    }

    /**
     * Verifica si el usuario es dueño de la empresa. Devuelve true si es
     * dueño, false si no y error si algo salió mal.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function esPropietario($id) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $id = $sanyval->sanyval($id, 'entero', 'entero');
        $empresa = $this->recuperar($id);
        if ($empresa['usuario_id'] == $vSesion) {
            return true;
        } else {
            return false;
        }
    }

}

/* end of class Empresa */