<?php

/*
 * @file
 * The theme class that is used to implement the theme
 */

/**
 * Imprime en pantalla la interfaz mediante uso de plantillas.
 *
 * @author J
 */
class Templater {

    /**
     * Nombre de la plantilla a cargar.
     * 
     * @access private
     * @var String
     */
    private $template;

    /**
     * Constructor que llama a load para cargar la plantilla.
     * 
     * @param template
     */
    function __construct($template = null) {
        if (isset($template)) {
            $this->load($template);
        }
    }

    /**
     * This function loads the template file.
     * 
     * @param template
     * @throws FileNotFoundException
     * @throws IOException
     */
    public function load($template) {
        if (!is_file($template)) {
            throw new FileNotFoundException("File not found: $template");
        } elseif (!is_readable($template)) {
            throw new IOException("Could not access file: $template");
        } else {
            $this->template = $template;
        }
    }

    /**
     * Asigna el contenido a la variable interna.
     * 
     * @param var
     * @param content
     */
    public function set($var, $content) {
        $this->$var = $content;
    }

    /**
     * Prints out the theme to the page
     * However, before we do that, we need to remove every var witin {} that are not set
     * @param output - whether to output the template to the screen or to just return the template
     */
    public function publish($output = true) {
        ob_start();
        require $this->template;
        $content = ob_get_clean();

        print $content;
    }

    /**
     * Function that just returns the template file so it can be reused
     */
    public function parse() {

        ob_start();
        require $this->template;
        $content = ob_get_clean();
        return $content;
    }

}
