<?php

/**
 * Administra los permisos.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Permiso {
    // --- ATTRIBUTES ---

    /**
     * Nombre del área para el log.
     *
     * @access public
     * @var String
     */
    private $areaLog = 'Manejo de permisos';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Agrega o modifica un permiso y agrega, modifica o elimina los permisos
     * de ese rol.
     *
     * @access public
     * @param  id
     * @param  nombre
     * @param  llave
     * @return mixed
     */
    public function modificarPermiso($id, $nombre, $llave) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $id = $sanyval->sanyval($id, 'entero', 'entero');
        $nombre = utf8_encode(@mysql_escape_string(($sanyval->sanyval($nombre, 'plano', false))));
        $llave = utf8_encode(@mysql_escape_string(($sanyval->sanyval($llave, 'plano', false))));
        if ($nombre != false && $llave != false) {
            $cadena = sprintf('SELECT nombre FROM permiso WHERE permiso_id = %u', $id);
            $sql = $BD->GetRow($cadena);
            if (count($sql) == 0) {
                $sql = sprintf('INSERT INTO permiso(nombre, llave) VALUES("%s","%s")', $nombre, $llave);
            } else {
                $sql = sprintf('UPDATE permiso SET nombre = "%s", llave = "%s" WHERE permiso_id = %u', $nombre, $llave, $id);
            }
            //$sql = sprintf("REPLACE INTO permiso SET permiso_id = %u, nombre = '%s', llave = '%s'", $id, $nombre, $llave);
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {
                if (empty($id)) {
                    $registro = new Registro($vSesion, $this->areaLog, 'El permiso ha sido agregado: ' . $nombre . ' ' . $llave);
                    return 'El permiso ha sido agregado. <img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'El permiso ha sido editado : ' . $id . '.');
                    return 'El permiso ha sido editado. <img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
                }
            }
        }
    }

    /**
     * Recibe el id de un permiso y elimina dicho permiso. También se elimina
     * la asignación de ese permiso a los roles y usuarios que está asignado.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function eliminarPermiso($id) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $idpermisos = $sanyval->sanyval($id, 'entero', 'entero');
        if ($idpermisos != false) {
            $sql = sprintf("DELETE FROM permiso WHERE permiso_id = %u", $idpermisos);
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                $respuesta.= ERROR_BD;
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'Se ha eliminado el permiso: ' . $idpermisos . '.');
                $respuesta.= 'Se ha eliminado el permiso.<br />';
            }
            $sql = sprintf("DELETE FROM rol_permiso WHERE permiso_id = %u", $idpermisos);
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                $respuesta.= ERROR_BD;
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'Se han eliminado las asignaciones de este permiso a los roles: ' . $idpermisos . '.');
                $respuesta.= 'Se han eliminado las asignaciones de este permiso a los roles.<br />';
            }
            $sql = sprintf("DELETE FROM permiso_usuario WHERE permiso_id = %u", $idpermisos);
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $respuesta.= ERROR_BD;
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            } else {
                $respuesta.= 'Se han eliminado las asignaciones de este permiso a los usuarios.<br />';
                $registro = new Registro($vSesion, $this->areaLog, 'Se han eliminado las asignaciones de este permiso a los usuarios: ' . $idpermisos . '.');
            }
            $respuesta.= '<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            return $respuesta;
        }
    }

}

/* end of class Permiso */
