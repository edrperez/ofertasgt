<?php

/**
 * Administra los métodos de pago.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Metodo {
    // --- ATTRIBUTES ---

    /**
     * Id del método de pago.
     *
     * @access private
     * @var Integer
     */
    private $id = null;

    /**
     * Nombre del método de pago.
     *
     * @access private
     * @var Integer
     */
    private $nombre = null;

    /**
     * Nombre del área para el log.
     *
     * @access private
     * @var String
     */
    private $areaLog = 'Manejo de Métodos de Pago';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Agregar un nuevo método de pago; devuelve un mensaje si algo salió mal,
     * de lo contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function agregar() {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT nombre FROM metodo WHERE nombre LIKE "%s"', $this->nombre);
        $reg = $BD->GetRow($cadena);
        if (!empty($reg)) {
            $resultado = 'El método ' . $reg['nombre'] . ' ya existe.';
            return $resultado;
        } else {
            $cadena = sprintf('INSERT INTO metodo (nombre, estado, imagen) VALUES ("%s", "Y", "%s")', $this->nombre, $this->imagen);
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'El método ha sido agregado: ' . $this->nombre . ' ' . $BD->Insert_ID());
                return true;
            }
        }
    }

    /**
     * Modifica un método de pago; devuelve un mensaje si algo salió mal, de lo
     * contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function modificar() {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT nombre FROM metodo WHERE nombre LIKE "%s"', $this->nombre);
        $reg = $BD->GetRow($cadena);
        if (!empty($reg) && !empty($this->imagen)) {
            $resultado = 'El método ' . $this->nombre . ' ya existe.';
            //return $resultado;
            $cadena = sprintf('UPDATE metodo SET imagen ="%s" WHERE metodo_id = %u', $this->imagen, $this->id);
        } else {
            $cadena = sprintf('UPDATE metodo SET nombre = "%s", imagen ="%s" WHERE metodo_id = %u', $this->nombre, $this->imagen, $this->id);
        }

        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            $registro = new Registro($vSesion, $this->areaLog, 'Método editado: ' . $this->id . '|' . $this->nombre . '.');
            return true;
        }
    }

    /**
     * Cambia el estado de un método de pago entre activado y desactivado.
     * Devuelve un mensaje si algo salió mal, de lo contrario devuelve true.
     *
     * @access public
     * @param  id
     * @param  estado
     * @return mixed
     */
    public function cambiarEstado($id, $estado) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $id = $sanyval->sanyval($id, 'entero', 'entero');

        if ($id != false) {
            if ($estado == 'Y' || $estado == 'N') {
                $sql = sprintf('UPDATE metodo SET estado = "%s" WHERE metodo_id = %u', $estado, $id);
            } else {
                return 'El estado debe ser Y o N.';
            }
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {
                if ($estado == 'Y') {
                    $registro = new Registro($vSesion, $this->areaLog, 'Método activado: ' . $id . '.');
                    return 'Método activado.';
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'Método desactivado: ' . $id . '.');
                    return 'Método desactivado.';
                }
            }
        }
    }

    /**
     * Devuelve los datos de un sólo método; devuelve un mensaje si algo salió
     * mal, de lo contrario devuelve un arreglo.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function recuperar($id) {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT * FROM metodo WHERE metodo_id = %u', $id);
        $reg = $BD->GetRow($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando: ' . $this->id . ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg());
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            return $reg;
        }
    }

    /**
     * Devuelve los datos de todos las métodos de pago; devuelve un mensaje si
     * algo salió mal, de lo contrario devuelve un arreglo multidimensional con
     * todos los valores.
     *
     * @access public
     * @return mixed
     */
    public function recuperarTodos() {
        global $BD;
        global $vSesion;
        $arreglo = [];
        $cadena = sprintf('SELECT * FROM metodo ORDER BY nombre ASC');
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando todos los métodos.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg());
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['metodo_id']] = ['nombre' => $reg->fields['nombre'], 'imagen' => $reg->fields['imagen']];
                $reg->MoveNext();
            }
            return $arreglo;
        }
    }

    /**
     * Devuelve el resultado de agregar o modificar un método de pago. Si el
     * nombre es el mismo y concuerda con el id entonces actualiza, sino
     * concuerda entonces da error.
     *
     * @access public
     * @param id
     * @param nombre
     * @param imagen
     * @return mixed
     */
    public function agregarMetodo($id, $nombre, $imagen) {
        global $sanyval;
        $respuesta = '';
        if ($nombre == "") {
            return 'Debe incluir un nombre.';
        } else {
            $this->id = @mysql_escape_string(($sanyval->sanyval($id, 'plano', false)));
            $this->nombre = @mysql_escape_string(($sanyval->sanyval($nombre, 'plano', false)));
            $this->imagen = $imagen;
            if ($id > 0) {
                $accion = $this->modificar();
                if ($accion == 1)
                    $respuesta.= 'Método modificado.';
            } else {
                $accion = $this->agregar();
                if ($accion == 1)
                    $respuesta.= 'Método agregado.';
            }
            if ($accion != 1) {
                $respuesta.= $accion;
            } else {
                $respuesta.='<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="parent.vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }
        }

        return $respuesta;
    }

}

/* end of class Metodo */