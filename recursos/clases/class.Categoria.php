<?php

/**
 * Administra las categorías de ofertas.
 * @access public
 * @copyright Copyright (C) 2015 Edgar Pérez <contacto@edgarperezgonzalez.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Categoria {
    // --- ATTRIBUTES ---

    /**
     * Id de la categoría.
     *
     * @access public
     * @var Integer
     */
    public $id = null;

    /**
     * Nombre de la categoría.
     *
     * @access public
     * @var Integer
     */
    public $nombre = null;

    /**
     * Nombre del área para el log.
     *
     * @access public
     * @var String
     */
    private $areaLog = 'Manejo de categorías';

    // --- OPERATIONS ---

    /**
     * Constructor de la clase.
     *
     * @access public
     * @return mixed
     */
    public function __construct() {
        
    }

    /**
     * Agregar un nueva categoría; devuelve un mensaje si algo salió mal, de lo
     * contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function agregar() {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT nombre FROM categoria WHERE nombre LIKE "%s"', $this->nombre);
        $reg = $BD->GetRow($cadena);
        if (!empty($reg)) {
            $resultado = 'La categoría ' . $reg['nombre'] . ' ya existe.';
            return $resultado;
        } else {
            $cadena = sprintf('INSERT INTO categoria (nombre, estado, usuario_id, modificado) VALUES ("%s", "Y", %u, "%s")', $this->nombre, $vSesion, date("Y-m-d H:i:s"));
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'La categoría ha sido agregada: ' . $this->nombre . ' ' . $BD->Insert_ID());
                return true;
            }
        }
    }

    /**
     * Modifica una categoría; devuelve un mensaje si algo salió mal, de lo
     * contrario devuelve true.
     *
     * @access private
     * @return mixed
     */
    private function modificar() {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT nombre FROM categoria WHERE nombre LIKE "%s"', $this->nombre);
        $reg = $BD->GetRow($cadena);
        if (!empty($reg)) {
            $resultado = 'La categoría ' . $this->nombre . ' ya existe.';
            return $resultado;
        } else {
            $cadena = sprintf('UPDATE categoria SET nombre = "%s", usuario_id = %u, modificado ="%s" WHERE categoria_id = %u', $this->nombre, $vSesion, date("Y-m-d H:i:s"), $this->id);
            $reg = $BD->Execute($cadena);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
            } else {
                $registro = new Registro($vSesion, $this->areaLog, 'Categoría editada: ' . $this->id . '|' . $this->nombre . '.');
                return true;
            }
        }
    }

    /**
     * Cambia el estado de una categoría entre activado y desactivado. Devuelve
     * un mensaje si algo salió mal, de lo contrario devuelve true.
     *
     * @access public
     * @param  id
     * @param  estado
     * @return mixed
     */
    public function cambiarEstado($id, $estado) {
        global $BD;
        global $sanyval;
        global $vSesion;
        $respuesta = '';
        $id = $sanyval->sanyval($id, 'entero', 'entero');

        if ($id != false) {
            if ($estado == 'Y' || $estado == 'N') {
                $sql = sprintf('UPDATE categoria SET estado = "%s", modificado = "%s", usuario_id = %u WHERE categoria_id = %u', $estado, date("Y-m-d H:i:s"), $vSesion, $id);
            } else {
                return 'El estado debe ser Y o N.';
            }
            $reg = $BD->Execute($sql);
            if (!$reg) {
                $registro = new Registro($vSesion, $this->areaLog, $BD->ErrorNo() . ': ' . $BD->ErrorMsg(), 'Y');
                return ERROR_BD;
            } else {
                if ($estado == 'Y') {
                    $registro = new Registro($vSesion, $this->areaLog, 'Categoría activada: ' . $id . '.');
                    return 'Categoría activada.';
                } else {
                    $registro = new Registro($vSesion, $this->areaLog, 'Categoría desactivada: ' . $id . '.');
                    return 'Categoría desactivada.';
                }
            }
        }
    }

    /**
     * Devuelve los datos de una sola categoría; devuelve un mensaje si algo
     * salió mal, de lo contrario devuelve un arreglo.
     *
     * @access public
     * @param  id
     * @return mixed
     */
    public function recuperar($id) {
        global $BD;
        global $vSesion;
        $cadena = sprintf('SELECT * FROM categoria WHERE categoria_id = %u', $id);
        $reg = $BD->GetRow($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando: ' . $this->id);
            return ERROR_BD . ' ' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg();
        } else {
            return $reg;
        }
    }

    /**
     * Devuelve los datos de todas las categorías; devuelve un mensaje si algo
     * salió mal, de lo contrario devuelve un arreglo multidimensional con id
     * y nombre por categoría.
     *
     * @access public
     * @param activas
     * @return mixed
     */
    public function recuperarTodos($activas = false) {
        global $BD;
        global $vSesion;
        $arreglo = [];
        if ($activas == false) {
            $cadena = sprintf('SELECT * FROM categoria ORDER BY nombre ASC');
        } else {
            $cadena = sprintf('SELECT * FROM categoria WHERE estado = "Y" ORDER BY nombre ASC');
        }
        $reg = $BD->Execute($cadena);
        if (!$reg) {
            $registro = new Registro($vSesion, $this->areaLog, 'Error recuperando todas las categorías.' . $BD->ErrorNo() . ': ' . $BD->ErrorMsg());
        } else {
            while (!$reg->EOF) {
                $arreglo[$reg->fields['categoria_id']] = ['nombre' => $reg->fields['nombre'],
                    'estado' => $reg->fields['estado']];
                $reg->MoveNext();
            }
            return $arreglo;
        }
    }

    /**
     * Devuelve el resultado de agregar o modificar
     * una categoría.
     *
     * @access public
     * @param  id
     * @param  nombre
     * @return mixed
     */
    public function agregarCategoria($id, $nombre) {
        global $sanyval;
        $respuesta = '';
        if ($nombre == "") {
            return 'Debe incluir un nombre.';
        } else {
            $this->id = utf8_encode(@mysql_escape_string(($sanyval->sanyval($id, 'plano', false))));
            $this->nombre = utf8_encode(@mysql_escape_string(($sanyval->sanyval($nombre, 'plano', false))));
            if ($id > 0) {
                $accion = $this->modificar();
                if ($accion == 1)
                    $respuesta.= 'Categoría modificada.';
            } else {
                $accion = $this->agregar();
                if ($accion == 1)
                    $respuesta.= 'Categoría agregada.';
            }
            if ($accion != 1) {
                $respuesta.= $accion;
            } else {
                $respuesta.='<img src="../' . RUTA_IMAGENES_DISENO . '/working.gif" onLoad="vete(2000,\'' . $_SERVER['PHP_SELF'] . '\')" />';
            }
        }

        return $respuesta;
    }

}

/* end of class Categoria */