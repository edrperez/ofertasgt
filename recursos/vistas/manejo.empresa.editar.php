<h2>Editar empresa</h2>
<form id="editar" name="editar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo @$this->nombre; ?>">
            </div>
        </div>
        <?php
        $telefonos = @$this->telefonos;
        $i = 1;
        foreach ($telefonos as $k => $v) {
            ?>
            <div class="form-group">
                <label for="telefono1" class="col-lg-2 control-label">Teléfono <?php echo $i ?></label>
                <div class="col-lg-10">
                    <input class="form-control" type="text" id="<?php echo $k ?>" name="<?php echo $k ?>" maxlength="50" value="<?php echo $v ?>" />
                </div>
            </div>

            <?php
            $i++;
        }
        ?>
        <div class="form-group">
            <label for="correo" class="col-lg-2 control-label">Correo</label>
            <div class="col-lg-10">
                <input class="form-control" id="correo" name="correo" type="text" value="<?php echo @$this->correo; ?>">
            </div>
        </div>        
        <div class="form-group">
            <label for="web" class="col-lg-2 control-label">Web</label>
            <div class="col-lg-10">
                <input class="form-control" id="web" name="web" type="text" value="<?php echo @$this->web; ?>">
            </div>
        </div>        
        <div class="form-group">
            <label for="direccion" class="col-lg-2 control-label">Dirección</label>
            <div class="col-lg-10">
                <textarea class="form-control" rows="3" id="direccion" name="direccion"><?php echo @$this->direccion; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Métodos de pago</label>
            <div class="col-lg-10">
                <?php
                $metodos = @$this->metodos;
                $metodosAsignados = @$this->metodosAsignados;
                $i = 0;
                foreach ($metodos as $k => $v) {
                    ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="metodo<?php echo $i; ?>" name="metodo<?php echo $i; ?>" value="<?php echo $k; ?>" <?php
                            if (@in_array($k, $metodosAsignados)) {
                                echo 'checked="checked"';
                            }
                            ?> />
                                   <?php echo $v['nombre']; ?>
                        </label>
                    </div>

                    <?php
                    $i++;
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" id="empresa_id" name="empresa_id" value="<?php echo @$this->empresa_id; ?>" />
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>'" />
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('empresa.php?op=agregar', 'editar', 'resultadosempresa', 0);" />
            </div>
        </div>
    </fieldset>
</form>
<div id="resultadosempresa"></div>