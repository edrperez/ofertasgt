<!DOCTYPE html>
<html lang="es">
    <head>
        <title>OfertasGT</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>ajax.js" type="text/javascript"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.min.js"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>bootstrap.min.js"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.countdown.js"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.bxslider.min.js"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.dataTables.min.js"></script>
		<!--[if IE]><link rel="shortcut icon" href="/<?php echo @$this->RUTA_IMAGENES_DISENO; ?>favicon.ico"><![endif]-->
        <link rel="apple-touch-icon-precomposed" href="/<?php echo @$this->RUTA_IMAGENES_DISENO; ?>apple-touch-icon-precomposed.png">
        <link rel="icon" href="/<?php echo @$this->RUTA_IMAGENES_DISENO; ?>favicon.png">
        <link rel="stylesheet" href="<?php echo '/' . @$this->RUTA_CSS; ?>bootstrap.css">
        <link href="<?php echo '/' . @$this->RUTA_CSS; ?>jquery.bxslider.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo '/' . @$this->RUTA_CSS; ?>jquery.dataTables.min.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img class="img-responsive" alt="Inicio" src="/<?php echo @$this->RUTA_IMAGENES_DISENO; ?>logo-30.png"></a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categorías<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                $categorias = @$this->categorias;
                                foreach ($categorias as $k => $v) {
                                    echo '<li><a href="/categoria/' . $k . '">' . $v["nombre"] . '</a></li>';
                                }
                                ?>
                            </ul>
                        </li>
                        <li><a href="/comprar/">Carrito (
                                <?php
                                if (!empty($_SESSION['carrito'])) {
                                    echo count($_SESSION['carrito']);
                                } else {
                                    echo '0';
                                }
                                ?>
                                )</a></li>
                        <li><a href="/anteriores">Compras anteriores</a></li>
                        <li><a href="/aclaracion">Aclaración</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/admin/index.php">Admin</a></li>
                    </ul>
                </div>
            </div>
        </nav>
