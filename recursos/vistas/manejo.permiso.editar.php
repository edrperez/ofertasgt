<h2><?php echo @$this->titulo; ?></h2>
<form id="permisosEditar" name="permisosEditar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo @$this->nombre; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="llave" class="col-lg-2 control-label">Llave</label>
            <div class="col-lg-10">
                <input class="form-control" id="llave" name="llave" type="text" value="<?php echo @$this->llave; ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" name="permiso_id" name="permiso_id" value="<?php echo @$this->permiso_id; ?>" />
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = 'permiso.php'" />
                <input class="btn btn-danger" type="button" value="Borrar" id="eliminar" name="eliminar" onclick="enviarFormulario('permiso.php?op=eliminar', 'permisosEditar', 'resultados', 0);" />
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('permiso.php?op=editar', 'permisosEditar', 'resultados', 0);" />
            </div>
        </div>
    </fieldset>
</form>