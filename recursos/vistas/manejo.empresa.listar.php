<table cellpadding="3" width="100%" border="1" cellspacing="0" class="table table-striped table-hover ">
    <tr>
        <th>Empresa</th>

        <?php if (@$this->modificar_empresas == true) { ?><th>Editar</th><?php } ?>

        <?php if (@$this->activar_empresas == true) { ?><th>Activar / Desactivar</th><?php } ?>
    </tr><?php foreach (@$this->datos as $dato) { ?>

        <tr<?php if ($dato['estado'] == 'N') echo ' class="warning"'; ?>>
            <td><?php echo $dato['nombre']; ?></td>
            <?php if (@$this->modificar_empresas == true) { ?>
                <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?editar&id=' . $dato["empresa_id"]; ?>">Editar</a></td>

                <?php
            }
            if (@$this->activar_empresas == true) {
                if ($dato['estado'] == 'Y') {
                    ?>
                    <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?cambiarEstado&id=' . $dato["empresa_id"]; ?>">Activado</a></td>
                <?php } else { ?>
                    <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?cambiarEstado&id=' . $dato["empresa_id"]; ?>">Desactivado</a>
                    </td>
                    <?php
                }
            }
            ?>

        </tr><?php } ?>
</table><center><?php echo @$this->paginar; ?></center>