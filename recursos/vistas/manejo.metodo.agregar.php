<h2>Agregar nuevo método</h2>
<form id="agregar" name="agregar" method="post" action="metodo.php?op=agregar" enctype="multipart/form-data" target="iResultados">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Imagen</label>
            <div class="col-lg-10">
                <input type="file" id="file" name="file" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input class="btn btn-primary" type="submit" value="Agregar datos" id="enviar" name="enviar" />
            </div>
        </div>
    </fieldset>
</form>
<iframe id="iResultados" name="iResultados" frameborder="0" scrolling="no" onload='javascript:resizeIframe(this);'></iframe>