<h2>Modificar roles del usuario: (<?php echo @$this->usuario; ?>)</h2>
<form id="usuarioRoles" name="usuarioRoles" method="post" action="#">
    <fieldset>
        <?php
        $roles = @$this->roles;
        $asignados = @$this->asignados;
        foreach ($roles as $k => $v) {
            ?>
            <div class="form-group">
                <label class="col-lg-2 control-label"><?php echo $v['nombre']; ?></label>
                <div class="col-lg-10">
                    <div class="radio">
                        <label>
                            <input type="radio" name="rol_<?php echo $v['id']; ?>" id="rol_<?php echo $v['id']; ?>_1" value="1"<?php if ($asignados[$v['id']]) { ?> checked="checked"<?php } ?> />
                            Si
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="rol_<?php echo $v['id']; ?>" id="rol_<?php echo $v['id'] ?>_0" value="0"<?php if (!$asignados[$v['id']]) { ?> checked="checked"<?php } ?> />
                            No
                        </label>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" name="action" value="usuarioRoles" />
                <input type="hidden" name="usuario_id" value="<?php echo @$this->usuario_id; ?>" />
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '?editar&idus=<?php echo @$this->usuario_id; ?>'" />
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('usuario.php?op=usuarioRoles', 'usuarioRoles', 'resultados', 'http://<?php echo $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF'] ?>?editar&idus=<?php echo @$this->usuario_id; ?>');" />
            </div>
        </div>
    </fieldset>
</form>