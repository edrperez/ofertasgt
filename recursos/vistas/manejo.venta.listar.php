<h2>Estadísticas de ventas</h2>
<form id="exportar" name="exportar" method="post" action="venta.php?op=exportar" enctype="multipart/form-data" target="iResultados">
    <fieldset>
        <legend>Exportar datos</legend>
        <div class="form-group">
            <label for="empresa_id" class="col-lg-2 control-label">Empresa</label>
            <div class="col-lg-10">
                <select class="form-control" id="empresa_id" name="empresa_id">
                    <?php
                    $empresas = @$this->empresas;
                    foreach ($empresas as $k => $v) {
                        echo '<option value=' . $k . '>' . $v["nombre"] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="fechai" class="col-lg-2 control-label">Fecha inicial</label>
            <div class="col-lg-10">
                <input class="form-control" id="fechai" name="fechai" type="text" value="<?php echo date('Y-m-d'); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="fechaf" class="col-lg-2 control-label">Fecha final</label>
            <div class="col-lg-10">
                <input class="form-control" id="fechaf" name="fechaf" type="text" value="<?php echo date('Y-m-d'); ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input class="btn btn-primary" type="submit" value="Consultar" id="enviar" name="enviar" />
            </div>
        </div>
    </fieldset>
</form>
<iframe id="iResultados" name="iResultados" frameborder="0" scrolling="no" width="100%" onload='javascript:resizeIframe(this);'></iframe>
<?php if (@$this->ver_ventas == true) { ?>
    <table cellpadding="3" width="100%" border="1" cellspacing="0" class="table table-striped table-hover ">
        <thead>
            <tr>
                <th>Empresa</th>

                <th>Fecha</th>

                <th>Monto (Q.)</th>

                <th>Detalle</th>

            </tr>
        </thead>
        <tbody>
            <?php foreach (@$this->datos as $dato) { ?>
                <tr>
                    <td><?php echo $dato['Empresa']; ?></td>
                    <td><?php echo $dato['Fecha']; ?></td>
                    <td class="text-right"><?php echo $dato['Monto']; ?></td>
                    <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?detalle&id=' . $dato["venta_id"]; ?>">Ver</a>
                    </td>
                    <?php
                }
                ?>
            </tr>
        </tbody>
    </table><center><?php echo @$this->paginar; ?></center>
    <?php
}?>