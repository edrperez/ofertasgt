<div class="container-fluid">
    <?php if (count(@$this->ofertas) > 0) { ?>
        <div class="row">
            <?php
            $ofertas = @$this->ofertas;
            foreach ($ofertas as $k => $v) {
                ?>
                <div class="col-sm-6 col-md-5">
                    <div class="thumbnail">
                        <a href="/ver/<?php echo $k; ?>">
                            <img src="/<?php echo @$this->RUTA_IMAGENES_DISENO; ?>ofertas/<?php echo $v['imagen'] ?>" alt="<?php echo $v['nombre'] ?>">
                        </a>
                        <div class="caption">
                            <h3><?php echo $v['nombre']; ?></h3>
                            <p><?php echo $v['resumen']; ?></p>
                            <p><a href="/ver/<?php echo $k; ?>" class="btn btn-primary" role="button">Ver</a>
                            <strike>Q<?php echo $v['precioo']; ?></strike>
                            <span class="descontado">Q<?php echo $v['preciod']; ?></span></p>
                        </div>
                    </div>
                </div>
            <?php }
            ?>
        </div>
    <?php } ?>
</div>
</div>