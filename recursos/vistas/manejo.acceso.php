<h2>Iniciar sesión</h2>
<form id="fingreso" name="fingreso" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="usuario" class="col-lg-2 control-label">Usuario</label>
            <div class="col-lg-10">
                <input class="form-control" id="usuario" name="usuario" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="clave" class="col-lg-2 control-label">Clave</label>
            <div class="col-lg-10">
                <input class="form-control" id="clave" name="clave" type="password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <a href="../index.php" class="btn btn-default">Cancelar</a>
                <input class="btn btn-primary" type="button" value="Ingresar" id="enviar" name="enviar" onclick="enviarFormulario('index.php?op=acceso', 'fingreso', 'resultados', 0);" /></td>
            </div>
        </div>
    </fieldset>
</form>