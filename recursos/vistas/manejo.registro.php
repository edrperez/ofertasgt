<table id="registro" cellpadding="3" width="100%" border="1" cellspacing="0" class="table table-striped table-hover ">
    <thead><tr>
            <th>Usuario</th>

            <th>Área</th>

            <th>Contenido</th>

            <th>Fecha</th>

            <th>IP</th>

            <th>Navegador</th>
        </tr></thead>
    <tbody>
        <?php
        foreach (@$this->datos as $k => $v) {
            ?>

            <tr<?php if ($v['error'] == 'Y') echo ' class="danger"'; ?>>
                <td><?php echo $v['usuario']; ?></td>
                <td><?php echo $v['area']; ?></td>
                <td><?php echo $v['contenido']; ?></td>
                <td><?php echo $v['fecha']; ?></td>
                <td><?php echo $v['ip']; ?></td>
                <td><?php echo $v['navegador']; ?></td>
            </tr><?php } ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#registro').DataTable({
            "language": {"url": "/js/datatables.spanish.json"},
            "order": [[3, "desc"]]});
    });
</script>