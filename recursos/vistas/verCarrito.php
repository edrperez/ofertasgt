<div class="container-fluid col-xs-12">
    <div class="row content">
        <div class="col-xs-10 col-xs-offset-1">
            <table class="table">
                <caption><h1>Carrito de compras</h1></caption>
                <thead>
                    <tr>
                        <th class="col-xs-1"></th>
                        <th class="col-xs-4">Item</th>
                        <th class="col-xs-1">Cantidad</th>
                        <th class="col-xs-1 text-right">Precio</th>
                        <th class="col-xs-2 text-right">Total</th>
                    </tr>
                </thead>
                <form method="get" action="/pagar.php">
                    <tbody>
                        <?php
                        $ofertas = @$this->ofertas;
                        $totalInicial = 0;
                        foreach ($ofertas as $k => $v) {
                            echo '<tr id="fila' . $k . '"><td><input id="' . $k . '" type="button" value="X" class="btn btn-warning"></td>';
                            echo '<td><p><strong>' . $v['nombre'] . '</strong></p><p>' . $v['resumen'] . '</p></td>';
                            echo '<td><img id="mas" src="/' . @$this->RUTA_IMAGENES_DISENO . 'mas.png" width="20" height="20" class="mas"/>';
                            echo '<input id="' . $k . '" name="' . $k . '" type="text" value="1" class="cantidad"/>';
                            echo '<img src="/' . @$this->RUTA_IMAGENES_DISENO . 'menos.png" id="menos" width="20" height="20" class="menos"/></td>';
                            echo '<td class="text-right">Q.<span class="precio">' . $v['preciod'] . '</span></td>';
                            echo '<td class="text-right">Q.<span id="total' . $k . '" class="total">' . $v['preciod'] . '</span></td>';
                            echo '</tr>';
                            $totalInicial += $v['preciod'];
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right"><strong>Total</strong></td>
                            <td class="text-right">Q.<span id="grantotal"><?php echo $totalInicial; ?></span></td>
                        </tr>
                    </tfoot>
            </table>
            <input type="submit" class="btn btn-info col-xs-offset-11" id="pagar" value="Pagar">
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        //Si no hay productos que el botón de pagar se deshabilite
        if ($('#grantotal').html() == '0') {
            $('#pagar').attr("disabled", true);
        }
        //Establecemos el valor inicial del gran total
        var granTotal = parseFloat(<?php echo $totalInicial; ?>);
        /** Botones identificando por clase para aumentar o disminuir la cantidad
         * del producto, también actualizan los subtotales y totales.
         */

        $('.mas').on('click', function () {
            var $cantidad = $(this).closest('td').find('.cantidad');
            var $total = $(this).closest('tr').find('.total');
            var $precio = $(this).closest('tr').find('.precio').html();
            var currentVal = parseInt($cantidad.val());
            var valorMas = 0;
            if (!isNaN(currentVal)) {
                valorMas = currentVal + 1;
                $cantidad.val(valorMas);
                $total.html(valorMas * $precio);
                granTotal += parseFloat($precio);
                $('#grantotal').html(granTotal);
            }
        });
        $('.menos').on('click', function () {
            var $cantidad = $(this).closest('td').find('.cantidad');
            var $total = $(this).closest('tr').find('.total');
            var $precio = $(this).closest('tr').find('.precio').html();
            var currentVal = parseInt($cantidad.val());
            var valorMenos = 0;
            if (!isNaN(currentVal) && currentVal > 1) {
                valorMenos = currentVal - 1;
                $cantidad.val(valorMenos);
                $total.html(valorMenos * $precio);
                granTotal -= parseFloat($precio);
                $('#grantotal').html(granTotal);
            }
        });
        /* El botón para eliminar un renglón de producto, si logra la llamada
         * ajax entonces elimina la fila y todo su contenido. También verifica
         * que si es cero el gran total el botón de pagar se deshabilite. 
         */
        $('.btn-warning').click(function () {
            var id = $(this).attr('id');
            granTotal -= parseFloat($('#total' + id).html());
            $('#grantotal').html(granTotal);
            $.ajax({
                url: '/limpiar/' + id,
                success: function (response) {
                    var tr = $('#fila' + id);
                    tr.addClass('alert-danger');
                    tr.fadeOut(1000, function () {
                        tr.remove();
                    });
                }
            });
            if ($('#grantotal').html() == '0') {
                $('#pagar').attr("disabled", true);
            }

            return false;
        });
    });
</script>