<h2>Agregar nueva oferta</h2>
<form id="agregar" name="agregar" method="post" action="oferta.php?op=agregar" enctype="multipart/form-data" target="iResultados">
    <fieldset>
        <div class="form-group">
            <label for="empresa_id" class="col-lg-2 control-label">Empresa</label>
            <div class="col-lg-10">
                <select class="form-control" id="empresa_id" name="empresa_id">
                    <?php
                    $empresas = @$this->empresas;
                    foreach ($empresas as $k => $v) {
                        echo '<option value=' . $k . '>' . $v["nombre"] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="categoria_id" class="col-lg-2 control-label">Categoría</label>
            <div class="col-lg-10">
                <select class="form-control" id="categoria_id" name="categoria_id">
                    <?php
                    $categorias = @$this->categorias;
                    foreach ($categorias as $k => $v) {
                        echo '<option value=' . $k . '>' . $v["nombre"] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="resumen" class="col-lg-2 control-label">Resumen</label>
            <div class="col-lg-10">
                <textarea class="form-control" rows="3" id="resumen" name="resumen"></textarea>
                <span id="contador" class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="fechaf" class="col-lg-2 control-label">Fecha de expiración</label>
            <div class="col-lg-10">
                <input class="form-control" id="fechaf" name="fechaf" type="text" value="<?php echo date('Y-m-d H'); ?>:00">
            </div>
        </div>
        <div class="form-group">
            <label for="fechav" class="col-lg-2 control-label">Fecha de vigencia del cupón</label>
            <div class="col-lg-10">
                <input class="form-control" id="fechav" name="fechav" type="text" value="<?php echo date('Y-m-d H'); ?>:00">
            </div>
        </div>
        <div class="form-group">
            <label for="descripcion" class="col-lg-2 control-label">Descripción</label>
            <div class="col-lg-10">
                <textarea class="form-control largo" rows="3" id="descripcion" name="descripcion"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="condiciones" class="col-lg-2 control-label">Condiciones</label>
            <div class="col-lg-10">
                <textarea class="form-control largo" rows="3" id="condiciones" name="condiciones"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="precioo" class="col-lg-2 control-label">Precio Original Q.</label>
            <div class="col-lg-10">
                <input class="form-control" id="precio" name="precioo" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="preciod" class="col-lg-2 control-label">Precio con Descuento Q.</label>
            <div class="col-lg-10">
                <input class="form-control" id="precio" name="preciod" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="portada" class="col-lg-2 control-label">Portada</label>
            <div class="col-lg-10">
                <input type="file" id="portada" name="portada" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input class="btn btn-primary" type="submit" value="Agregar datos" id="enviar" name="enviar" />
            </div>
        </div>
    </fieldset>
</form>
<iframe id="iResultados" name="iResultados" frameborder="0" scrolling="no" width="100%" onload='javascript:resizeIframe(this);'></iframe>