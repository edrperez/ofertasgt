<h2>Agregar nuevo usuario</h2>

<form id="agregar" name="agregar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="usuario" class="col-lg-2 control-label">Usuario</label>
            <div class="col-lg-10">
                <input class="form-control" id="usuario" name="usuario" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="clave" class="col-lg-2 control-label">Clave</label>
            <div class="col-lg-10">
                <input class="form-control" id="clave" name="clave" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="correo" class="col-lg-2 control-label">Correo</label>
            <div class="col-lg-10">
                <input class="form-control" id="correo" name="correo" type="text">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input class="btn btn-primary" type="button" value="Agregar datos" id="enviar" name="enviar" onclick="enviarFormulario('usuario.php?op=agregar', 'agregar', 'resultadosusuario', 0);" />
            </div>
        </div>
    </fieldset>
</form>
<div id="resultadosusuario"></div>
<table cellpadding="3" width="100%" border="1" cellspacing="0" class="table table-striped table-hover ">
    <tr>
        <th>Usuario</th>

        <th>Editar</th>

        <th>Activar / Desactivar</th>
    </tr><?php foreach (@$this->datos as $dato) { ?>

    <tr<?php if ($dato['estado'] == 'N') echo ' class="warning"'; ?>>
        <td><?php echo $dato['usuario']; ?></td>

        <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?editar&idus=' . $dato["usuario_id"]; ?>">Editar</a></td>
            <?php if ($dato['estado'] == 'Y') { ?>
                <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?estadoUsuario&idus=' . $dato["usuario_id"]; ?>">Activado</a></td>
            <?php } else { ?>
                <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?estadoUsuario&idus=' . $dato["usuario_id"]; ?>">Desactivado</a>
                    <?php
                    if ($dato['intento'] >= @$this->intentos) {
                        printf('* el usuario ha excedido el número (%u) de intentos de acceso', $dato['intentos']);
                    }
                    ?>
                </td>
            <?php } ?>

        </tr><?php } ?>
</table><center><?php echo @$this->paginar; ?></center>