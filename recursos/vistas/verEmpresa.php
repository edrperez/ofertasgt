<div class="container-fluid col-xs-12">
    <div class="row content">
        <div class="col-xs-7 col-xs-offset-1">
            <h2><?php echo @$this->datos['nombre']; ?></h2>
            <p><strong>Teléfonos:</strong> <?php echo @$this->telefonos; ?></p>
            <p><strong>Correo:</strong> <a href="mailto:<?php echo @$this->datos['correo']; ?>" target="_blank"><?php echo @$this->datos['correo']; ?></a></p>
            <p><strong>Web:</strong> <a href="<?php echo @$this->datos['web']; ?>" target="_blank"><?php echo @$this->datos['web']; ?></a></p>
            <p><strong>Dirección:</strong> <?php echo @$this->datos['direccion']; ?></p>
        </div>
    </div>