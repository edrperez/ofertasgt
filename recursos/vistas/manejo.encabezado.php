<!DOCTYPE html>
<html lang="es">
    <head>
        <title>OfertasGT - Administración</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>ajax.js" type="text/javascript"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.min.js"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>bootstrap.min.js"></script>
        <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.dataTables.min.js"></script>
        <?php if (@$this->datetimepicker == true) { ?>
            <script src="<?php echo '/' . @$this->RUTA_JS; ?>jquery.datetimepicker.js"></script>
            <link rel="stylesheet" href="<?php echo '/' . @$this->RUTA_CSS; ?>jquery.datetimepicker.css">
        <?php } ?>
        <link rel="stylesheet" href="<?php echo '/' . @$this->RUTA_CSS; ?>jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo '/' . @$this->RUTA_CSS; ?>bootstrap.css">
    </head>
    <body>
        <?php
        if (@$this->sesion > 0) {
            ?>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">Inicio</a>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php
                            if (@$this->modificar_usuarios == true) {
                                ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Usuarios<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="usuario.php">Usuarios</a></li>
                                        <?php if (@$this->modificar_roles == true) {
                                            ?>
                                            <li><a href="rol.php">Roles</a></li>
                                        <?php } ?>
                                        <?php
                                        if (@$this->modificar_permisos == true) {
                                            ?>
                                            <li><a href="permiso.php">Permisos</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                            <?php
                            if (@$this->modificar_categorias == true) {
                                ?>
                                <li><a href="categoria.php">Categorías</a></li>
                                <?php
                            }
                            ?>
                            <?php
                            if (@$this->modificar_empresas == true || @$this->activar_empresas == true) {
                                ?>
                                <li><a href ="empresa.php">Empresas</a></li>
                            <?php } ?>
                            <?php
                            if (@$this->modificar_metodos_de_pago == true) {
                                ?>
                                <li><a href="metodo.php">Métodos de pago</a></li>
                            <?php } ?>
                            <?php
                            if (@$this->modificar_ofertas == true || @$this->activar_ofertas == true) {
                                ?>
                                <li><a href="oferta.php">Ofertas</a></li>
                            <?php } ?>
                            <?php
                            if (@$this->ver_ventas == true || @$this->ventas_completo == true) {
                                ?>
                                <li><a href="venta.php">Ventas</a></li>
                            <?php } ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php?op=salir">Cerrar sesión</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        <?php } ?>