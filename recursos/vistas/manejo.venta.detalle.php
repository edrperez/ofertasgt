<?php
$detalle = @$this->detalle;
?>
<div class="container-fluid col-xs-12">
    <div class="row content">
        <div class="col-xs-7 col-xs-offset-1">
            <h2>Detalle de venta</h2>
            <p><strong>Comprador:</strong> <?php echo @$this->datos['nombre']; ?></p>
            <p><strong>NIT:</strong> <?php echo @$this->datos['nit']; ?></p>
            <p><strong>Correo:</strong> <?php echo @$this->datos['correo']; ?></p>
            <p><strong>Teléfono:</strong> <?php echo @$this->datos['telefono']; ?></p>
            <p><strong>Dirección:</strong> <?php echo @$this->datos['direccion']; ?></p>
            <p><strong>Fecha de compra:</strong> <?php echo @$this->datos['fechai']; ?></p>
            <?php
            foreach ($detalle as $k => $v) {
                ?>
                <p>Cupón: <?php echo $v['oferta_id'] . $v['venta_id'] . $k; ?>
                    (
                    <?php
                    if ($v['canjeado'] == 'Y') {
                        echo 'Canejado';
                    } else {
                        echo 'Sin canjear';
                    }
                    ?>)
                </p>
                <ul>
                    <li>Oferta: <?php echo $v['oferta']; ?></li>
                    <li>Beneficiario: <?php echo $v['usuario']; ?></li>
                    <li>Correo: <?php echo $v['correo']; ?></li>
                    <li>Precio: Q.<?php echo $v['preciod']; ?></li>
                </ul>
                <?php
            }
            ?>
        </div>
    </div>