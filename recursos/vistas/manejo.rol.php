<h2>Seleccione un rol a modificar:</h2>
<div class="list-group">
    <?php
    foreach (@$this->roles as $k => $v) {
        ?>
        <a class="list-group-item" href="<?php echo '' . $_SERVER["PHP_SELF"] . '?editar&idrol=' . $v['id']; ?>"><?php echo $v['nombre']; ?></a><br />
    <?php }
    ?>
</div>

<?php
if (count(@$this->roles) < 1) {
    ?>    
    <div class="alert alert-dismissible alert-info">
        <strong>No hay roles.</strong>
    </div>
    <?php
}
?>
<input class="btn btn-primary" type="button" id="nuevorol" name="nuevorol" value="Nuevo Rol" onclick="window.location = '?editar'" />