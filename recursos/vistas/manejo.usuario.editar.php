<h2>Editar usuario</h2>

<form id="editar" name="editar" method="post" action="#">
    <fieldset>
        <legend>Usuario: <?php echo @$this->usuario; ?></legend>
        <div class="form-group">
            <label for="clave" class="col-lg-2 control-label">Clave</label>
            <div class="col-lg-10">
                <input class="form-control" id="clave" name="clave" type="text" placeholder="* dejar en blanco para no editar">
            </div>
        </div>
        <div class="form-group">
            <label for="correo" class="col-lg-2 control-label">Correo</label>
            <div class="col-lg-10">
                <input class="form-control" id="correo" name="correo" type="text" value="<?php echo @$this->correo; ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" id="usuario" name="usuario" value="<?php echo @$this->usuario; ?>" />
                <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo @$this->usuario_id; ?>"/>
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>'" />        
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('usuario.php?op=editar', 'editar', 'resultadosusuario', 0);" />
            </div>
        </div>
    </fieldset>
</form>
<div id="resultadosusuario"></div>
<h3>Roles para el usuario: (<a href="?roles&idus=<?php echo @$this->idUs; ?>">Modificar sus roles</a>)</h3>
<ul>
    <?php foreach (@$this->roles as $k => $v) { ?>
        <li><?php echo @$this->nombres[$v] ?> </li>
    <?php } ?>
</ul>
<h3>Permisos para el usuario: (<a href="?permisos&idus=<?php echo @$this->idUs; ?>">Modificar sus permisos</a>)</h3>
<ul>
    <?php
    $perms = @$this->permisos;
    foreach ($perms as $k => $v) {
        if ($v['valor'] === false) {
            continue;
        }
        ?>
        <li><?php
            echo $v['nombre'];
            if ($v['heredado']) {
                ?> (heredado)
            <?php } ?>
        </li>  
    <?php } ?>
</ul>