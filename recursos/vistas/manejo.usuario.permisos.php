<h2>Modificar permisos del usuario: (<?php echo @$this->usuario; ?>)</h2>
<form id="usuarioPermisos" name="usuarioPermisos" method="post" action="#">
    <fieldset>
        <?php
        $uPerms = @$this->permisos;
        $tPerms = @$this->permisosTotales;
        foreach ($tPerms as $k => $v) {
            $iVal = '';
            ?>
            <div class="form-group">
                <label for="perm_<?php echo $v['id']; ?>" class="col-lg-2 control-label"><?php echo $v['nombre']; ?></label>
                <div class="col-lg-10">
                    <select class="form-control" id="perm_<?php echo $v['id']; ?>" name="perm_<?php echo $v['id']; ?>">
                        <option value="1"<?php
                        if (isset($uPerms[$v['llave']])) {
                            if ($uPerms[$v['llave']]['valor'] === true && $uPerms[$v['llave']]['heredado'] != true) {
                                ?> selected="selected" <?php
                                    }
                                }
                                ?>>Permitir</option>
                        <option value="0"<?php
                        if (isset($uPerms[$v['llave']])) {
                            if ($uPerms[$v['llave']]['valor'] === false && $uPerms[$v['llave']]['heredado'] != true) {
                                ?> selected="selected" <?php
                                    }
                                }
                                ?>>Denegar</option>
                        <option value="x"<?php
                        if (isset($uPerms[$v['llave']])) {
                            if ($uPerms[$v['llave']]['heredado'] == true || !array_key_exists($v['llave'], $uPerms)) {
                                ?> selected="selected"
                                        <?php
                                        if ($uPerms[$v['llave']]['valor'] === true) {
                                            $iVal = '(Permitido)';
                                        } else {
                                            $iVal = '(Denegado)';
                                        }
                                    }
                                }
                                ?>
                                >Heredar <?php echo $iVal; ?></option>
                    </select>
                    <br>
                </div>
            </div>
        <?php } ?>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" name="usuario_id" value="<?php echo @$this->usuario_id; ?>" />
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '?editar&idus=<?php echo @$this->usuario_id; ?>'" />        
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('usuario.php?op=usuarioPermisos', 'usuarioPermisos', 'resultados', 'http://<?php echo $_SERVER['SERVER_ADDR'] . $_SERVER['PHP_SELF'] ?>?editar&idus=<?php echo @$this->usuario_id; ?>');" />
            </div>
        </div>
    </fieldset>
</form>