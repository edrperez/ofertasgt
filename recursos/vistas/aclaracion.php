<div class="container text-center">
    <img src="<?php echo @$this->RUTA_IMAGENES_DISENO; ?>umg.png" class="img-responsive center-block">
    <h4>
        Este es un sitio ficticio que ha sido creado con fines propiamente académicos por
        un grupo de estudiantes de la Universidad Mariano Gálvez de Guatemala.
    </h4>
    <h4>
        Todas las imágenes y otros textos han sido copiados sin permiso de sus respectivas
        fuentes. El objetivo ha sido de aprender sobre técnicas de Ingeniería de Software.
    </h4>
    <h4>
        Guatemala, 2015
    </h4>
	<h4>
	<a href="https://bitbucket.org/edrperez/ofertasgt" target="_blank">Repositorio en Bitbucket</a>
	</h4>
</div>