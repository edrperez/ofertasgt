<h2>Activar / Desactivar usuario</h2>
<form id="eliminar" name="eliminar" method="post" action="#">
    <fieldset>
        <legend>Usuario: <?php echo @$this->usuario; ?></legend>
        <div class="form-group">
            <div class="col-lg-10">
                <div class="checkbox">
                    <label>
                        <?php if (@$this->estado == 'Y') { ?>
                            <input type="checkbox" id="estado" name="estado" value="1" checked="checked" />
                        <?php } else { ?>
                            <input type="checkbox" id="estado" name="estado" value="1" />
                        <?php } ?>
                        Activar / Desactivar
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input id="seguro" name="seguro" type="checkbox" value="1" />¿Está seguro?
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo @$this->usuario_id; ?>"/>
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>'" />        
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('usuario.php?op=estadoUsuario', 'eliminar', 'resultados', 0);" />
            </div>
        </div>
    </fieldset>
</form>