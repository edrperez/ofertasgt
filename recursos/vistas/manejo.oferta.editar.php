<h2>Editar oferta</h2>
<?php $datos = @$this->datos; ?>
<form id="agregar" name="agregar" method="post" action="oferta.php?op=agregar" enctype="multipart/form-data" target="iResultados">
    <fieldset>
        <div class="form-group">
            <label for="empresa_id" class="col-lg-2 control-label">Empresa</label>
            <div class="col-lg-10">
                <select class="form-control" id="empresa_id" name="empresa_id">
                    <?php
                    $empresas = @$this->empresas;
                    foreach ($empresas as $k => $v) {
                        if ($k == $datos['empresa_id']) {
                            echo '<option value=' . $k . ' selected>' . $v["nombre"] . '</option>';
                        } else {
                            echo '<option value=' . $k . '>' . $v["nombre"] . '</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="categoria_id" class="col-lg-2 control-label">Categoría</label>
            <div class="col-lg-10">
                <select class="form-control" id="categoria_id" name="categoria_id">
                    <?php
                    $categorias = @$this->categorias;
                    foreach ($categorias as $k => $v) {
                        if ($k == $datos['categoria_id']) {
                            echo '<option value=' . $k . ' selected>' . $v["nombre"] . '</option>';
                        } else {
                            echo '<option value=' . $k . '>' . $v["nombre"] . '</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo $datos['nombre']; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="resumen" class="col-lg-2 control-label">Resumen</label>
            <div class="col-lg-10">
                <textarea class="form-control" rows="3" id="resumen" name="resumen"><?php echo $datos['resumen']; ?></textarea>
                <span id="contador" class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="fechaf" class="col-lg-2 control-label">Fecha de expiración</label>
            <div class="col-lg-10">
                <input class="form-control" id="fechaf" name="fechaf" type="text" value="<?php echo date('Y-m-d H:i', strtotime($datos['fechaf'])); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="fechav" class="col-lg-2 control-label">Fecha de vigencia del cupón</label>
            <div class="col-lg-10">
                <input class="form-control" id="fechav" name="fechav" type="text" value="<?php echo date('Y-m-d H:i', strtotime($datos['fechav'])); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="descripcion" class="col-lg-2 control-label">Descripción</label>
            <div class="col-lg-10">
                <textarea class="form-control largo" rows="3" id="descripcion" name="descripcion"><?php echo $datos['descripcion']; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="condiciones" class="col-lg-2 control-label">Condiciones</label>
            <div class="col-lg-10">
                <textarea class="form-control largo" rows="3" id="condiciones" name="condiciones"><?php echo $datos['condiciones']; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="precioo" class="col-lg-2 control-label">Precio Original Q.</label>
            <div class="col-lg-10">
                <input class="form-control" id="precioo" name="precioo" type="text" value="<?php echo $datos['precioo']; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="preciod" class="col-lg-2 control-label">Precio con Descuento Q.</label>
            <div class="col-lg-10">
                <input class="form-control" id="preciod" name="preciod" type="text" value="<?php echo $datos['preciod']; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="imagen" class="col-lg-2 control-label">Portada actual</label>
            <div class="col-lg-10">
                <img src="<?php echo "/" . @$this->RUTA_IMAGENES_DISENO . "ofertas/" . $datos['imagen']; ?>" class="img-responsive">
            </div>
        </div>
        <div class="form-group">
            <label for="portada" class="col-lg-2 control-label">Nueva portada</label>
            <div class="col-lg-10">
                <input type="file" id="portada" name="portada" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" id="oferta_id" name="oferta_id" value="<?php echo $datos['oferta_id']; ?>" />
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>'" />
                <input class="btn btn-primary" type="submit" value="Enviar" id="enviar" name="enviar" />
            </div>
        </div>
    </fieldset>
    <!--<table>
        <tr>
            <td style="font: bold;">Empresa</td>
            <td><select id="empresa_id" name="empresa_id" />
    <?php
    $empresas = @$this->empresas;
    foreach ($empresas as $k => $v) {
        if ($k == $datos['empresa_id']) {
            echo '<option value=' . $k . ' selected>' . $v["nombre"] . '</option>';
        } else {
            echo '<option value=' . $k . '>' . $v["nombre"] . '</option>';
        }
    }
    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="font: bold;">Nombre</td>
            <td><input type="text" id="nombre" name="nombre" maxlength="100" size ="100" value="<?php echo $datos['nombre']; ?>" /></td>
        </tr>
        <tr>
            <td style="font: bold;">Resumen</td>
            ​<td><textarea id="resumen" name="resumen" rows="2" cols="50"><?php echo $datos['resumen']; ?></textarea><span id="contador"></span></td>
        </tr>
        <tr>
            <td style="font: bold;">Fecha de expiración</td>
            ​<td><input type="text" id="fechaf" name="fechaf" value="<?php echo date('Y-m-d H:i', strtotime($datos['fechaf'])); ?>" /></td>
        </tr>
        <tr>
            <td style="font: bold;">Descripción</td>
            ​<td><textarea id="descripcion" name="descripcion" rows="10" cols="50" class="largo"><?php echo $datos['descripcion']; ?></textarea></td>
        </tr>
        <tr>
            <td style="font: bold;">Condiciones</td>
            ​<td><textarea id="condiciones" name="condiciones" rows="10" cols="50" class="largo"><?php echo $datos['condiciones']; ?></textarea></td>
        </tr>
        <tr>
            <td style="font: bold;">Precio Q.</td>
            <td><input type="text" id="precio" name="precio" maxlength="50" size="15" value="<?php echo $datos['precio']; ?>" /></td>
        </tr>
        <tr>
            <td style="font: bold;">Portada actual</td>
            <td><img src="<?php echo "/" . @$this->RUTA_IMAGENES_DISENO . "ofertas/" . $datos['imagen']; ?>"></td>
        </tr>
        <tr>
            <td style="font: bold;">Nueva portada</td>
            <td><input type="file" id="portada" name="portada" /></td>
        </tr>
        <tr>
        <input type="hidden" id="oferta_id" name="oferta_id" value="<?php echo $datos['oferta_id']; ?>" />
        <tr>
            <td colspan="2"><input type="submit" value="Enviar" id="enviar" name="enviar" /></td>
        </tr>
    </table>
    -->
</form>
<iframe id="iResultados" name="iResultados" frameborder="0" scrolling="no" width="100%" onload='javascript:resizeIframe(this);'></iframe>