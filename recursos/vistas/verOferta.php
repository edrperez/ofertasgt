<?php
$imagenes = @$this->imagenes;
?>
<div class="container-fluid col-xs-12">
    <h1><?php echo @$this->datos['nombre']; ?></h1>
    <ul class="bxslider">
        <?php
        foreach ($imagenes as $k => $v) {
            echo '<li><img src="/' . @$this->RUTA_IMAGENES_DISENO . 'ofertas/' . $v["nombre"] . '" /></li>';
        }
        ?>
    </ul>
    <div class="row content">
        <div class="col-xs-7 col-xs-offset-1">
            <h2>Descripción</h2>
            <p><?php echo @$this->datos['descripcion']; ?></p>
            <h2>Condiciones</h2>
            <p><?php echo @$this->datos['condiciones']; ?></p>
            <p>Esta oferta se puede canjear hasta: <?php echo date('d-m-Y H:i',strtotime(@$this->datos['fechav'])); ?></p>
        </div>
        <div class="col-xs-4">
            <div class="countdown">
                Termina en <span id="clock" class="clock"></span>
            </div>
            <strike>Q<?php echo @$this->datos['precioo']; ?></strike>
            <span class="descontado">Q<?php echo @$this->datos['preciod']; ?></span>
            <p></p>
            <a href="/comprar/<?php echo @$this->datos['oferta_id']; ?>" class="btn btn-success" role="button">Comprar</a>
            <br>
            <p></p>
            <?php
            echo '<a href="/vendedor/' . @$this->empresa['empresa_id'] . '">' . @$this->empresa['nombre'] . '</a><p>';
            $metodos = @$this->empresa['metodos'];
            $metodosCompletos = @$this->metodos;
            foreach ($metodos as $k => $v) {
                echo '<img src="/' . @$this->RUTA_IMAGENES_DISENO . 'pagos/' . $metodosCompletos[$v]['imagen'] . '" alt="' . $metodosCompletos[$v]['nombre'] . '">';
            }
            ?>
        </div>
    </div>
</div>
<script>
    /* Instanciamos el slide show de imágenes de la oferta */
    $(document).ready(function () {
        $('.bxslider').bxSlider({
            auto: true,
            pause: 10000,
            mode: "fade",
        });
    });
    /* Generamos una cuenta regresiva con fecha última la expiración de la oferta
     * en el sitio. */
    $('#clock').countdown('<?php echo @$this->datos['fechaf']; ?>')
            .on('update.countdown', function (event) {
                var format = '%H:%M:%S';
                if (event.offset.days > 0) {
                    format = '%-d día%!d ' + format;
                }
                if (event.offset.weeks > 0) {
                    format = '%-w semana%!w ' + format;
                }
                $(this).html(event.strftime(format));
            })
            .on('finish.countdown', function (event) {
                $(this).html('Ya no está disponible.')
                        .parent().addClass('disabled')

            });
</script>