<h2 class=" col-xs-offset-4">Datos para la factura</h2>
<div class="row content">
    <div class="col-xs-10 col-xs-offset-1">
        <form id="anteriores" name="anteriores" method="post" action="/veranterior">
            <fieldset>
                <div class="form-group  col-xs-offset-4">
                    <label for="nit" class="col-xs-2 control-label">NIT</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="nit" name="nit" type="text">
                    </div>
                </div>
                <div class="form-group  col-xs-offset-4">
                    <label for="correo" class="col-xs-2 control-label">Correo</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="correo" name="correo" type="text">
                    </div>
                </div>
                <div class="form-group  col-xs-offset-4">
                    <label for="telefono" class="col-xs-2 control-label">Teléfono</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="telefono" name="telefono" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-10 col-xs-offset-6">
                        <input class="btn btn-danger btn-lg"  type="submit" value="Ver compras" id="enviar" name="enviar">
                    </div>
                </div>
                </div>
            </fieldset>
        </form>
    </div>