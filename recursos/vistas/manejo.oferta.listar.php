<table cellpadding="3" width="100%" border="1" cellspacing="0" class="table table-striped table-hover ">
    <tr>
        <th>Oferta</th>

        <th>Empresa</th>

        <?php if (@$this->modificar_ofertas == true) { ?><th>Editar oferta</th><?php } ?>

        <?php if (@$this->modificar_ofertas == true) { ?><th>Editar imagenes</th><?php } ?>

        <?php if (@$this->activar_ofertas == true) { ?><th>Activar / Desactivar</th><?php } ?>
    </tr><?php foreach (@$this->datos as $dato) { ?>

        <tr<?php if ($dato['estado'] == 'N') echo ' class="warning"'; ?>>
            <td><?php echo $dato['nombre']; ?></td>

            <td><?php echo $dato['empresa']; ?></td>
            <?php if (@$this->modificar_ofertas == true) { ?>
                <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?editar&id=' . $dato["oferta_id"]; ?>">Editar</a></td>

                <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?imagenes&id=' . $dato["oferta_id"]; ?>">Editar</a></td>
            <?php } ?>
            <?php if (@$this->activar_ofertas == true) { ?>
                <?php if ($dato['estado'] == 'Y') { ?>
                    <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?cambiarEstado&id=' . $dato["oferta_id"]; ?>">Activado</a></td>
                <?php } else { ?>
                    <td><a href="<?php echo '' . $_SERVER["PHP_SELF"] . '?cambiarEstado&id=' . $dato["oferta_id"]; ?>">Desactivado</a>
                    </td>
                <?php } ?>
            <?php } ?>

        </tr><?php } ?>
</table><center><?php echo @$this->paginar; ?></center>