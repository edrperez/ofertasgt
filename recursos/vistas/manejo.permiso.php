<h2>Seleccione un permiso a modificar:</h2>
<div class="list-group">
    <?php
    foreach (@$this->permisos as $k => $v) {
        ?>
        <a class="list-group-item" href="?editar&idpermiso=<?php echo $v['id']; ?>"><?php echo $v['nombre']; ?></a><br />
    <?php } ?>
</div>

<?php
if (count(@$this->permisos) < 1) {
    ?>    
    <div class="alert alert-dismissible alert-info">
        <strong>No hay permisos.</strong>
    </div>
    <?php
}
?>
<input class="btn btn-primary" type="button" id="nuevopermiso" name="nuevopermiso" value="Nuevo Permiso" onclick="window.location = '?editar'" />