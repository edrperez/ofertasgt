<h2>Editar categoría</h2>
<form id="editar" name="editar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo @$this->nombre; ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" id="categoria_id" name="categoria_id" value="<?php echo @$this->categoria_id; ?>"/>
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>'" />
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('categoria.php?op=agregar', 'editar', 'resultadoscategoria', 0);" />
            </div>
        </div>
    </fieldset>
</form>
<div id="resultadoscategoria"></div>