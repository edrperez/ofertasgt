<h2>Editar método</h2>
<form id="agregar" name="agregar" method="post" action="metodo.php?op=agregar" enctype="multipart/form-data" target="iResultados">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo @$this->nombre; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="imagen" class="col-lg-2 control-label">Imagen actual</label>
            <div class="col-lg-10">
                <img id="imagen" name="imagen" src="<?php echo "/" . @$this->RUTA_IMAGENES_DISENO . "pagos/" . @$this->imagen ?>" class="img-responsive">
            </div>
        </div>
        <div class="form-group">
            <label for="file" class="col-lg-2 control-label">Nueva imagen</label>
            <div class="col-lg-10">
                <input type="file" id="file" name="file" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" id="metodo_id" name="metodo_id" value="<?php echo @$this->metodo_id; ?>"/>
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = '<?php echo $_SERVER['PHP_SELF']; ?>'" />
                <input class="btn btn-primary" type="submit" value="Agregar datos" id="enviar" name="enviar" />
            </div>
        </div>
    </fieldset>
<!--    <table>
        <tr>
            <td style="font: bold;">Método</td>
            <td><input type="text" id="nombre" name="nombre" value="<?php echo @$this->nombre; ?>" /></td>
        </tr>
        <tr>
            <td style="font: bold;">Imagen actual</td>
            <td><img src="<?php echo "/" . @$this->RUTA_IMAGENES_DISENO . "pagos/" . @$this->imagen ?>"></td>
        </tr>
        <tr>
            <td style="font: bold;">Nueva imagen</td>
            <td><input type="file" id="file" name="file" /></td>
        </tr>
        <tr>
        <input type="hidden" id="metodo_id" name="metodo_id" value="<?php echo @$this->metodo_id; ?>"/>
        <td colspan="2"><input type="submit" value="Agregar datos" id="enviar" name="enviar" /></td>
        </tr>
    </table>
-->
</form>
<iframe id="iResultados" name="iResultados" frameborder="0" scrolling="no" onload='javascript:resizeIframe(this);'></iframe>