<h2><?php echo @$this->titulo; ?></h2>
<form id="rolesEditar" name="rolesEditar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre: </label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo @$this->nombre; ?>">
            </div>
        </div>    
        <?php
        $aPerms = @$this->aPerms;
        $rPerms = @$this->rPerms;
        $idrol = @$this->idrol;
        foreach ($aPerms as $k => $v) {
            ?>
            <div class="form-group">
                <label class="col-lg-2 control-label"><?php echo $v['nombre']; ?></label>
                <div class="col-lg-10">
                    <div class="radio">
                        <label>
                            <input type="radio" name="perm_<?php echo $v['id']; ?>" id="perm_<?php echo $v['id']; ?>_1" value="1"
                            <?php
                            if (isset($rPerms[$v['llave']])) {
                                if ($rPerms[$v['llave']]['valor'] === true && @$this->idrol != '') {
                                    ?> checked="checked" <?php
                                       }
                                   }
                                   ?>/>
                            Si
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="perm_<?php echo $v['id']; ?>" id="perm_<?php echo $v['id']; ?>_0" value="0"
                            <?php
                            if (isset($rPerms[$v['llave']])) {
                                if ($rPerms[$v['llave']]['valor'] != true && @$this->idrol != '') {
                                    ?> checked="checked" <?php
                                       }
                                   }
                                   ?>/>
                            No
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="perm_<?php echo $v['id']; ?>" id="perm_<?php echo $v['id']; ?>_X" value="X"
                                   <?php if (@$this->idrol == '' || !array_key_exists($v['llave'], $rPerms)) { ?> checked="checked" <?php } ?>/>
                            Ignorar
                        </label>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="hidden" name="rol_id" name="rol_id" value="<?php echo @$this->idrol; ?>" />
                <input class="btn btn-default" type="button" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location = 'rol.php'" />
                <input class="btn btn-danger" type="button" value="Borrar" id="eliminar" name="eliminar" onclick="enviarFormulario('rol.php?op=eliminar', 'rolesEditar', 'resultados', 0);" />
                <input class="btn btn-primary" type="button" value="Enviar" id="enviar" name="enviar" onclick="enviarFormulario('rol.php?op=editar', 'rolesEditar', 'resultados', 0);" />                
            </div>
        </div>
    </fieldset>
</form>