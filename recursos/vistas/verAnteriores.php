<div class="container-fluid col-xs-12">
    <div class="row content">
        <div class="col-xs-10 col-xs-offset-1">
            <table id="ventas" namne="ventas" cellpadding="3" width="100%" border="1" cellspacing="0" class="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th>Cupón</th>
                        <th>Fecha de compra</th>
                        <th>Oferta</th>
                        <th>Empresa</th>
                        <th>Beneficiario</th>
                        <th>Precio pagado</th>
                        <th>Canjeado</th>
                    </tr><?php foreach (@$this->ventas as $k => $v) { ?>
                </thead>
                <tbody>
                    <tr>
                        <!--<td class="text-center"><a href="/cupon/cupon-<?php echo $v["oferta_id"] . $v["venta_id"] . $k; ?>.pdf"><?php echo $v["oferta_id"] . $v["venta_id"] . $k; ?></a></td>-->
                        <td class="text-center"><a href="/cupon/cupon-<?php echo $k; ?>"><?php echo $v["oferta_id"] . $v["venta_id"] . $k; ?></a></td>
                        <td><?php echo $v['fecha']; ?></td>
                        <td><?php echo $v['oferta']; ?></td>
                        <td><?php echo $v['empresa']; ?></td>
                        <td><?php echo $v['usuario']; ?></td>
                        <td class="text-right">Q<?php echo $v['preciod']; ?></td>
                        <td><?php
                                if ($v['cobrado'] == "Y") {
                                    echo 'Si';
                                } else {
                                    echo 'No';
                                }
                                ?></td>
                        </tr><?php } ?>
                </tbody>
            </table><center><?php echo @$this->paginar; ?></center>
        </div>
    </div>
</div>