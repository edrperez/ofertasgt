<h2>Agregar nueva categoría</h2>
<form id="agregar" name="agregar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input class="btn btn-primary"  type="button" value="Agregar datos" id="enviar" name="enviar" onclick="enviarFormulario('categoria.php?op=agregar', 'agregar', 'resultadoscategoria', 0);" />
            </div>
        </div>
    </fieldset>
</form>
<div id="resultadoscategoria"></div>