<div id="resultados"></div>
<?php if (@$this->tinymce == true) { ?>
    <script src="/<?php echo @$this->RUTA_JS; ?>tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea.largo', language: 'es'});</script>
    <script>
        /* Contamos y mostramos en pantalla los carácteres restantes que serán
         * almacenados en la base de datos para esta textarea. */
        $('#resumen').keyup(function () {
            var left = 150 - $(this).val().length;
            if (left < 0) {
                left = 0;
            }
            $('#contador').text('Restante: ' + left);
        });
    </script>
<?php } ?>
<?php if (@$this->datetimepicker == true) { ?>
    <script>
        /* Declaramos y configuramos los datetimepicker que son utilizados en el área
         * de administración de ofertas y generación de estadísticas de ventas. */
        $('#fechav').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'es',
            format: 'Y-m-d H:i',
        });
        $('#fechaf').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'es',
            format: 'Y-m-d H:i',
        });
        $('#fechai').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'es',
            format: 'Y-m-d H:i',
        });
    </script>
<?php } ?>
</body>
</html>