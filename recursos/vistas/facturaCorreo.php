<?php $valores = @$this->valores; ?>
<style type="text/css">
    <!--
    table {
        border-collapse: separate;
        border-spacing: 0;
        color: #4a4a4d;
        font: 14px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    th,
    td {
        padding: 10px 15px;
        vertical-align: middle;
    }
    thead {
        background: #395870;
        background: linear-gradient(#49708f, #293f50);
        color: #000;
        font-size: 11px;
        text-transform: uppercase;
    }
    tbody tr:nth-child(even) {
        background: #f0f0f2;
    }
    tbody td:last-child {
        text-align: right;
    }
    td {
        border-bottom: 1px solid #cecfd5;
        border-right: 1px solid #cecfd5;
    }
    td:first-child {
        border-left: 1px solid #cecfd5;
    }
    tfoot {
        text-align: right;
    }
    tfoot tr:last-child {
        background: #f0f0f2;
        color: #395870;
        font-weight: bold;
    }
    tfoot tr:last-child td:first-child {
        border-bottom-left-radius: 5px;
    }
    tfoot tr:last-child td:last-child {
        border-bottom-right-radius: 5px;
    }
    -->
</style>
<page orientation="portrait" style="font-size: 10pt">
    <div style="position: absolute; right: 3mm; top: 3mm; text-align: right; font-size: 4mm; ">
        <img src="../publico/img/logo-70.png">
        <h1>Ofertas GT</h1>
        <a href="http://ofertasgt.cu.cc">http://ofertasgt.cu.cc</a>
    </div>
    <div style="position: absolute; left: 3mm; top: 3mm; text-align: left; font-size: 3.5mm; ">
        <h2>Factura</h2>
        <strong>Fecha:</strong> <?php echo date("d-m-Y"); ?><br>
        <strong>Nombre:</strong> <?php echo $valores['compradorN']; ?><br>
        <strong>NIT:</strong> <?php echo $valores['nit']; ?><br>
        <strong>Correo:</strong> <?php echo $valores['compradorC']; ?><br>
        <strong>Teléfono:</strong> <?php echo $valores['telefono']; ?><br>
        <strong>Dirección:</strong> <?php echo $valores['direccion']; ?><br>
        <h3>Detalle</h3>
        <table>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Oferta</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total = 0;
                for ($i = 0; $i < $valores['totalDetalle']; $i++) {
                    $total += $valores['detalle'][$i]['preciod'];
                    ?>
                    <tr>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo wordwrap($valores['detalle'][$i]['noferta'], 30, "<br />"); ?></td>
                        <td><?php echo wordwrap($valores['detalle'][$i]['nombre'], 20, "<br />"); ?></td>
                        <td><?php echo $valores['detalle'][$i]['correo']; ?></td>
                        <td style="text-align:right;">Q.<?php echo $valores['detalle'][$i]['preciod']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" style="text-align:right;">Total</td>
                    <td style="text-align:right;">Q.<?php echo $total; ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
</page>