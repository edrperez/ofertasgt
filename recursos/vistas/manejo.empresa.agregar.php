<h2>Agregar nueva empresa</h2>
<form id="agregar" name="agregar" method="post" action="#">
    <fieldset>
        <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
                <input class="form-control" id="nombre" name="nombre" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="telefono1" class="col-lg-2 control-label">Teléfono 1</label>
            <div class="col-lg-10">
                <input class="form-control" id="telefono1" name="telefono1" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="telefono2" class="col-lg-2 control-label">Teléfono 2</label>
            <div class="col-lg-10">
                <input class="form-control" id="telefono2" name="telefono2" type="text">
            </div>
        </div>        
        <div class="form-group">
            <label for="telefono3" class="col-lg-2 control-label">Teléfono 3</label>
            <div class="col-lg-10">
                <input class="form-control" id="telefono3" name="telefono3" type="text">
            </div>
        </div>        
        <div class="form-group">
            <label for="correo" class="col-lg-2 control-label">Correo</label>
            <div class="col-lg-10">
                <input class="form-control" id="correo" name="correo" type="text">
            </div>
        </div>        
        <div class="form-group">
            <label for="web" class="col-lg-2 control-label">Web</label>
            <div class="col-lg-10">
                <input class="form-control" id="web" name="web" type="text" value="http://">
            </div>
        </div>        
        <div class="form-group">
            <label for="direccion" class="col-lg-2 control-label">Dirección</label>
            <div class="col-lg-10">
                <textarea class="form-control" rows="3" id="direccion" name="direccion"></textarea>
                <span class="help-block">Ejempo: 5ta. Avenida, 8-06, Zona 9, Guatemala.</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Métodos de pago</label>
            <div class="col-lg-10">
                <?php
                $metodos = @$this->metodos;
                $i = 0;
                foreach ($metodos as $k => $v) {
                    ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="metodo<?php echo $i; ?>" name="metodo<?php echo $i; ?>" value="<?php echo $k; ?>" />
                            <?php echo $v['nombre']; ?>
                        </label>
                    </div>

                    <?php
                    $i++;
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input class="btn btn-primary" type="button" value="Agregar datos" id="enviar" name="enviar" onclick="enviarFormulario('empresa.php?op=agregar', 'agregar', 'resultadosempresa', 0);" />
            </div>
        </div>
    </fieldset>
</form>
<div id="resultadosempresa"></div>