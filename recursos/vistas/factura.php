<h2 class=" col-xs-offset-4">Datos para la factura</h2>
<div class="row content">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="col-xs-1">
            <h4>Total</h4>
            <h4>Q.<?php echo @$this->total; ?></h4>
        </div>
        <form id="factura" name="factura" method="post" action="/pagar/">
            <fieldset>
                <div class="form-group  col-xs-offset-4">
                    <label for="compradorN" class="col-xs-2 control-label">Nombre</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="compradorN" name="compradorN" type="text">
                    </div>
                </div>
                <div class="form-group  col-xs-offset-4">
                    <label for="nit" class="col-xs-2 control-label">NIT</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="nit" name="nit" type="text">
                    </div>
                </div>
                <div class="form-group  col-xs-offset-4">
                    <label for="compradorC" class="col-xs-2 control-label">Correo</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="compradorC" name="compradorC" type="text">
                    </div>
                </div>
                <div class="form-group  col-xs-offset-4">
                    <label for="telefono" class="col-xs-2 control-label">Teléfono</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="telefono" name="telefono" type="text">
                    </div>
                </div>
                <div class="form-group  col-xs-offset-4">
                    <label for="direccion" class="col-xs-2 control-label">Dirección</label>
                    <div class="col-xs-10">
                        <input class="form-control" id="direccion" name="direccion" type="text">
                    </div>
                </div>
                <table class="table">
                    <caption><h2>Datos de los beneficiarios para los cupones</h2></caption>
                    <thead>
                        <tr>
                            <th class="col-xs-1">No.</th>
                            <th class="col-xs-3">Item</th>
                            <th class="col-xs-2 text-center">Nombre</th>
                            <th class="col-xs-2 text-center">Correo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $tabla = @$this->tabla;
                        foreach ($tabla as $k => $v) {
                            echo '<tr><td>' . ($k + 1) . '</td><td>' . $v['nombre'] . '</td>';
                            echo '<td><input id="oferta' . $k . '" name="oferta' . $k . '" type="hidden" value="' . $v['oferta'] . '">';
                            echo '<input class="form-control" id="nombre' . $k . '" name="nombre' . $k . '" type="text"></td>';
                            echo '<td><input class="form-control" id="correo' . $k . '" name="correo' . $k . '" type="text"></td>';
                            echo '</tr>';
                        }
                        echo '<td><input id="totalDetalle" name="totalDetalle" type="hidden" value="' . count($tabla) . '">';
                        ?>
                    </tbody>
                </table>
                <div id="resultado"></div>
                <div class="form-group">
                    <div class="col-xs-10 col-xs-offset-6">
                        <input class="btn btn-danger btn-lg"  type="button" value="Enviar pago" id="enviar" name="enviar">
                    </div>
                </div>
                </div>
            </fieldset>
        </form>
    </div>

    <script>
        /* Agregamos la función al hacer click al botón enviar y hacemos la llamada
         * Ajax para cargar los datos. */
        $(document).ready(function () {
            $('#enviar').click(function () {
                var data = $('#factura').serialize();
                $.ajax({
                    type: 'post',
                    data: data,
                    url: '/pagar/',
                    success: function (response) {
                        $('#resultado').html(response);
                        $('html, body').animate({
                            scrollTop: ($('#resultado').offset().top)
                        }, 500);
                    }
                });
            });
        });
    </script>