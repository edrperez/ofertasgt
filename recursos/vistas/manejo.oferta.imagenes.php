<h2>Imágenes de la oferta</h2>

<form id="agregar" name="agregar" method="post" action="oferta.php?op=imagenes" enctype="multipart/form-data" target="iResultados">
    <fieldset>
        <legend> <?php echo @$this->nombre; ?></legend>
        <div class="form-group">
            <label for="archivos" class="col-lg-2 control-label">Nuevas</label>
            <div class="col-lg-10">
                <input type="file" id="archivos" name="archivos[]" multiple="multiple" />
            </div>
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <input type="hidden" id="oferta_id" name="oferta_id" value="<?php echo @$this->oferta_id; ?>" />
                    <input class="btn btn-primary" type="submit" value="Agregar datos" id="enviar" name="enviar" />
                </div>
            </div>
        </div>
    </fieldset>
    <!--<table>
        <tr>
            <td style="font: bold;">Nuevas</td>
            <td><input type="file" id="archivos" name="archivos[]" multiple="multiple" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="hidden" id="oferta_id" name="oferta_id" value="<?php echo @$this->oferta_id; ?>" />
                <input type="submit" value="Agregar datos" id="enviar" name="enviar" />
            </td>
        </tr>
    </table>-->
</form>
<iframe id="iResultados" name="iResultados" frameborder="0" scrolling="no" width="100%" onload='javascript:resizeIframe(this);'></iframe>
<form id="eliminar" name="eliminar" method="post" action="#">
    <fieldset>
        <?php
        $imagenes = @$this->imagenes;
        foreach ($imagenes as $k => $v) {
            ?>
            <div id="<?php echo $k; ?>" class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <img src="../<?php echo @$this->RUTA_IMAGENES_DISENO . "ofertas/" . $v["nombre"] ?>" class="img-responsive">
                    <input class="btn btn-danger" type="button" value="Eliminar" id="eliminar" name="eliminar" onclick="eliminarImagen('oferta.php?op=eliminarImagen', '<?php echo $k; ?>', '<?php echo $v["nombre"]; ?>');" />
                </div>
            </div>
        <?php }
        ?>
    </fieldset>
</form>