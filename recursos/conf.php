<?php

if (basename($_SERVER['PHP_SELF']) == 'conf.php') {

    header('Location: http://' . $_SERVER['SERVER_ADDR']);
} else {
    session_name('OFERTASGT');
    session_start();
    ob_start();
    //Se incluye la librería ADODB para acceso a base de datos
    require_once 'adodb/adodb.inc.php';
    //Arreglo de valoes para acceder a la BD
    //El bd1 es de desarrollo y el bd2 de producción cuando existe
    $bdConfig = array('bd' => array('bd1' => array('base' => 'ofertasgt', 'usuario' => 'ofertasgt', 'clave' => 'ofertasgt1999', 'servidor' =>
                '127.0.0.1'), 'bd2' => array('base' => 'ofertasgt2', 'usuario' => 'ofertasgt2', 'clave' => 'ofertasgt2000', 'servidor' =>
                '192.168.1.10')));
    //Constantes que definen rutas muy utilizadas en el sistema
    defined('RUTA_RECURSOS') or define('RUTA_RECURSOS', '../recursos/');
    defined('RUTA_CLASES') or define('RUTA_CLASES', 'clases/');
    defined('RUTA_VISTAS') or define('RUTA_VISTAS', 'vistas/');
    defined('RUTA_CSS') or define('RUTA_CSS', 'css/');
    defined('RUTA_JS') or define('RUTA_JS', 'js/');
    defined('RUTA_IMAGENES_DISENO') or define('RUTA_IMAGENES_DISENO', 'img/');
    defined('ERROR_BD') or define('ERROR_BD', 'Error en la base de datos.');

    //Datos a usar para la base de datos
    $bdcd = 'bd1';
    //Se crea el objeto para el acceso a la base de datos una sola vez
    $BD = NewADOConnection('mysqli');
    $BD->Connect($bdConfig['bd'][$bdcd]['servidor'], $bdConfig['bd'][$bdcd]['usuario'], $bdConfig['bd'][$bdcd]['clave'], $bdConfig['bd'][$bdcd]['base']);
    require_once RUTA_CLASES . 'class.Templater.php';
    require_once RUTA_CLASES . 'class.Registro.php';
    $ageip = json_encode(Registro::obtenerAgentIp());
    require_once RUTA_CLASES . 'class.Sanyval.php';
    //No hay utilidad de momento.    
    //require_once RUTA_CLASES . 'class.CargarXMLPorId.php';
    $sanyval = new SanyVal();
    require_once RUTA_CLASES . 'class.Opcion.php';
    $obtopc = new Opcion();
    require_once RUTA_CLASES . 'class.Paginado.php';
    require_once RUTA_CLASES . 'class.Permiso.php';
    require_once RUTA_CLASES . 'class.Rol.php';
    require_once RUTA_CLASES . 'class.Usuario.php';
    require_once RUTA_CLASES . 'class.Acceso.php';
    $Acceso = new Acceso();
    $vSesion = $Acceso->verificarSesion();
    require_once RUTA_CLASES . 'class.ACL.php';
    $ACL = new ACL($vSesion);
    require_once RUTA_CLASES . 'class.Categoria.php';
    require_once RUTA_CLASES . 'class.Empresa.php';
    require_once RUTA_CLASES . 'class.Metodo.php';
    require_once RUTA_CLASES . 'class.Oferta.php';
    require_once RUTA_CLASES . 'class.Venta.php';
}