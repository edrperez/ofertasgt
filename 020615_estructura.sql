-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ofertasgt
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `categoria_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(50) NOT NULL COMMENT 'El nombre de la categoría.',
  `estado` enum('Y','N') NOT NULL COMMENT 'Estado activado o desactivado.',
  `modificado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de última modificación.',
  `usuario_id` int(10) unsigned NOT NULL COMMENT 'Último usuario que lo modificó.',
  PRIMARY KEY (`categoria_id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  CONSTRAINT `fk_usuario_id_categoria` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Este es el listado de categorías de ofertas y su estado activo/inactivo.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `consultar_cupones`
--

DROP TABLE IF EXISTS `consultar_cupones`;
/*!50001 DROP VIEW IF EXISTS `consultar_cupones`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `consultar_cupones` AS SELECT 
 1 AS `cupon`,
 1 AS `usuario`,
 1 AS `precioo`,
 1 AS `correo`,
 1 AS `preciod`,
 1 AS `Oferta`,
 1 AS `Fecha`,
 1 AS `Empresa`,
 1 AS `condiciones`,
 1 AS `telefonos`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `empresa_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(100) NOT NULL COMMENT 'Nombre de la empresa.',
  `usuario_id` int(10) unsigned NOT NULL COMMENT 'El usuario que es propietario de ésta empresa.',
  `telefonos` varchar(100) NOT NULL COMMENT 'Un arreglo almacenado en formato JSON donde están los teléfonos de la empresa.',
  `correo` varchar(100) NOT NULL COMMENT 'Correo electrónico de la empresa.',
  `web` varchar(255) DEFAULT NULL COMMENT 'Sitio Web de la empresa.',
  `direccion` text NOT NULL COMMENT 'Dirección física de la empresa. Aquí va departamento, municipio, etc.',
  `estado` enum('Y','N') NOT NULL COMMENT 'Si está activa Y, si no lo está N.',
  `modificado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de última modificación.',
  PRIMARY KEY (`empresa_id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  CONSTRAINT `fk_usuario_id_empresa` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Contiene los datos básicos de cada empresa.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imagen`
--

DROP TABLE IF EXISTS `imagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagen` (
  `imagen_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `oferta_id` int(10) unsigned NOT NULL COMMENT 'Id de la oferta a la que pertenece.',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre de la imagen.',
  `modificado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de modificación.',
  PRIMARY KEY (`imagen_id`),
  KEY `fk_oferta_id_imagen_idx` (`oferta_id`),
  CONSTRAINT `fk_oferta_id_imagen` FOREIGN KEY (`oferta_id`) REFERENCES `oferta` (`oferta_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='Almacena los nombres de las imágenes de las ofertas.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `listar_ofertas`
--

DROP TABLE IF EXISTS `listar_ofertas`;
/*!50001 DROP VIEW IF EXISTS `listar_ofertas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `listar_ofertas` AS SELECT 
 1 AS `oferta_id`,
 1 AS `nombre`,
 1 AS `estado`,
 1 AS `empresa`,
 1 AS `usuario_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `metodo`
--

DROP TABLE IF EXISTS `metodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metodo` (
  `metodo_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del método de pago.',
  `imagen` varchar(45) NOT NULL COMMENT 'Nombre de la imagen que representa al método de pago.',
  `codigo` text COMMENT 'Código de forma de pago para incrustar en el sitio si tiene.',
  `estado` enum('Y','N') NOT NULL COMMENT 'Si está activo Y, si no lo está N.',
  PRIMARY KEY (`metodo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Almacena los métodos de pago.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metodo_empresa`
--

DROP TABLE IF EXISTS `metodo_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metodo_empresa` (
  `metodo_id` int(10) unsigned NOT NULL COMMENT 'Id del método de pago.',
  `empresa_id` int(10) unsigned NOT NULL COMMENT 'Id de la empresa.',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la última modificación.',
  KEY `fk_metodo_id_idx` (`metodo_id`),
  KEY `fk_empresa_id_idx` (`empresa_id`),
  CONSTRAINT `fk_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_metodo_id` FOREIGN KEY (`metodo_id`) REFERENCES `metodo` (`metodo_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Métodos de pago asignados a una empresa.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oferta`
--

DROP TABLE IF EXISTS `oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta` (
  `oferta_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `empresa_id` int(10) unsigned NOT NULL COMMENT 'Id de la empresa a la que pertenece esta oferta.',
  `categoria_id` int(10) unsigned NOT NULL COMMENT 'Id de la categoría.',
  `nombre` varchar(100) NOT NULL COMMENT 'Nombre de la oferta.',
  `imagen` varchar(45) NOT NULL COMMENT 'Nombre de la imagen de portada.',
  `precioo` double unsigned NOT NULL COMMENT 'Precio original de la oferta.',
  `preciod` double NOT NULL COMMENT 'Precio con descuento de la oferta.',
  `estado` enum('Y','N') NOT NULL COMMENT 'Si está activo Y, si no lo está N.',
  `fechai` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de ingreso.',
  `fecham` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de modificación.',
  `fechaf` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de finalización de la oferta.',
  `fechav` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de vigencia del cupón.',
  `descripcion` text COMMENT 'Descripción general de la oferta.',
  `resumen` varchar(150) DEFAULT NULL COMMENT 'Resumen de la oferta.',
  `condiciones` text COMMENT 'Condiciones de uso de la oferta.',
  PRIMARY KEY (`oferta_id`),
  KEY `fk_empresa_id_oferta_idx` (`empresa_id`),
  KEY `fk_categoria_id_oferta_idx` (`categoria_id`),
  CONSTRAINT `fk_empresa_id_oferta` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='Almacena las ofertas.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `opcion`
--

DROP TABLE IF EXISTS `opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion` (
  `opcion_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(45) NOT NULL COMMENT 'El nombre de la opción.',
  `valor` text NOT NULL COMMENT 'El valor de la opción.',
  PRIMARY KEY (`opcion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Una serie de opciones que pueden servir para realizar ciertas operaciones a nivel global.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permiso`
--

DROP TABLE IF EXISTS `permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permiso` (
  `permiso_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del permiso.',
  `llave` varchar(45) NOT NULL COMMENT 'Llave del permiso.',
  PRIMARY KEY (`permiso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los permisos con su nombre y llave.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permiso_usuario`
--

DROP TABLE IF EXISTS `permiso_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permiso_usuario` (
  `permiso_usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `usuario_id` int(10) unsigned NOT NULL COMMENT 'Id del usuario.',
  `permiso_id` int(10) unsigned NOT NULL COMMENT 'Id del permiso.',
  `valor` tinyint(1) NOT NULL COMMENT 'Valor que representa si el usuario tiene permiso o no tiene permiso o no lo tiene asignado.',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la creación/modificación.',
  PRIMARY KEY (`permiso_usuario_id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  KEY `fk_permiso_id_idx` (`permiso_id`),
  CONSTRAINT `fk_permiso_id_permiso_usuario` FOREIGN KEY (`permiso_id`) REFERENCES `permiso` (`permiso_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario_id_permiso_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Acá se almacena las asignaciones de permisos a usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `registro_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `usuario_id` int(10) unsigned NOT NULL COMMENT 'El id del usuario, si es que hay, del que generó la actividad.',
  `area` varchar(50) NOT NULL COMMENT 'El área del sistema donde ocurrió el evento.',
  `contenido` text NOT NULL COMMENT 'El contenido del registro.',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha y hora del evento.',
  `ip` varchar(45) NOT NULL COMMENT 'IP desde donde se generó el evento.',
  `navegador` tinytext NOT NULL COMMENT 'El navegador del cliente que se utilizó.',
  `error` enum('Y','N') NOT NULL COMMENT 'Si fue un error Y, si no lo fue N.',
  PRIMARY KEY (`registro_id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  CONSTRAINT `fk_usuario_id_registro` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1814 DEFAULT CHARSET=utf8 COMMENT='Este es el almacén para todos los eventos que se registran.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `rol_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del rol.',
  PRIMARY KEY (`rol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Tabla de almacenamiento de los roles.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rol_permiso`
--

DROP TABLE IF EXISTS `rol_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_permiso` (
  `rol_permiso_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rol_id` int(10) unsigned NOT NULL COMMENT 'Id del rol.',
  `permiso_id` int(10) unsigned NOT NULL COMMENT 'Id del permiso.',
  `valor` tinyint(1) NOT NULL COMMENT 'Valor que representa si el rol tiene un permiso o no tiene permiso o no lo tiene asignado.',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la creación/modificación.',
  PRIMARY KEY (`rol_permiso_id`),
  KEY `fk_rol_id_idx` (`rol_id`),
  KEY `fk_permiso_id_idx` (`permiso_id`),
  CONSTRAINT `fk_permiso_id_rol_permiso` FOREIGN KEY (`permiso_id`) REFERENCES `permiso` (`permiso_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rol_id_rol_permiso` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`rol_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=utf8 COMMENT='Esta tabla almacena las asignaciones de permisos a roles.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rol_usuario`
--

DROP TABLE IF EXISTS `rol_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_usuario` (
  `rol_usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `usuario_id` int(10) unsigned NOT NULL COMMENT 'Id del usuario.',
  `rol_id` int(10) unsigned NOT NULL COMMENT 'Id del rol.',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se crea/modifica.',
  PRIMARY KEY (`rol_usuario_id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  KEY `fk_rol_id_idx` (`rol_id`),
  CONSTRAINT `fk_rol_id_rol_usuario` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`rol_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario_id_rol_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='Acá se almacenan las asignaciones de roles a usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sesion`
--

DROP TABLE IF EXISTS `sesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sesion` (
  `sesion_id` varchar(50) NOT NULL COMMENT 'Identificador de la sesión.',
  `usuario_id` int(10) unsigned NOT NULL COMMENT 'Id del usuario.',
  `uacceso` datetime NOT NULL COMMENT 'Últma fecha de modificación.',
  `datos` text NOT NULL COMMENT 'Datos de la sesión.',
  PRIMARY KEY (`sesion_id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  CONSTRAINT `fk_usuario_id_sesion` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contiene las sesiones de los usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `usuario` varchar(50) NOT NULL COMMENT 'Nombre del usuario.',
  `clave` varchar(128) NOT NULL COMMENT 'Hash de la clave del usuario.',
  `correo` varchar(100) NOT NULL COMMENT 'Correo del usuario.',
  `uacceso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Última vez que accedió al sistema.',
  `estado` enum('Y','N') NOT NULL COMMENT 'Si está activo Y, si no lo está N.',
  `usesion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `intento` tinyint(4) NOT NULL,
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los datos básicos del usuario para que pueda autenticarse en el sistema.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `venta_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `nombre` varchar(100) NOT NULL COMMENT 'Nombre del comprador.',
  `nit` varchar(20) NOT NULL COMMENT 'Número de Identificación Tributaria del comprador.',
  `correo` varchar(100) NOT NULL COMMENT 'Correo electrónico del comprador.',
  `telefono` varchar(45) NOT NULL COMMENT 'Teléfono del comprador.',
  `direccion` tinytext,
  `fechai` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de inicio de la compra.',
  `fecham` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la última modificación de la compra.',
  `ip` tinytext NOT NULL COMMENT 'IP del comprador.',
  `navegador` tinytext NOT NULL COMMENT 'Navegador del comprador.',
  PRIMARY KEY (`venta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COMMENT='Almacena los datos generales de las ventas.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venta_detalle`
--

DROP TABLE IF EXISTS `venta_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta_detalle` (
  `venta_detalle_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Un idendificador autoincremental y arbitrario para la tabla.',
  `venta_id` int(10) unsigned NOT NULL COMMENT 'Id de la venta.',
  `oferta_id` int(10) unsigned NOT NULL COMMENT 'Id de la oferta.',
  `usuario` tinytext NOT NULL COMMENT 'Nombre de la persona que usará la oferta.',
  `correo` varchar(100) NOT NULL COMMENT 'Correo de la persona que usará la oferta.',
  `precioo` double NOT NULL COMMENT 'Precio original de la oferta al momento de la compra.',
  `preciod` double NOT NULL COMMENT 'Precio con descuento al momento de la compra.',
  `enviado` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Si el cupón se ha enviado Y, si no se ha enviado N.',
  `cobrado` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Si el cupón se ha cobrado Y, si no se ha cobrado N.',
  PRIMARY KEY (`venta_detalle_id`),
  KEY `fk_oferta_id_venta_detalle_idx` (`oferta_id`),
  KEY `fk_venta_id_venta_detalle_idx` (`venta_id`),
  CONSTRAINT `fk_oferta_id_venta_detalle` FOREIGN KEY (`oferta_id`) REFERENCES `oferta` (`oferta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_id_venta_detalle` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`venta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `consultar_cupones`
--

/*!50001 DROP VIEW IF EXISTS `consultar_cupones`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `consultar_cupones` AS select concat(`venta_detalle`.`oferta_id`,`venta_detalle`.`venta_id`,`venta_detalle`.`venta_detalle_id`) AS `cupon`,`venta_detalle`.`usuario` AS `usuario`,`venta_detalle`.`precioo` AS `precioo`,`venta_detalle`.`correo` AS `correo`,`venta_detalle`.`preciod` AS `preciod`,`oferta`.`nombre` AS `Oferta`,`venta`.`fechai` AS `Fecha`,`empresa`.`nombre` AS `Empresa`,`oferta`.`condiciones` AS `condiciones`,`empresa`.`telefonos` AS `telefonos` from (((`venta_detalle` join `oferta` on((`oferta`.`oferta_id` = `venta_detalle`.`oferta_id`))) join `venta` on((`venta`.`venta_id` = `venta_detalle`.`venta_id`))) join `empresa` on((`empresa`.`empresa_id` = `oferta`.`empresa_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `listar_ofertas`
--

/*!50001 DROP VIEW IF EXISTS `listar_ofertas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `listar_ofertas` AS select `oferta`.`oferta_id` AS `oferta_id`,`oferta`.`nombre` AS `nombre`,`oferta`.`estado` AS `estado`,`empresa`.`nombre` AS `empresa`,`empresa`.`usuario_id` AS `usuario_id` from (`oferta` join `empresa` on((`oferta`.`empresa_id` = `empresa`.`empresa_id`))) order by `empresa`.`nombre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-02 10:05:17
